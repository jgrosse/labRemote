#ifndef EQUIPCONF__H
#define EQUIPCONF__H

#include <string>
#include <unordered_map>

#include <nlohmann/json.hpp>

#include <ClimateSensor.h>
#include <ADCDevice.h>
#include <I2CCom.h>
#include <ICom.h>

/** \brief Factory for discovering climate sensors and instantiate/configure them
 *
 * Valid JSON hardware config file is made up of one blocks that lists all requested sensors
 *  - sensors: physical sensors registered in a registry
 */
class ClimateSensorConf
{
public:
  /** Configuration @{ */
  
  /** Constructor */
  ClimateSensorConf();

  /** Constructor
   * @param hardwareConfigFile input JSON file with list of hardware resources and options
   */
  ClimateSensorConf(const std::string& hardwareConfigFile);

  /** Constructor
   * @param hardwareConfig JSON object with list of hardware sources and options
   */ 
  ClimateSensorConf(const nlohmann::json& hardwareConfig);

  ~ClimateSensorConf();

  /** Set input hardware list file
   * @param hardwareConfigFile input JSON file with list of hardware resources and options
   */
  void setHardwareConfig(const std::string& hardwareConfigFile);

  /** Set input hardware list file
   * @param hardwareConfig input JSON object with list of hardware resources and options
   */
  void setHardwareConfig(const nlohmann::json& hardwareConfig);

  /** Get input hardware json
   */
  nlohmann::json& getHardwareConfig(){return m_hardwareConfig;};

  /** Get sensor JSON configuration 
      @param label sensor name
      @return device JSON configuration (by reference)
   */  
  nlohmann::json getSensorConf(const std::string& label);

  /** Get ADC JSON configuration 
      @param label ADC name
      @return device JSON configuration (by reference)
   */  
  nlohmann::json getADCConf(const std::string& label);
  /** Get I2C JSON configuration 
      @param label I2C name
      @return device JSON configuration (by reference)
   */  
  nlohmann::json getI2CConf(const std::string& label);

  /** Get ICom JSON configuration 
      @param label ICom name
      @return device JSON configuration (by reference)
   */  
  nlohmann::json getIComConf(const std::string& label);

  /** @} */

public:
  /** Get (or create) hardware handles (by label) 
   * @{ 
   */

  /** Get sensor object corresponding to name
   *
   * If this is the first time the power supply name is requested:
   *  1. Create the object
   *  2. Call ClimateSensor::setConfiguration()
   *  3. Call ClimateSensor::init();
   *  4. Set ADC device 
   *
   * @param name label for the hardware object in JSON configuration file (*not* the model)
   */
  std::shared_ptr<ClimateSensor> getClimateSensor(const std::string& name);  

  /** \brief Retrieve or create and initialize an ADCDevice object based on JSON settings
   *
   * \param name of ADCDevice
   */
  std::shared_ptr<ADCDevice> getADCDevice(const std::string& name);  

  /** \brief Retrieve or create and initialize an I2CDevice object based on JSON settings
   *
   * \param name of I2CDevice
   */
  std::shared_ptr<I2CCom> getI2CDevice(const std::string& name);  

  /** \brief clear all objects
   *
   */
  void clear();

  /** @} */
  
private:

  /// JSON file with hardware list and options (see example input-hw.nlohmann::json file for syntax)
  nlohmann::json m_hardwareConfig;

  /// Stored handles of ClimateSensor pointers created
  std::unordered_map<std::string, std::shared_ptr<ClimateSensor>> m_listClimateSensor;

  /// Stored handles of ADCDevice pointers created
  std::unordered_map<std::string, std::shared_ptr<ADCDevice>> m_listADCDevice;

  /// Stored handles of I2CCom pointers created
  std::unordered_map<std::string, std::shared_ptr<I2CCom>> m_listI2CDevice;

  /// Stored handles of ICom pointers created
  std::unordered_map<std::string, std::shared_ptr<ICom>> m_listICom;

  /** \brief Create and initialize a ICom object based on JSON settings
   *
   * The `ComRegistry` is used to look up the `ICom` class
   * corresponding to the "protocol" field of the config object.
   * If no such class exists, an exception is thrown.
   *
   * \param name of ICom object
   */
  std::shared_ptr<ICom> createCommunication(const std::string& name);
};

#endif
