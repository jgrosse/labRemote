#include "ClimateSensorConf.h"
#include "ClimateSensorAnalog.h"
#include "ClimateSensorI2C.h"
#include "HIH4000.h"
#include "SHT85.h"
#include "ClimateSensorRegistry.h"
#include "ComRegistry.h"
#include "Logger.h"
#include <TextSerialCom.h>
#include <ADCDevComuino.h>
#include <I2CDevComuino.h>
#include <PCA9548ACom.h>
#include "FileUtils.h" // labremote_schema_file

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>

// json
#include <nlohmann/json-schema.hpp>
using nlohmann::json;
using nlohmann::json_schema::json_validator;

using json = nlohmann::json;

////////////////////
// Configuration
////////////////////

ClimateSensorConf::ClimateSensorConf()
{ }

ClimateSensorConf::~ClimateSensorConf()
{ }

ClimateSensorConf::ClimateSensorConf(const std::string& hardwareConfigFile)
{
  setHardwareConfig(hardwareConfigFile);
}

ClimateSensorConf::ClimateSensorConf(const json& hardwareConfig)
{
  setHardwareConfig(hardwareConfig);
}

void ClimateSensorConf::setHardwareConfig(const std::string& hardwareConfigFile)
{
  //load JSON object from file
  std::ifstream i(hardwareConfigFile);
  if(!i.good()) {
    throw std::runtime_error("Provided equipment configuration file \"" + hardwareConfigFile + "\" could not be found or opened");
  }
  i >> m_hardwareConfig;

  // validate the loaded schema
  json_validator validator;
  std::string schema_path = utils::labremote_schema_file();
  if(schema_path != "") {
    std::ifstream ifs_schema(schema_path, std::ios::in);
    validator.set_root_schema(json::parse(ifs_schema));
    try {
        validator.validate(m_hardwareConfig);
    } catch (std::exception& e) {
        logger(logERROR) << "The provided JSON configuration failed the schema check (input file: " << hardwareConfigFile << ")";
        logger(logERROR) << "Are using an old-style labRemote JSON configuration? If so, try running the following command:";
        logger(logERROR) << "    python /path/to/labRemote/scripts/update_config.py " << hardwareConfigFile;
        logger(logERROR) << "The error messsage from the JSON validation was: " << e.what();
    }
  } else {
    logger(logWARNING) << "Could not locate schema definition, cannot validate equipment configuration!";
  }
}

void ClimateSensorConf::setHardwareConfig(const json& hardwareConfig)
{
  //store JSON config file
  m_hardwareConfig = hardwareConfig;

  // validate the loaded schema
  json_validator validator;
  std::string schema_path = utils::labremote_schema_file();
  if(schema_path != "") {
    std::ifstream ifs_schema(schema_path, std::ios::in);
    validator.set_root_schema(json::parse(ifs_schema));
    try {
        validator.validate(m_hardwareConfig);
    } catch (std::exception& e) {
        logger(logERROR) << "The provided JSON configuration failed the schema check";
        logger(logERROR) << "Are using an old-style labRemote JSON configuration? If so, try running the following command:";
        logger(logERROR) << "    python /path/to/labRemote/scripts/update_config.py";
        logger(logERROR) << "The error messsage from the JSON validation was: " << e.what();
    }
  } else {
    logger(logWARNING) << "Could not locate schema definition, cannot validate equipment configuration!";
  }
}

json ClimateSensorConf::getSensorConf(const std::string& label)
{
  for (const auto& hw : m_hardwareConfig["sensors"]) {
    //check label 
    if (hw["name"] == label) return hw;
  }
  return json();
}

json ClimateSensorConf::getADCConf(const std::string& label)
{
  for (const auto& hw : m_hardwareConfig["ADCDevices"]) {
    //check label 
    if (hw["name"] == label) return hw;
  }
  return json();
}

json ClimateSensorConf::getI2CConf(const std::string& label)
{
  for (const auto& hw : m_hardwareConfig["I2CDevices"]) {
    //check label 
    if (hw["name"] == label) return hw;
  }
  return json();
}

json ClimateSensorConf::getIComConf(const std::string& label)
{
  for (const auto& hw : m_hardwareConfig["Com"]) {
    //check label 
    if (hw["name"] == label) return hw;
  }
  return json();
}

////////////////////
// Sensor
////////////////////

std::shared_ptr<ClimateSensor> ClimateSensorConf::getClimateSensor(const std::string& name)
{
  //first check if an object with the same name/type is already available (was already instantiated)
  if (m_listClimateSensor.find(name) != m_listClimateSensor.end()) {
    return m_listClimateSensor[name];
  }

  //Otherwise, create the object
  //check first if hardware configuration is available
  json reqCSConf = getSensorConf(name);
  if (reqCSConf.empty()) {
    logger(logWARNING) << "Requested sensor not found in input configuration file: " << name;
    return nullptr;
  }

  //setup device
  std::shared_ptr<ClimateSensor> cs = EquipRegistry::createClimateSensor(reqCSConf["type"], name);
  if(cs==nullptr) return nullptr;

  // configure already the sensor
  cs->setConfiguration(reqCSConf);
  cs->init();

  // connect T-sensor object to HIH4000 (a bit ugly, but works at least)
  std::shared_ptr<HIH4000> hum1 = std::dynamic_pointer_cast<HIH4000>(cs);
  if(hum1!=nullptr)
    {
      if(!reqCSConf.contains("Tsens"))
	{
	  logger(logWARNING) << "Requested sensor needs a separate temperature sensor but is missing its configuration";
	  return nullptr;
	}
      hum1->setTempSens(getClimateSensor(reqCSConf["Tsens"]));
    }

  std::shared_ptr<SHT85> hum2 = std::dynamic_pointer_cast<SHT85>(cs);
  if(hum2!=nullptr)
    {
      if(reqCSConf.contains("Tsens")) //  no need for a warning, SHT can work with internal T sensor
	{
	  hum2->setTempSens(getClimateSensor(reqCSConf["Tsens"]));
	}
    }

  // set up ADC object for analog sensors
  std::shared_ptr<ClimateSensorAnalog> csa = std::dynamic_pointer_cast<ClimateSensorAnalog>(cs);
  if(csa!=nullptr)
    {
      if(!reqCSConf.contains("ADC"))
	{
	  logger(logWARNING) << "Requested sensor is missing ADC configuration";
	  return nullptr;
	}
      csa->setADC(getADCDevice(reqCSConf["ADC"]));
    }

  // set up I2C object for digital sensors
  std::shared_ptr<ClimateSensorI2C> csi = std::dynamic_pointer_cast<ClimateSensorI2C>(cs);
  if(csi!=nullptr)
    {
      if(!reqCSConf.contains("I2C"))
	{
	  logger(logWARNING) << "Requested sensor is missing I2C configuration";
	  return nullptr;
	}
      csi->setI2C(getI2CDevice(reqCSConf["I2C"]));
    }


  // register sensor in private list
  m_listClimateSensor.insert(std::make_pair(name, cs));
  return cs;
}

////////////////////
// Devices
////////////////////

std::shared_ptr<ADCDevice> ClimateSensorConf::getADCDevice(const std::string& name){
  // first check if an object with the same name/type is already available (was already instantiated)
  if (m_listADCDevice.find(name) != m_listADCDevice.end()) {
    return m_listADCDevice[name];
  }

  // Otherwise, create the object
  // check first if hardware configuration is available
  json reqADCConf = getADCConf(name);
  if(reqADCConf.empty() || reqADCConf["communication"].empty()){
    logger(logWARNING) << "Requested ADCDevice not found in input configuration file: " << name;
    return nullptr;
  }

  // open serial device
  std::shared_ptr<ICom> com=createCommunication(reqADCConf["communication"]);

  // ADCDevice created as ADCDevComuino - hard-coded for now, needs to be fixed
  double reference = 1.;
  if(!reqADCConf["reference"].empty())
    reference = reqADCConf["reference"];
  std::shared_ptr<ADCDevice> dev(new ADCDevComuino(reference,std::dynamic_pointer_cast<TextSerialCom>(com)));
  m_listADCDevice.insert(std::make_pair(name, dev));

  return dev;
} 

std::shared_ptr<I2CCom> ClimateSensorConf::getI2CDevice(const std::string& name){
  // first check if an object with the same name/type is already available (was already instantiated)
  if (m_listI2CDevice.find(name) != m_listI2CDevice.end()) {
    return m_listI2CDevice[name];
  }

  // Otherwise, create the object
  // check first if hardware configuration is available
  json reqI2CConf = getI2CConf(name);
  if(reqI2CConf.empty() || reqI2CConf["deviceAddr"].empty()){
    logger(logWARNING) << "Requested device address for I2CDevice not found in input configuration file: " << name;
    return nullptr;
  }
  uint8_t deviceAddr = reqI2CConf["deviceAddr"];

  if(reqI2CConf["I2Ctype"].empty()){
    logger(logWARNING) << "Requested I2Ctype not found in input configuration file: " << name;
    return nullptr;
  }
  std::string type = reqI2CConf["I2Ctype"];

  // need a factory for the following
  if(type=="I2CDevComuino"){
    if(reqI2CConf["communication"].empty()){
      logger(logWARNING) << "Requested I2CDevice not found in input configuration file: " << name;
      return nullptr;
    }
    // open serial device
    std::shared_ptr<ICom> com=createCommunication(reqI2CConf["communication"]);
    
    // I2CDevice created as I2CDevComuino - hard-coded for now, needs to be fixed
    std::shared_ptr<I2CCom> dev(new I2CDevComuino(deviceAddr, std::dynamic_pointer_cast<TextSerialCom>(com)));
    m_listI2CDevice.insert(std::make_pair(name, dev));
    return dev;
  }
  else if(type=="PCA9548ACom"){
    if(reqI2CConf["I2CCom"].empty()){
      logger(logWARNING) << "Requested I2CCom not found in input configuration file: " << name;
      return nullptr;
    }
    // open I2Com
    std::shared_ptr<I2CCom> com=getI2CDevice(reqI2CConf["I2CCom"]);
    uint8_t channel = 0;
    if(!reqI2CConf["channel"].empty()){
      channel = reqI2CConf["channel"];
    }
    // I2CDevice created as PCA9548ACom - hard-coded for now, needs to be fixed
    std::shared_ptr<I2CCom> dev(new PCA9548ACom(deviceAddr, channel, com));
    m_listI2CDevice.insert(std::make_pair(name, dev));
    return dev;
  }
  else {
      logger(logWARNING) << "Requested I2CDevice type not handled: " << name;
      return nullptr;
  }

} 
std::shared_ptr<ICom> ClimateSensorConf::createCommunication(const std::string& name)
{
  // first check if an object with the same name/type is already available (was already instantiated)
  if (m_listICom.find(name) != m_listICom.end()) {
    return m_listICom[name];
  }

  // Otherwise, create the object
  // check first if hardware configuration is available
  json config = getIComConf(name);
  if (!config.contains("protocol"))
    throw std::runtime_error("Communicaiton block is missing protocol definition.");

  std::shared_ptr<ICom> com=EquipRegistry::createCom(config["protocol"]);
  com->setConfiguration(config);
  com->init();
  m_listICom.insert(std::make_pair(name, com));

  // somehow needed to initiate communication to Arduino
  try{
    com->send("???");
    com->receive();
  } catch(...){
  }
  // now the real test
  try{
    com->send("???");
    com->receive();
  } catch(std::runtime_error &err){
    logger(logWARNING) << "Testing Arduino communication: runtime exception: " << err.what();
    return nullptr;
  } catch(...){
    logger(logWARNING) << "Testing Arduino communication: unknown exception";
    return nullptr;
  }

  return com;
}
void ClimateSensorConf::clear(){
  m_listClimateSensor.clear();
  m_listICom.clear();
  m_listADCDevice.clear();
  m_listI2CDevice.clear();
  return;
}
