#include "SerialCom.h"

#include "Logger.h"
#include "ScopeLock.h"

#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/file.h>

#include <cerrno>
#include <cstring>
#include <stdexcept>

//Register com
#include "ComRegistry.h"
REGISTER_COM(SerialCom)

const std::unordered_map<std::string, speed_t> SerialCom::mapBAUDRATE = {
									 {"B0"    , B0    },
									 {"B50"   , B50   },
									 {"B75"   , B75   },
									 {"B110"  , B110  },
									 {"B134"  , B134  },
									 {"B150"  , B150  },
									 {"B200"  , B200  },
									 {"B300"  , B300  },
									 {"B600"  , B600  },
									 {"B1200" , B1200 },
									 {"B1800" , B1800 },
									 {"B2400" , B2400 },
									 {"B4800" , B4800 },
									 {"B9600" , B9600 },
									 {"B19200", B19200},
									 {"B38400", B38400},
									 {"B115200", B115200}
};

const std::unordered_map<std::string, SerialCom::CharSize> SerialCom::mapCHARSIZE = {
										     {"CS5" , CharSize5 },
										     {"CS6" , CharSize6 },
										     {"CS7" , CharSize7 },
										     {"CS8" , CharSize8 }
};

SerialCom::SerialCom(const std::string& port, speed_t baud, bool parityBit, bool twoStopBits, bool flowControl, CharSize charsize)
  : m_port(port), m_baudrate(baud), m_parityBit(parityBit), m_twoStopBits(twoStopBits), m_flowControl(flowControl), m_charsize(charsize)
{ }

SerialCom::SerialCom()
{ }

SerialCom::~SerialCom()
{ if(m_dev!=0) close(m_dev); }

void SerialCom::setTimeout(float timeout)
{
  m_timeout=timeout;

  //
  // Also update the device
  if(m_dev!=0)
    {
      ScopeLock lock(this);

      if (tcgetattr(m_dev, &m_tty))
	throw std::runtime_error("SerialCom::config -> Could not get tty attributes!");

      m_tty.c_cc[VMIN ] = 0;
      m_tty.c_cc[VTIME] = static_cast<uint32_t>(m_timeout/10+0.5);

      if (tcsetattr(m_dev, TCSANOW, &m_tty))
	throw std::runtime_error("SerialCom::config -> Could not set tty attributes!");
    }
}

float SerialCom::getTimeout()
{
  if(m_dev==0)
    {
      return m_timeout;
    }
  else
    {
      ScopeLock lock(this);

      if (tcgetattr(m_dev, &m_tty))
	throw std::runtime_error("SerialCom::config -> Could not get tty attributes!");

      return m_tty.c_cc[VTIME]*10;
    }
}


void SerialCom::init()
{
  if(is_open()) return;

  if(m_port.empty())
    throw std::runtime_error("SerialCom::init requires a device name");

  m_dev = open(m_port.c_str(), O_RDWR | O_NOCTTY);
  if(m_dev<0)
    throw std::runtime_error("Error opening "+m_port+": "+std::strerror(errno));

  config();
  m_good = true;
}

void SerialCom::config()
{
  logger(logDEBUG3) << __PRETTY_FUNCTION__;

  logger(logDEBUG3) << "Configuring serial device " << m_port;
  logger(logDEBUG3) << "  Baud Rate: " << m_baudrate;
  logger(logDEBUG3) << "  Enable parity bit: " << m_parityBit;
  logger(logDEBUG3) << "  Use two stop bits: " << m_twoStopBits;
  logger(logDEBUG3) << "  Enable hardware flow control: " << m_flowControl;
  logger(logDEBUG3) << "  Character size: " << m_charsize;

  ScopeLock lock(this);
  if (tcgetattr(m_dev, &m_tty))
    throw std::runtime_error("SerialCom::config -> Could not get tty attributes!");

  m_tty_old = m_tty;

  cfsetospeed(&m_tty, m_baudrate);
  cfsetispeed(&m_tty, m_baudrate);

  if(m_parityBit)
    m_tty.c_cflag &=  PARENB;
  else
    m_tty.c_cflag &= ~PARENB;

  if(m_twoStopBits)
    m_tty.c_cflag &=  CSTOPB;
  else
    m_tty.c_cflag &= ~CSTOPB;

  if(m_flowControl)
    m_tty.c_cflag &=  CRTSCTS;
  else
    m_tty.c_cflag &= ~CRTSCTS;

  m_tty.c_cflag &= ~CSIZE;
  m_tty.c_cflag |= m_charsize;
  m_tty.c_cflag |= CREAD | CLOCAL;	// turn on READ & ignore ctrl lines

  cfmakeraw(&m_tty);
  m_tty.c_lflag &= ~(ICANON|ECHO);	// Non-canonical mode, no echo

  // Timeout
  m_tty.c_cc[VMIN ] = 0;
  m_tty.c_cc[VTIME] = static_cast<uint32_t>(m_timeout*10+0.5);

  tcflush(m_dev, TCIFLUSH);		// Empty buffers

  if (tcsetattr(m_dev, TCSANOW, &m_tty))
    std::runtime_error("SerialCom::config -> Could not set tty attributes!");
}

void SerialCom::flush()
{
  if(tcflush(m_dev, TCIFLUSH)<0)
    throw std::runtime_error("Error flushing for "+m_port+": "+std::strerror(errno));
}

void SerialCom::setDTR(bool on)
{
  int controlbits = TIOCM_DTR;
  if(ioctl(m_dev, (on ? TIOCMBIS : TIOCMBIC), &controlbits)<0)
    throw std::runtime_error("Error setting control bits for "+m_port+": "+std::strerror(errno));
}

void SerialCom::send(const std::string& buf)
{
  ScopeLock lock(this);
  int n_write = ::write(m_dev, buf.c_str(), buf.size());

  if(n_write<0)
    throw std::runtime_error("Error writing to "+m_port+": "+std::strerror(errno));
}

void SerialCom::send(char *buf, size_t length)
{
  ScopeLock lock(this);
  int n_write = ::write(m_dev, buf, length);

  if(n_write<0)
    throw std::runtime_error("Error writing to "+m_port+": "+std::strerror(errno));
}

std::string SerialCom::receive()
{
  ScopeLock lock(this);
  int n_read = ::read(m_dev, m_tmpbuf, MAX_READ);

  if(n_read>=0)
    return std::string(m_tmpbuf, n_read);
  else
    throw std::runtime_error("Error reading from "+m_port+": "+std::strerror(errno));
}

uint32_t SerialCom::receive(char *buf, size_t length)
{
  ScopeLock lock(this);
  int n_read = ::read(m_dev, buf, length);

  if(n_read<0)
    throw std::runtime_error("Error reading from "+m_port+": "+std::strerror(errno));

  return n_read;
}

std::string SerialCom::sendreceive(const std::string& cmd)
{
  ScopeLock lock(this);

  send(cmd);
  std::string ret=receive();

  return ret;
}

void SerialCom::sendreceive(char *wbuf, size_t wlength, char *rbuf, size_t rlength)
{
  ScopeLock lock(this);

  send(wbuf, wlength);
  receive (rbuf, rlength);
}

void SerialCom::lock()
{
  flock(m_dev, LOCK_EX);
  m_lock_counter++;
} 

void SerialCom::unlock()
{
  if(m_lock_counter==0) return; // No lock exists

  m_lock_counter--;
  if(m_lock_counter==0)
    flock(m_dev, LOCK_UN);
} 

void SerialCom::setConfiguration(const nlohmann::json& config)
{
  for (const auto &kv : config.items())
    {
      if(kv.key()=="port")
	{
	  m_port=kv.value();
	}
      else if(kv.key()=="baudrate")
	{
	  if(mapBAUDRATE.count(kv.value())==1)
	    m_baudrate=mapBAUDRATE.at(kv.value());
	  else
	    throw std::runtime_error("SerialCom::setConfiguration saw invalid baudrate value "+kv.value().get<std::string>());
	}
      else if(kv.key()=="parityBit")
	{
	  m_parityBit=kv.value();
	}
      else if(kv.key()=="twoStopBits")
	{
	  m_twoStopBits=kv.value();
	}
      else if(kv.key()=="flowControl")
	{
	  m_flowControl=kv.value();
	}
      else if(kv.key()=="charsize")
	{
	  if(mapCHARSIZE.count(kv.value())==1)
	    m_charsize=mapCHARSIZE.at(kv.value());
	  else
	    throw std::runtime_error("SerialCom::setConfiguration saw invalid charsize value "+kv.value().get<std::string>());
	}      
      else if(kv.key()=="timeout")
	{
	  m_timeout=kv.value();
	}
    }
}

  


