#ifndef GPIBNICOM_H
#define GPIBNICOM_H

#include "ICom.h"

/** 
 * Implementation of block serial communication with
 * read/write calls that uses ASCII strings for data
 * transmission in the presence of a GPIB bus.
 *
 * No termination needed
 *
 * NI GPIB controllers are assumed:
 * see https://linux-gpib.sourceforge.io/ for driver
 * Code was tested with NI's GPIB-USB-HS controllers:
 * https://www.ni.com/en-us/support/model.gpib-usb-hs.html
 * 
 */
class GPIBNICom : public ICom
{
public:
  /** \brief Create serial communication object and set settings
   * @param board_id ID of GPIB dongle or PCIe board (i.e. 0 if on /dev/gpib0)
   * @param gpib_addr Primary address (gpib_saddr: secondary addr.) of device on GPIB bus
   */
  GPIBNICom(uint16_t board_id, uint16_t gpib_addr, uint16_t gpib_saddr);
  GPIBNICom();

  ~GPIBNICom();

  /** \brief Initialize communication.
   *
   * initialise GPIB divice via linux-gpib
   *
   * On success, the implementation should set `m_good=true`.
   *
   * Throw `std::runtime_error` on error.
   */
  void init();

  /** \brief Set communication settings from JSON object
   *
   * Valid keys:
   *  - `gpib_addr` : Primary address (PAD) of device on GPIB bus
   *  - `gpib_saddr`: Secondary address (SAD) of device on GPIB bus (default: 0)
   *  - `gpib_board`: Board ID of GPIB interface, i.e. 0 if on /dev/gpib0 (default: 0)
   *  - `read_tmo`: Timeout (s) to wait for new data (default: T10s)
   *
   * \param config JSON configuration
   */
  void setConfiguration(const nlohmann::json& config);

  /** Send data to device
   *
   * Throw `std::runtime_error` on error.
   *
   * \param buf Data to be sent
   * \param length Number of characters in `buf` that should be sent
   */  
  void send(char *buf, size_t length);

  /** Send data to device
   *
   * Throw `std::runtime_error` on error.
   *
   * \param buf Data to be sent
   */
  void send(const std::string& buf);

  /** Read data from device
   *
   * Throw `std::runtime_error` on error.
   *
   * \return Received data
   */  
  std::string receive();

  /** Receive data from device
   *
   * Throw `std::runtime_error` on error.
   *
   * \param buf Buffer where to store results 
   * \param length Number of characters to receive.
   *
   * \return Number of characters received
   */
  uint32_t receive(char *buf, size_t length);

  /** Send data to device and receive reply
   *
   * Throw `std::runtime_error` on error.
   *
   * \param cmd Data to be sent
   *
   * \return Returned data
   */
  std::string sendreceive(const std::string& cmd);

  /** Send data to device and receive reply
   *
   * Throw `std::runtime_error` on error.
   *
   * \param wbuf Data to be sent
   * \param wlength Number of characters in `wbuf` that should be sent
   * \param rbuf Buffer where to store results 
   * \param rlength Number of characters to receive.
   *
   * \return Number of characters received
   */
  void sendreceive(char *wbuf, size_t wlength, char *rbuf, size_t rlength);  

protected:
  /** Request exlusive access to device.
   *
   * If a single hardware bus is used to connect multiple devices,
   * the access to all of them should be locked to remove changes
   * of cross-talk.
   *
   * Throw `std::runtime_error` on error.
   *
   */  
  void lock();

  /** Release exlusive access to device.
   *
   * Throw `std::runtime_error` on error.
   *
   */  
  void unlock();

private:
  //! verbous translation of linux-GPIB error codes
  void procGPIBErr();

  //! device id returned from opening
  uint16_t m_device;

  //! ID of GPIB interface
  uint16_t m_gpib_board = 0;

  //! Addresses of device on GPIB bus
  uint16_t m_gpib_addr = 0;
  uint16_t m_gpib_saddr = 1;

  //! Timeout for waiting for new data on receive
  uint16_t m_read_tmo = 10;

  //! Count number of lock() calls on this device
  uint32_t m_lock_counter = 0;

  // dummy file handle to be able to use flock() for locking
  FILE *m_fp;
};

#endif
