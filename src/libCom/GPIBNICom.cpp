#include "GPIBNICom.h"

#include <gpib/ib.h>
#include <string>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <unistd.h>

#include "Logger.h"

//Register com
#include "ComRegistry.h"
REGISTER_COM(GPIBNICom)

GPIBNICom::GPIBNICom(uint16_t board_id, uint16_t gpib_addr, uint16_t gpib_saddr) : ICom() {
  m_good = false;
  m_gpib_board = board_id;
  m_gpib_addr = gpib_addr;
  m_gpib_saddr = gpib_saddr;
  m_read_tmo = T10s;
}
GPIBNICom::GPIBNICom() : ICom() {
  m_good = false;
  m_gpib_addr = 0;
  m_gpib_saddr = 0;
  m_gpib_board = 0;
  m_read_tmo = T10s;
}
GPIBNICom::~GPIBNICom() {
  if(m_good){
    lock();
    ibclr(m_device);
    ibonl(m_device, 0);
    unlock();
    fclose(m_fp);
  }
}
void GPIBNICom::init() {
  m_device = ibdev(m_gpib_board, m_gpib_addr, m_gpib_saddr, m_read_tmo, 1, 0x0000);

  m_fp = fopen(("/tmp/gpib_"+std::to_string(m_gpib_board)).c_str(),"r");
  if(m_fp!=nullptr){
    fclose(m_fp);
  } else {
    system(("touch /tmp/gpib_"+std::to_string(m_gpib_board)+"; chmod 664 /tmp/gpib_"+std::to_string(m_gpib_board)).c_str());
  }

  m_fp = fopen(("/tmp/gpib_"+std::to_string(m_gpib_board)).c_str(),"a");
  if (ibsta & ERR) {
    logger(logERROR) << "ERROR initialising GPIB: ";
    procGPIBErr();
    throw std::runtime_error("GPIBNICom::init() returned error from ibdev");
  } else
    m_good = true;
}
void GPIBNICom::setConfiguration(const nlohmann::json& config) {
  for (const auto &kv : config.items())  {
    if(kv.key()=="gpib_addr") {
      m_gpib_addr = kv.value();
    }
    else if(kv.key()=="gpib_saddr") {
      m_gpib_saddr = kv.value();
    }
    else if(kv.key()=="gpib_board") {
      m_gpib_board = kv.value();
    }
    else if(kv.key()=="read_tmo") {
      m_read_tmo = kv.value();
    }
  }
}
void GPIBNICom::send(char *buf, size_t length) {
  if(!m_good) throw std::runtime_error("GPIBNICom::send(char*,size_t) : device in bad state!");

  lock();
  ibwrt(m_device, buf, length);
  unlock();

  if (ibsta & ERR) {
    logger(logERROR) << "ERROR from ibwrt: ";
    procGPIBErr();
    throw std::runtime_error("GPIBNICom::send error from ibwrt");
 }
}
void GPIBNICom::send(const std::string& buf) {
  char *mybuf = new char[buf.length()];
  strcpy(mybuf, buf.c_str());
  send(mybuf, buf.length());
  delete[] mybuf;
}

std::string GPIBNICom::receive() {
  std::string retstr="";
  int rdc=255;
  char buf[256];
  while(rdc==255){
    rdc = receive(buf, 255);
    retstr+=std::string(buf);
  }
  return retstr;
}

uint32_t GPIBNICom::receive(char *buf, size_t length) {
  if(!m_good) throw std::runtime_error("GPIBNICom::receive(char*,size_t) : device in bad state!");

  lock();
  ibrd(m_device, buf, length);// returns error code, not no. char's
  unlock();

  int retval = strlen(buf);
  // remove trailing '\n'
  for(int i=0;i<=retval;i++){
    if(buf[i] == '\n'){
      buf[i] = '\0';
      for(int k=i+1;k<=retval;k++) buf[k] = 0;
      break;
    }
  }
  if (ibsta & ERR) {
    logger(logERROR) << "ERROR from ibrd: ";
    procGPIBErr();
    throw std::runtime_error("GPIBNICom::receive error from ibrd");
  }
  return retval;
}
std::string GPIBNICom::sendreceive(const std::string& cmd) {
  lock();
  send(cmd);
  std::string retstr = receive();
  unlock();
  return retstr;
}
void GPIBNICom::sendreceive(char *wbuf, size_t wlength, char *rbuf, size_t rlength) {
  lock();
  send(wbuf, wlength);
  receive (rbuf, rlength);
  unlock();
}
void GPIBNICom::procGPIBErr(){
    if (iberr == EDVR) logger(logERROR) << " EDVR <Driver Error>" ;
    if (iberr == ECIC) logger(logERROR) << " ECIC <Not Controller-In-Charge>" ;
    if (iberr == ENOL) logger(logERROR) << " ENOL <No Listener>" ;
    if (iberr == EADR) logger(logERROR) << " EADR <Address error>" ;
    if (iberr == EARG) logger(logERROR) << " EARG <Invalid argument>" ;
    if (iberr == ESAC) logger(logERROR) << " ESAC <Not System Controller>" ;
    if (iberr == EABO) logger(logERROR) << " EABO <Operation aborted>" ;
    if (iberr == ENEB) logger(logERROR) << " ENEB <No GPIB board>" ;
    if (iberr == EDMA) logger(logERROR) << " EDMA <DMA Error>" ;   
    if (iberr == EOIP) logger(logERROR) << " EOIP <Async I/O in progress>" ;
    if (iberr == ECAP) logger(logERROR) << " ECAP <No capability>" ;
    if (iberr == EFSO) logger(logERROR) << " EFSO <File system error>" ;
    if (iberr == EBUS) logger(logERROR) << " EBUS <Command error>" ;
    if (iberr == ESRQ) logger(logERROR) << " ESRQ <SRQ stuck on>" ;
    if (iberr == ETAB) logger(logERROR) << " ETAB <Table Overflow>" ;
}
void GPIBNICom::lock()
{
  if(flock(fileno(m_fp), LOCK_EX)!=0){ 
    if(errno==EBADF) logger(logERROR) << "fd is not an open file descriptor";
    else if(errno==EINTR) logger(logERROR) << "While waiting to acquire a lock, the  call  was  interrupted";
    else if(errno==EINVAL) logger(logERROR) << "operation is invalid";
    else if(errno==ENOLCK) logger(logERROR) << "kernel ran out of lock memory";
    else logger(logERROR) << "unknown flock error";
  }
  m_lock_counter++;
} 

void GPIBNICom::unlock()
{
  if(m_lock_counter==0) return; // No lock exists

  m_lock_counter--;
  if(m_lock_counter==0)
    flock(fileno(m_fp), LOCK_UN);
} 
