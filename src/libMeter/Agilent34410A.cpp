#include "Agilent34410A.h"
#include "ComRegistry.h"
#include "Logger.h"

Agilent34410A::Agilent34410A(const nlohmann::json& config) {
  m_com = EquipRegistry::createCom("GPIBNICom");
  m_com->setConfiguration(config);
  m_com->init();
}

Agilent34410A::Agilent34410A(uint16_t board_id, uint16_t gpib_addr, uint16_t gpib_saddr) {
  nlohmann::json gpibConfig = {
    {"gpib_addr", gpib_addr},
    {"gpib_saddr", gpib_saddr},
    {"gpib_board", board_id},
    {"read_tmo", 10}
  };
  
  m_com = EquipRegistry::createCom("GPIBNICom");
  m_com->setConfiguration(gpibConfig);
  m_com->init();
}


Agilent34410A::~Agilent34410A() {
}

void Agilent34410A::send(std::string cmd) {
	logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
	m_com->send(cmd);
	std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}


std::string Agilent34410A::receive(std::string cmd)
{
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->send(cmd);
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf=m_com->receive();
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
  return buf;
}

std::string Agilent34410A::sense(enum AgilentMode mode) {
    std::string buf = "";
    switch (mode) {
    	case AgilentMode::VOLTAGEDC:
    		//this->send("CONF:VOLT:DC AUTO");
                buf = this->receive("MEAS:VOLT:DC?");
    		break;
    	case AgilentMode::CURRENTDC: // not tested yet.
    		this->send("CONF:CURR:DC");
                buf = this->receive("READ?");
    		break;
    	case AgilentMode::FREQUENCY: // not tested yet.
                buf = this->receive("MEAS:FREQ?");
    		break;
    	case AgilentMode::CAPACITANCE: // not tested yet.
                buf = this->receive("MEAS:CAP?");
    		break;
    	case AgilentMode::RESISTANCE: // not tested yet.
                buf = this->receive("MEAS:RES?");
    		break;
    	default:
    		logger(logERROR) << __PRETTY_FUNCTION__ << " : Unknown mode!";
    		break;
    }
    //logger(logDEBUG2) << __PRETTY_FUNCTION__ << " SYST:ERR " << this->receive("SYST:ERR?");
    return buf;
}


