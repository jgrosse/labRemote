#ifndef AGILENT34410A_H
#define AGILENT34410A_H

// ###################
// AGILENT 34410A Multimeter
// Author:
// Date: Apr.2023
// ###################


#include <iostream>
#include <string>
#include <thread>
#include <chrono>

#include "ICom.h"

enum class AgilentMode {
    CURRENTDC,
    VOLTAGEDC,
    FREQUENCY,
    CAPACITANCE,
    RESISTANCE
};

class Agilent34410A{
    public:
        Agilent34410A(const nlohmann::json& config);
        Agilent34410A(uint16_t board_id, uint16_t gpib_addr, uint16_t gpib_saddr);
        ~Agilent34410A();

        std::string sense(enum AgilentMode);

    private:
        std::shared_ptr<ICom> m_com;
        unsigned m_addr;

        void send(std::string cmd);
        std::string receive(std::string cmd);

        std::chrono::milliseconds m_wait{20};
};

#endif

