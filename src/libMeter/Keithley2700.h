#ifndef KEITHLEY2700_H
#define KEITHLEY2700_H

// ###############################
// Keithley Meter
// Author: Joern Grosse-Knetter
// Date: Jul 2023
// Notes: Assuming NI GPIB to USB
// ##############################

#include <iostream>
#include <string>
#include <thread>
#include <chrono>

#include "ICom.h"

enum class KeithleyMode {
    VOLTAGE,
    CURRENT,
    RESISTANCE,
    FREQUENCY
};

class Keithley2700 {
    public:
        Keithley2700(const nlohmann::json& config);
        Keithley2700(uint16_t board_id, uint16_t gpib_addr, uint16_t gpib_saddr);
        ~Keithley2700();

        void init();

	std::string readChannel(enum KeithleyMode);
	std::string readChannel(std::string channel, enum KeithleyMode);


    private:
        std::shared_ptr<ICom> m_com;
	std::string m_unit="";

        void send(std::string cmd);
        std::string receive(std::string cmd);
        void setSense(enum KeithleyMode);
        std::string sense();

};

#endif
