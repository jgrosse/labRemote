#include "Keithley2700.h"
#include "ComRegistry.h"
#include "Logger.h"

Keithley2700::Keithley2700(const nlohmann::json& config){
  m_com = EquipRegistry::createCom("GPIBNICom");
  m_com->setConfiguration(config);
  m_com->init();
}

Keithley2700::Keithley2700(uint16_t board_id, uint16_t gpib_addr, uint16_t gpib_saddr){
  nlohmann::json gpibConfig = {
    {"gpib_addr", gpib_addr},
    {"gpib_saddr", gpib_saddr},
    {"gpib_board", board_id},
    {"read_tmo", 10}
  };
  
  m_com = EquipRegistry::createCom("GPIBNICom");
  m_com->setConfiguration(gpibConfig);
  m_com->init();
}

Keithley2700::~Keithley2700() {
}

// TODO send/receive should be in prologix class
void Keithley2700::send(std::string cmd) {
    logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
    m_com->send(cmd);
}

std::string Keithley2700::receive(std::string cmd)
{
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->send(cmd);
  std::string buf=m_com->receive();
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
  return buf;
}

void Keithley2700::init() {
    this->send("*RST");
    this->send(":FORMAT:ELEMENTS READ,CHAN,UNIT");
    this->send(":SYST:BEEP:STAT OFF");//disable beep
    this->send(":ROUTE:OPEN:ALL");
}

void Keithley2700::setSense(enum KeithleyMode mode) {
    switch (mode) {
        case KeithleyMode::VOLTAGE:
            this->send(":SENSE:FUNC \"VOLT\"");
            this->send(":SENS:VOLT:DC:RANG:AUTO ON");
	    m_unit="VDC";
            break;
        case KeithleyMode::CURRENT:
            this->send(":SENSE:FUNC \"CURR\"");
            this->send(":SENS:CURR:DC:RANG:AUTO ON");
	    m_unit="ADC";
            break;
        case KeithleyMode::RESISTANCE:
            this->send(":SENSE:FUNC \"RES\"");
            this->send(":SENS:RES:RANG:AUTO ON");
	    m_unit="OHM";
            break;
        case KeithleyMode::FREQUENCY:
            this->send(":SENSE:FUNC \"FREQ\"");
            this->send(":SENS:RES:RANG:AUTO ON");
	    m_unit="HZ";
            break;
        default:
            logger(logERROR) << __PRETTY_FUNCTION__ << " : Unknown mode!";
            break;
    }
}

std::string Keithley2700::sense() {
    std::string retval = this->receive(":READ?");
    int pos = retval.find(m_unit+",");
    m_unit="";
    if(pos<0) pos = retval.find(",");
    logger(logDEBUG) << "Raw reaing: " << retval <<" , filtered reading: " << retval.substr(0, pos);
    return retval.substr(0, pos);
}

// single channel from front panel
std::string Keithley2700::readChannel(enum KeithleyMode mode){
    setSense(mode);
    return sense();
}

//SCAN scanner board -- Switching Matrix Functions

std::string Keithley2700::readChannel(std::string channel, enum KeithleyMode mode){
    this->send(":ROUTE:OPEN:ALL");
    setSense(mode);
    this->send(":ROUTE:CLOSE (@" + channel + ")");
    return sense();
}



