#ifndef HS1101LF_H
#define HS1101LF_H

#include "ClimateSensorAnalog.h"

//! \brief An implementation of 'ClimateSensorAnalog' for reading an HS1101LF sensor
/**
 * Measures humidity using a HS1101LF sensor connected to a frequency meter
 * to be checked if connecting to one of the analog pins
 * on an Arduino via an ADC intereface.works; current work-around:
 * set frequency from external source, fix Arduino connection later
 * Frequency as from prescribed oscillator requires temperature connection: according sensor 
 * has to be provided (tempSensF). For complete reading and for dewpoint calculation, also
 * a sensor for the ambient temperature (which might not be the oscillator temperature)
 * has to be provided (tempSensA)
 */

class HS1101LF: public ClimateSensorAnalog
{
public:
  /**
   * \param chan The channel number of the analog channel connected to the HS1101LF
   * \param dev The ADCDevice for interfacing with the Adruino or similar - should be calibrated
   *        to return measured voltage in Volts to match offset and slope
   * \param tempSensA The Temperature sensor used for ambient temperature (dewpoint)
   * \param tempSensF The Temperature sensor used for the temperature compensation
   * \param Coffset The capacity offset e.g. due to stray capacitances in the oscillator
   * \param Cnom The nominal capacity of the humidity sensor
   * \param Tslope The gradient for the frequency temperature correction
   * \param Rosc1 Resistor used in the oscillator (R22 in manufacturer's data sheet)
   * \param Rosc2 Resistor used in the oscillator (R4 in manufacturer's data sheet)
   */
  HS1101LF(uint8_t chan, std::shared_ptr<ADCDevice> dev, std::shared_ptr<ClimateSensor> tempSensA, 
           std::shared_ptr<ClimateSensor> tempSensF, float Coffset = 20.e-12, float Tslope = 2.e-4,
	   float Cnom = 180.e-12, float Rosc1=468.8e3, float Rosc2=50.e3);

  /* \brief constructor needed for factory
   *
   */
  HS1101LF(std::string name);

  virtual ~HS1101LF();

  /* \brief Configure sensor based on JSON object
   *
   * Valid keys:
   *  - `chan`: The channel number of the analog channel connected to the HS1101LF
   *
   * \param config JSON configuration
   */
  virtual void setConfiguration(const nlohmann::json& config);

  /**
   * \param tempSens pointer to the temperature sensor needed to temperature-correct hum. reading
   */
  void setTempSensA(std::shared_ptr<ClimateSensor> tempSensA);

  /**
   * \param tempSens pointer to the temperature sensor needed to temperature-correct freq. reading
   */
  void setTempSensF(std::shared_ptr<ClimateSensor> tempSensF);

  virtual void init();
  virtual void reset();
  virtual void read();

  // work-around for now: allow to set frequency and temperatures from external source instead of reading Arduino
  virtual void setFreq(float freq){m_freq = freq;};
  virtual void setTemp(float temp){m_temperature = temp;};
  virtual void setTempOsc(float temp){m_temperatureOsc = temp;};

  virtual float temperature() const;
  virtual float humidity() const;
  virtual float pressure() const;

private:
  //! The channel of the ADC connected to the Pt
  uint8_t m_chan = 0;
  //! The temperature recorded in the last reading (defaults to -273.15)
  float m_temperature = -273.15;
  //! The humidity recorded in the last reading (defaults to 0)
  float m_humidity = 0.;
  //! measured frequency
  float m_freq;
  //! The oscillator temperature recorded in the last reading for freq. correction (defaults to -273.15)
  float m_temperatureOsc = -273.15;
  //! nominal capacitance
  float m_Cnom = 180.e-12;
  //! stray capacitance
  float m_Coffset = 20.e-12;
  //! T-slope for frequency correction
  float m_Tslope = 2.e-4;
  //! resistors in the oscillator circutry
  float m_Rosc1=468.8e3, m_Rosc2=50.e3;
  //! ref. to the temperature sensor from which temp. correction is derived
  std::shared_ptr<ClimateSensor> m_tempSensA=nullptr;
  //! ref. to the temperature sensor from which freq. correction is derived
  std::shared_ptr<ClimateSensor> m_tempSensF=nullptr;
};

#endif // HS1101LF_H
