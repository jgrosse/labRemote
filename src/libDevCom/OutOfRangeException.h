#ifndef OUTOFRANGEEXCEPTION_H
#define OUTOFRANGEEXCEPTION_H

#include <iostream>
#include  <cstdint>

#include "ComException.h"

class OutOfRangeException : public ComException
{
public:
  OutOfRangeException(uint32_t ch, uint32_t minCh, uint32_t maxCh);

  virtual const char* what() const throw();

private:
  uint32_t m_ch;
  uint32_t m_minCh;
  uint32_t m_maxCh;;

  std::string m_msg;  
};

#endif // OUTOFRANGEEXCEPTION_H
