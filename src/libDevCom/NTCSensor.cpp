#include "NTCSensor.h"
#include "DeviceComRegistry.h"
REGISTER_DEVCOM(NTCSensor, ClimateSensor)

#include "NotSupportedException.h"

#include <cmath>

#include "ClimateSensorRegistry.h"
REGISTER_CLIMATESENSOR(NTCSensor)

NTCSensor::NTCSensor(uint8_t chan, std::shared_ptr<ADCDevice> dev, bool steinhart, float Tref_A, float Rref_B, float Bntc_C,
	             bool readoverntc, float Rdiv, float Vsup)
: ClimateSensorAnalog("unknown"), m_chan(chan), m_Tref(Tref_A), m_Rref(Rref_B), m_Bntc(Bntc_C), 
  m_shA(Tref_A), m_shB(Rref_B), m_shC(Bntc_C), m_Rdiv(Rdiv), m_Vsup(Vsup), m_readoverntc(readoverntc), m_steinhart(steinhart)
{
  setADC(dev);
}

NTCSensor::NTCSensor(std::string name)
  : ClimateSensorAnalog(name), m_temperature(-273.15), 
    m_chan(0), m_Tref(298.15), m_Rref(10000.0), m_Bntc(3435.0), 
    m_shA(m_Tref), m_shB(m_Rref), m_shC(m_Bntc),
    m_Rdiv(10000.0), m_Vsup(5.0), m_readoverntc(false), m_steinhart(false)
{ }

NTCSensor::~NTCSensor()
{ }

void NTCSensor::setConfiguration(const nlohmann::json& config)
{
  for (const auto &kv : config.items())
    {
      if(kv.key()=="chan")
        {
          m_chan=kv.value();
        }
      if(kv.key()=="Tref")
        {
          m_Tref=kv.value();
          m_shA=m_Tref;
        }
      if(kv.key()=="Rref")
        {
          m_Rref=kv.value();
          m_shB=m_Rref;
        }
      if(kv.key()=="Bntc")
        {
          m_Bntc=kv.value();
          m_shC=m_Bntc;
        }
      if(kv.key()=="Rdiv")
        {
          m_Rdiv=kv.value();
        }
      if(kv.key()=="Vsup")
        {
          m_Vsup=kv.value();
        }
      if(kv.key()=="readoverntc")
        {
          m_readoverntc=kv.value();
        }
      if(kv.key()=="steinhart")
        {
          m_steinhart=kv.value();
        }
    }
}

void NTCSensor::init()
{ }

void NTCSensor::reset()
{ }

void NTCSensor::read()
{
  if(m_adcdev==nullptr) throw std::runtime_error("Error (sensor "+m_name+"): pointer to ADC device is NULL.");

  m_temperature = RtoC(getRntc(m_adcdev->read(m_chan)));
}

float NTCSensor::temperature() const
{
  return m_temperature;
}

float NTCSensor::humidity() const
{
  throw NotSupportedException("Humidity not supported for NTC");
  return 0;
}

float NTCSensor::pressure() const
{
  throw NotSupportedException("Pressure not supported for NTC");
  return 0;
}

float NTCSensor::getRntc(float Vout)
{
  // If there is a reading error, e.g. open circuit, return a default value
  float Rntc = std::numeric_limits<float>::max(); //3.40282e+38
  if (!m_readoverntc)
  {
    if(Vout<=0.01*m_Vsup) return std::numeric_limits<float>::max();
    Rntc = m_Rdiv*(m_Vsup/Vout-1.);
  } else {
    if(Vout>=0.99*m_Vsup) return std::numeric_limits<float>::max();
    Rntc = m_Rdiv*Vout/(m_Vsup-Vout);
  }
  return Rntc;
}

float NTCSensor::RtoC(float Rntc)
{
  if (Rntc >= 9.e+37) return -273.15;
  if (m_steinhart)
  {
    float logres = log(Rntc);
    float absT = 1./(m_shA+m_shB*logres+m_shC*pow(logres,3));
    return absT-273.15;
  }
  return m_Bntc*m_Tref/(m_Tref*std::log(Rntc/m_Rref)+m_Bntc) - 273.15;
}
