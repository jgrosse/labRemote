#ifndef CLIMATESENSOR_H
#define CLIMATESENSOR_H

#include <nlohmann/json.hpp>

//! \brief Abstract implementation of a climate sensor
/** 
 * A climate sensor is defined as a sensor that can measure 
 * any one of the following properties.
 *  - temperature
 *  - humidity
 *  - pressure
 *
 * If a given sensor does not support a certain property, then
 * it should throw the `NotSupported` exception when trying to
 * retrieve the given property.
 * 
 * All classes that inherit from this class have Python bindings
 */
class ClimateSensor
{
public:
  ClimateSensor(std::string name = "unknown") {m_name = name;};
  virtual ~ClimateSensor() =default;

  //! \brief Configure sensor based on JSON object
  virtual void setConfiguration(const nlohmann::json& config) =0;

  //! \brief Initialize the sensor
  /**
   * Setup any necessary sensor configuration here.
   */
  virtual void init() =0;

  //! \brief Reset the sensor
  virtual void reset() =0;

  //! \brief Read the sensor measurement
  /**
   * Sensor climate measurements should be retrieved here
   * and stored inside private variables.
   */
  virtual void read() =0;

  //! \brief Last measured temperature (Celsius)
  /**
   * Raise `NotSupported` exception if the sensor 
   * cannot measure temperature.
   */
  virtual float temperature() const =0;

  //! \brief Last measured relative humidity (0 to 1)
  /**
   * Raise `NotSupported` exception if the sensor 
   * cannot measure humidity.
   */  
  virtual float humidity() const =0;

  //! \brief Last measured pressure (Pascal)
  /**
   * Raise `NotSupported` exception if the sensor 
   * cannot measure pressure.
   */  
  virtual float pressure() const =0;

  //! \brief Dew point corresponding to the last measured temperature and humidity
  /**
   * At zero (measured) relative humidity, the smallest possible float
   * is returned.
   *
   * \return Dew poitn in [°C]
   */
  float dewPoint();

 protected:
  std::string m_name;

};

#endif // CLIMATESENSOR_H
