#ifndef CLIMATESENSORANALOG_H
#define CLIMATESENSORANALOG_H

#include "ClimateSensor.h"
#include "ADCDevice.h"

//! \brief Abstract implementation of a climate sensor with analog read-out via ADC class
/** 
 * A climate sensor is defined as a sensor that can measure 
 * any one of the following properties.
 *  - temperature
 *  - humidity
 *  - pressure
 *
 * If a given sensor does not support a certain property, then
 * it should throw the `NotSupported` exception when trying to
 * retrieve the given property.
 *
 * This class inherits from ClimateSensor, but provides a generic
 * function to load an ADC interface needed for all analog sensors
 * 
 * All classes that inherit from this class have Python bindings
 */
class ClimateSensorAnalog : public ClimateSensor
{
public:
  ClimateSensorAnalog(std::string name = "unknown");
  virtual ~ClimateSensorAnalog() =default;

  //! \brief set ADC interface required for reading sensor
  void setADC(std::shared_ptr<ADCDevice> dev);

  //! \brief Configure sensor based on JSON object
  virtual void setConfiguration(const nlohmann::json& config) =0;

  //! \brief Initialize the sensor
  /**
   * Setup any necessary sensor configuration here.
   */
  virtual void init() =0;

  //! \brief Reset the sensor
  virtual void reset() =0;

  //! \brief Read the sensor measurement
  /**
   * Sensor climate measurements should be retrieved here
   * and stored inside private variables.
   */
  virtual void read() =0;

  //! \brief Last measured temperature (Celsius)
  /**
   * Raise `NotSupported` exception if the sensor 
   * cannot measure temperature.
   */
  virtual float temperature() const =0;

  //! \brief Last measured relative humidity (0 to 1)
  /**
   * Raise `NotSupported` exception if the sensor 
   * cannot measure humidity.
   */  
  virtual float humidity() const =0;

  //! \brief Last measured pressure (Pascal)
  /**
   * Raise `NotSupported` exception if the sensor 
   * cannot measure pressure.
   */  
  virtual float pressure() const =0;

 protected:
  //! Interface with the Arduino via the ADC protocol
  std::shared_ptr<ADCDevice> m_adcdev;
};

#endif // CLIMATESENSORANALOG_H
