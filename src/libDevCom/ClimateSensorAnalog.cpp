#include "ClimateSensorAnalog.h"

ClimateSensorAnalog::ClimateSensorAnalog(std::string name)
  : ClimateSensor(name), m_adcdev(nullptr)
{
}
void ClimateSensorAnalog::setADC(std::shared_ptr<ADCDevice> dev)
{
  m_adcdev = dev;
}

