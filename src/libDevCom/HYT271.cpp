#include "HYT271.h"
#include "DeviceComRegistry.h"
REGISTER_DEVCOM(HYT271, ClimateSensor)

#include <unistd.h>
#include <math.h>

#include "NotSupportedException.h"
#include "ChecksumException.h"

HYT271::HYT271(std::shared_ptr<I2CCom> i2c)
  : m_i2c(i2c)
{ }

HYT271::~HYT271()
{ }

void HYT271::setConfiguration(const nlohmann::json& config)
{ }

void HYT271::init()
{ }

void HYT271::reset()
{ }

void HYT271::read()
{
  m_i2c->write_reg8(0x00);  // Send any value to start measurement
  usleep(120e3);            // Conversion time is between 60ms to 100ms

  std::vector<uint8_t> data(4);
  m_i2c->read_block(data);

  if ((data[0] & 0x40) != 0) {
    // Got no new data - try re-reading after waiting a bit longer
    usleep(200e3);
    m_i2c->read_block(data);
  }
  if ((data[0] & 0x40) != 0) {
    // Something is wrong
    throw ComIOException("failed to read new data from HYT271 sensor");
  }
  m_humidity = ((data[0] & 0x3f) << 8 | data[1]) * (100.0 / (pow(2,14)-1.));
  m_temperature = ((data[2] << 8 | (data[3] & 0xfc)) >> 2) * (165.0 / (pow(2,14)-1.)) - 40.;
}

uint HYT271::status() const
{ return m_status; }

float HYT271::temperature() const
{ return m_temperature; }

float HYT271::humidity() const
{ return m_humidity; }

float HYT271::pressure() const
{ throw NotSupportedException("HYT271 does not have a pressure sensor"); return 0; }
