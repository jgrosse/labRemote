#ifndef DACDEVICE_H
#define DACDEVICE_H

#include <cstdint>
#include <vector>
#include <memory>
#include <map>

#include "DeviceCalibration.h"
#include "DummyCalibration.h"

//! \brief Abstract implementation of an digital to analogue converter
/**
 * The `DACDevice` class serves two functions:
 * - Conversion from volts to counts using the `DeviceCalibration` classes.
 * - Defines an abstract interface for writing DAC values in counts.
 *
 * Multi-channel DAC's are supported via an optional channel argument to 
 * most functions.
 *
 * ## Calibration
 *
 * Calibration handles converting requested output values to raw DAC counts that
 * are then written to the device.
 *
 * The units themselves are not defined and don't have to corresponds to the
 * to the voltage on the DAC output. For example, consider a DAC output that
 * is reduced using a voltage divider. The calibration defined as the reduced
 * value.
 *
 * For multi-channel DAC's, a per-channel calibration can be applied using the 
 * `setCalibration(ch,...)` function. If no per-channel calibration is set for
 * a specific channel, then the global calibration (set via constructor or
 * `setCalibration(...)`) is used.
 */
class DACDevice
{
public:
  /**
   * \param calibration Global calibration for all channels.
   */  
  DACDevice(std::shared_ptr<DeviceCalibration> calibration=std::make_shared<DummyCalibration>());
  virtual ~DACDevice();

  //! Set global calibration
  /**
   * \param calibration Global calibration for all channels.
   */    
  void setCalibration(std::shared_ptr<DeviceCalibration> calibration);

  //! Set per-channel calibration
  /**
   * Override global calibration for specific channels.
   *
   * \param ch Channel to calibrate
   * \param calibration Calibration for `ch`
   */ 
  void setCalibration(uint8_t ch, std::shared_ptr<DeviceCalibration> calibration);

  //! Write DAC value (counts) of "default" channel
  /**
   * Implementation of writing to the DAC goes here
   */  
  virtual void setCount(int32_t counts) =0;

  //! Write DAC value (counts) of channel `ch`
  /**
   * Implementation of writing to the DAC goes here
   */    
  virtual void setCount(uint8_t ch, int32_t counts) =0;

  //! Bulk write DAC value (counts) of channels `chs`
  /**
   * Implementation of writing to the DAC goes here
   *
   * \param chs List of channels to write
   * \param counts DAC outputs corresponding to channels in `chs`
   */      
  virtual void setCount(const std::vector<uint8_t>& chs, const std::vector<int32_t>& counts) =0;

  //! Read current DAC value of "default" channel (counts)
  /**
   * Implementation of reading from the DAC goes here
   */
  virtual int32_t readCount() =0;

  //! Read current DAC value channel `ch` (counts)
  /**
   * Implementation of reading from the DAC goes here
   */
  virtual int32_t readCount(uint8_t ch) =0;

  //! Bulk read current DAC values of channels `chs` (counts)
  /**
   * Implementation of reading from the DAC goes here
   * 
   * The `data` vector used to store the results does not 
   * need to be the same size as `chs`. The implementatino
   * of `readCount` should clear and allocated the vector
   * to the right size.
   *
   * \param chs List of channel numbers to read
   * \param data Vector where readings will be stored
   */  
  virtual void    readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& data) =0;

  //! Set DAC output value of "default" channel (volts)
  /**
   * Call `setCount` using per-channel calibration.
   */  
  double set(double value);

  //! Set DAC output value of channel `ch` (volts)
  /**
   * Call `setCount` using per-channel calibration.
   */  
  double set(uint8_t ch, double value);

  //! Set DAC output values for channels `chs` (volts)
  /**
   * Call `setCount` using per-channel calibration.
   */
  void   set(const std::vector<uint8_t>& chs, const std::vector<double>& values);

  //! Read calibrated current DAC value of "default" channel (volts)
  /**
   * \return result of `readCount`, followed by calibration.
   */
  double read();

  //! Read calibrated current DAC value of channel `ch` (volts)
  /**
   * \return result of `readCount`, followed by calibration.
   */
  double read(uint8_t ch);

  //! Read calibrated current DAC value of channels `chs` (volts)
  /**
   * \return result of `readCount`, followed by calibration.
   */
  void   read(const std::vector<uint8_t>& chs, std::vector<double>& data);

private:
  //! Find `DeviceCalibration` for given channel
  /**
   * The global calibration is returned if `ch` does not have a per-channel calibration
   * set.
   *
   * \return The calibration to be used for the given channel. (per-channel or global)
   */
  std::shared_ptr<DeviceCalibration> findCalibration(uint8_t ch) const;

  std::shared_ptr<DeviceCalibration> m_calibration;
  std::map<uint8_t, std::shared_ptr<DeviceCalibration>> m_channelCalibration;
};

#endif // DACDEVICE_H
