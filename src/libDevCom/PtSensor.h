#ifndef PTSENSOR_H
#define PTSENSOR_H

#include <string>
#include <cstdint>

#include "ClimateSensorAnalog.h"

//! \brief An implementation of 'ClimateSensorAnalog' for reading an Pt temperature sensor
/**
 * Measures temperature using an Pt sensor (Pt100, Pt1000 etc.) connected to one of the 
 * analog channelss on an Arduino via an ADC intereface.
 * PT sensors are available from various sources; this code was tested with a Pt1000 from TE -
 * specs: https://www.te.com/global-en/product-NB-PTCO-157.html
 */
class PtSensor : public ClimateSensorAnalog
{
public:
  /**
   * \param chan The channel number of the analog channel connected to the Pt
   * \param dev The ADCDevice for interfacing with the Adruino or similar - should be calibrated
   *        to return measured voltage as 0...100% of the operational voltage
   * \param Rref The Pt's resistance at 0degC
   * \param Apt The value of the linear calibration coefficient
   * \param Bpt The value of the quadratic calibration coefficient
   * \param Rdiv The resistance of the resistor in the voltage divider
   *   (the Pt and the resistor)
   */
  PtSensor(uint8_t chan, std::shared_ptr<ADCDevice> dev, float Rref=1.e3, 
	   float Apt=3.9083e-3, float Bpt=-5.775e-7, float Rdiv=1.e3, float Vsup=5.0,
	   bool wOpAmp=false, float Ampl=6.8, float Vor=1.);

  /* \brief constructor needed for factory
   *
   */
  PtSensor(std::string name);

  virtual ~PtSensor();

  /* \brief Configure sensor based on JSON object
   *
   * Valid keys:
   *  - `chan`: The channel number of the analog channel connected to the Pt sensor (the Pt and the resistor)
   *  - `Rref`: The Pt's resistance at 0degC
   *  - `Rdiv`: The resistance of the resistor in the voltage divider
   *  - `Apt`: The value of the linear calibration coefficient
   *  - `Bpt`: The value of the quadratic calibration coefficient
   *  - 'wOpAmp': OpAmp is used to amplify voltage swing
   *  - 'Ampl': Amplification if OpAmp is used
   *  - 'Vor': Ratio of resistors to define voltage level at + input of OpAmp
   *
   * \param config JSON configuration
   */
  virtual void setConfiguration(const nlohmann::json& config);

  //! Does nothing (requried to implement ClimateSensor)
  virtual void init();
  //! Does nothing (required to implement ClimateSensor)
  virtual void reset();
  //! Reads the temperature from the NTC and stores it in m_temperature
  virtual void read();

  //! Returns the temperature (in Celsius) from the most recent reading
  //  (defaults to -273.15)
  virtual float temperature() const;
  //! Throws a Not Supported exception (required to implement ClimateSensor)
  virtual float humidity() const;
  //! Throws a Not Supported exception (required to implement ClimateSensor
  virtual float pressure() const;

private:
  //! The resistance of the Pt at 0degC
  float m_Rref = 1.e3;
  //! The value of the linear calibration coefficient
  float m_Apt = 3.9083e-3;
  //! The value of the quadratic calibration coefficient
  float m_Bpt = -5.775e-7;
  //! The resistance of the resistor in the voltage divider circuit
  float m_Rdiv = 1.e3;
  //! The total voltage across the voltage divider (the Pt and the resistor)
  float m_Vsup=5.0;
  //! false: read from voltage divider, true: read from OpAmp
  bool m_wOpAmp=false;
  //! only for OpAmp: amplification
  float m_Ampl=6.8;
  //! only for OpAmp: voltage divider on "+" input (ratio of resistor values)
  float m_Offs=1.;
  //! The channel of the ADCS connected to the Pt
  uint8_t m_chan = 0;
  //! The temperature recorded in the last reading (defaults to -273.15)
  float m_temperature = -273.15;

  //! Converts from the raw readout of the Arduino channel to temperature in Celsius
  float raw_to_C(float volt);
};

#endif // PTSENSOR_H
