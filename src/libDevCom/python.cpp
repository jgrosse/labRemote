#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "DeviceComRegistry.h"

#include "ClimateSensor.h"
#include "NTCSensor.h"
#include "HTS221.h"
#include "Si7021.h"
#include "SHT85.h"
#include "HYT271.h"
#include "HIH4000.h"
#include "HIH6130.h"

#include "DeviceCom.h"
#include "I2CCom.h"
#include "I2CDevCom.h"
#include "I2CDevComuino.h"


#include "PCA9548ACom.h"
#ifdef ENABLE_FTDI
  #include "I2CFTDICom.h"
  #include "MPSSEChip.h"
  #include "FT232H.h"
#endif

#include "DeviceCalibration.h"
#include "DummyCalibration.h"
#include "LinearCalibration.h"
#include "FileCalibration.h"

#include "ADCDevComuino.h"
#include "ADCDevice.h"
#include "AD799X.h"
#include "LTC2451.h"
#include "MAX11619.h"
#include "MCP3425.h"
#include "MCP3428.h"

#include "DACDevice.h"
#include "AD56X9.h"
#include "DAC5571.h"
#include "DAC5574.h"
#include "MCP4801.h"

#include "TextSerialCom.h"

namespace py = pybind11;


class PyClimateSensor : public ClimateSensor {
public:
    using ClimateSensor::ClimateSensor;

    void setConfiguration(const  nlohmann::json& config) override {
        PYBIND11_OVERLOAD(
            void,
            ClimateSensor, 
            setConfiguration,
            config
        );
    }

    void init() override {
        PYBIND11_OVERLOAD_PURE(
            void,
            ClimateSensor,
            init
        );
    }
    
    void reset() override {
        PYBIND11_OVERLOAD_PURE(
            void,
            ClimateSensor,
            reset
        );
    }
    
    void read() override {
        PYBIND11_OVERLOAD_PURE(
            void,
            ClimateSensor,
            read
        );
    }
    
    float temperature() const override {
        PYBIND11_OVERLOAD_PURE(
            float const,
            ClimateSensor,
            temperature
        );
    }
    
    float humidity() const override {
        PYBIND11_OVERLOAD_PURE(
            float,
            ClimateSensor,
            humidity
        );
    }
    
    float pressure() const override {
        PYBIND11_OVERLOAD_PURE(
            float,
            ClimateSensor,
            pressure
        );
    }
};

template <class Sensor = NTCSensor> class PySensor: public Sensor {
public:
    using Sensor::Sensor;

    void init() override {
        PYBIND11_OVERLOAD(
            void,
            Sensor,
            init
        );
    }
    
    void reset() override {
        PYBIND11_OVERLOAD(
            void,
            Sensor,
            reset
        );
    }
    
    void read() override {
        PYBIND11_OVERLOAD(
            void,
            Sensor,
            read
        );
    }
    
    float temperature() const override {
        PYBIND11_OVERLOAD(
            float const,
            Sensor,
            temperature
        );
    }
    
    float humidity() const override {
        PYBIND11_OVERLOAD(
            float,
            Sensor,
            humidity
        );
    }
    
    float pressure() const override {
        PYBIND11_OVERLOAD(
            float,
            Sensor,
            pressure
        );
    }
    float getRntc(float Vout) {
        PYBIND11_OVERLOAD(
            float,
            Sensor,
            getRntc,
            Vout
        );
    }
};

template <class Sensor = SHT85> class PySensor_w_status: public Sensor {
public:
    using Sensor::Sensor;

    void init() override {
        PYBIND11_OVERLOAD(
            void,
            Sensor,
            init
        );
    }
    
    void reset() override {
        PYBIND11_OVERLOAD(
            void,
            Sensor,
            reset
        );
    }
    
    void read() override {
        PYBIND11_OVERLOAD(
            void,
            Sensor,
            read
        );
    }
    
    unsigned status() const override {
        PYBIND11_OVERLOAD(
            unsigned const,
            Sensor,
            status
        );
    }
    
    float temperature() const override {
        PYBIND11_OVERLOAD(
            float const,
            Sensor,
            temperature
        );
    }
    
    float humidity() const override {
        PYBIND11_OVERLOAD(
            float,
            Sensor,
            humidity
        );
    }
    
    float pressure() const override {
        PYBIND11_OVERLOAD(
            float,
            Sensor,
            pressure
        );
    }
};

template <class Com = DeviceCom> class PyPureVirtualCom : public Com {
public:
    using Com::Com;

    void write_reg32(uint32_t address, uint32_t data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            Com,
            write_reg32,
            address,
            data
        );
    }

    void write_reg16(uint32_t address, uint16_t data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            Com,
            write_reg16,
            address,
            data
        );
    }

    void write_reg8(uint32_t address, uint8_t data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            Com,
            write_reg8,
            address,
            data
        );
    }

    void write_reg32(uint32_t data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            Com,
            write_reg32,
            data
        );
    }

    void write_reg16(uint16_t data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            Com,
            write_reg16,
            data
        );
    }

    void write_reg8(uint8_t data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            Com,
            write_reg8,
            data
        );
    }

    void write_block(uint32_t address, const std::vector<uint8_t>& data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            Com,
            write_block,
            address,
            data
        );
    }

    void write_block(const std::vector<uint8_t>& data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            Com,
            write_block,
            data
        );
    }

    uint32_t read_reg32(uint32_t address) override {
        PYBIND11_OVERLOAD_PURE(
            uint32_t,
            Com,
            read_reg32,
            address
        );
    }

    uint32_t read_reg24(uint32_t address) override {
        PYBIND11_OVERLOAD_PURE(
            uint32_t,
            Com,
            read_reg24,
            address
        );
    }

    uint16_t read_reg16(uint32_t address) override {
        PYBIND11_OVERLOAD_PURE(
            uint16_t,
            Com,
            read_reg16,
            address
        );
    }

    uint8_t read_reg8(uint32_t address) override {
        PYBIND11_OVERLOAD_PURE(
            uint8_t,
            Com,
            read_reg8,
            address
        );
    }

    uint32_t read_reg32() override {
        PYBIND11_OVERLOAD_PURE(
            uint32_t,
            Com,
            read_reg32
        );
    }

    uint32_t read_reg24() override {
        PYBIND11_OVERLOAD_PURE(
            uint32_t,
            Com,
            read_reg24
        );
    }

    uint16_t read_reg16() override {
        PYBIND11_OVERLOAD_PURE(
            uint16_t,
            Com,
            read_reg16
        );
    }

    uint8_t read_reg8() override {
        PYBIND11_OVERLOAD_PURE(
            uint8_t,
            Com,
            read_reg8
        );
    }

    void read_block(uint32_t address, std::vector<uint8_t>& data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            Com,
            read_block,
            address,
            data
        );
    }

    void read_block(std::vector<uint8_t>& data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            Com,
            read_block,
            data
        );
    }
};

template <class Com = I2CDevCom> class PyCom : public Com {
public:
    using Com::Com;

    void write_reg32(uint32_t address, uint32_t data) override {
        PYBIND11_OVERLOAD(
            void,
            Com,
            write_reg32,
            address,
            data
        );
    }

    void write_reg16(uint32_t address, uint16_t data) override {
        PYBIND11_OVERLOAD(
            void,
            Com,
            write_reg16,
            address,
            data
        );
    }

    void write_reg8(uint32_t address, uint8_t data) override {
        PYBIND11_OVERLOAD(
            void,
            Com,
            write_reg8,
            address,
            data
        );
    }

    void write_reg32(uint32_t data) override {
        PYBIND11_OVERLOAD(
            void,
            Com,
            write_reg32,
            data
        );
    }

    void write_reg16(uint16_t data) override {
        PYBIND11_OVERLOAD(
            void,
            Com,
            write_reg16,
            data
        );
    }

    void write_reg8(uint8_t data) override {
        PYBIND11_OVERLOAD(
            void,
            Com,
            write_reg8,
            data
        );
    }

    void write_block(uint32_t address, const std::vector<uint8_t>& data) override {
        PYBIND11_OVERLOAD(
            void,
            Com,
            write_block,
            address,
            data
        );
    }

    void write_block(const std::vector<uint8_t>& data) override {
        PYBIND11_OVERLOAD(
            void,
            Com,
            write_block,
            data
        );
    }

    uint32_t read_reg32(uint32_t address) override {
        PYBIND11_OVERLOAD(
            uint32_t,
            Com,
            read_reg32,
            address
        );
    }

    uint32_t read_reg24(uint32_t address) override {
        PYBIND11_OVERLOAD(
            uint32_t,
            Com,
            read_reg24,
            address
        );
    }

    uint16_t read_reg16(uint32_t address) override {
        PYBIND11_OVERLOAD(
            uint16_t,
            Com,
            read_reg16,
            address
        );
    }

    uint8_t read_reg8(uint32_t address) override {
        PYBIND11_OVERLOAD(
            uint8_t,
            Com,
            read_reg8,
            address
        );
    }

    uint32_t read_reg32() override {
        PYBIND11_OVERLOAD(
            uint32_t,
            Com,
            read_reg32
        );
    }

    uint32_t read_reg24() override {
        PYBIND11_OVERLOAD(
            uint32_t,
            Com,
            read_reg24
        );
    }

    uint16_t read_reg16() override {
        PYBIND11_OVERLOAD(
            uint16_t,
            Com,
            read_reg16
        );
    }

    uint8_t read_reg8() override {
        PYBIND11_OVERLOAD(
            uint8_t,
            Com,
            read_reg8
        );
    }

    void read_block(uint32_t address, std::vector<uint8_t>& data) override {
        PYBIND11_OVERLOAD(
            void,
            Com,
            read_block,
            address,
            data
        );
    }

    void read_block(std::vector<uint8_t>& data) override {
        PYBIND11_OVERLOAD(
            void,
            Com,
            read_block,
            data
        );
    }
};

class PyDeviceCalibration : public DeviceCalibration {
public:
    using DeviceCalibration::DeviceCalibration;
    double calibrate(int32_t counts) override {
        PYBIND11_OVERLOAD_PURE(
            double,
            DeviceCalibration,
            calibrate,
            counts
        );
    }
    int32_t uncalibrate(double value) override {
        PYBIND11_OVERLOAD_PURE(
            int32_t,
            DeviceCalibration,
            uncalibrate,
            value
        );
    }   
};

template <class ADC = ADCDevice> class PyADC : public ADC {
public:
    using ADC::ADC;

    void setCalibration(std::shared_ptr<DeviceCalibration> calibration) {
        PYBIND11_OVERLOAD(
            void,
            ADC,
            setCalibration,
            calibration
        );
    }

    void setCalibration(uint8_t ch, std::shared_ptr<DeviceCalibration> calibration) {
        PYBIND11_OVERLOAD(
            void,
            ADC,
            setCalibration,
            ch,
            calibration
        );
    }
    
    double read() {
        PYBIND11_OVERLOAD(
            double,
            ADC,
            read
        );
    }
    
    double read(uint8_t ch) {
        PYBIND11_OVERLOAD(
            double,
            ADC,
            read,
            ch
        );
    }
    
    void read(const std::vector<uint8_t>& chs, std::vector<double>& data) {
        PYBIND11_OVERLOAD(
            void,
            ADC,
            read,
            chs,
            data
        );
    }
    
    int32_t readCount() override {
        PYBIND11_OVERLOAD_PURE(
            int32_t,
            ADC,
            readCount
        );
    }
    
    int32_t readCount(uint8_t ch) override {
        PYBIND11_OVERLOAD_PURE(
            int32_t,
            ADC,
            readCount,
            ch
        );
    }

    void readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            ADC,
            readCount,
            chs,
            data
        );
    }
};

#ifdef ENABLE_FTDI
class PyMPSSEChip : public MPSSEChip {
public:
    using MPSSEChip::MPSSEChip;

    bool start() override {
        PYBIND11_OVERLOAD_PURE(
            bool,
            MPSSEChip,
            start
        );
    }

    bool stop() override {
        PYBIND11_OVERLOAD_PURE(
            bool,
            MPSSEChip,
            stop
        );
    }

    bool write(char* data, int size) override {
        PYBIND11_OVERLOAD_PURE(
            bool,
            MPSSEChip,
            write,
            data,
            size
        );
    }

    char* read(int size) override {
        PYBIND11_OVERLOAD_PURE(
            char*,
            MPSSEChip,
            read,
            size
        );
    }

    void gpio_write(int pin, int state) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            MPSSEChip,
            gpio_write,
            pin,
            state
        );
    }

    bool get_ack() override {
        PYBIND11_OVERLOAD_PURE(
            bool,
            MPSSEChip,
            get_ack
        );
    }

    void set_ack() override {
        PYBIND11_OVERLOAD_PURE(
            void,
            MPSSEChip,
            set_ack
        );
    }

    void set_nack() override {
        PYBIND11_OVERLOAD_PURE(
            void,
            MPSSEChip,
            set_nack
        );
    }
};

template <class FTDIDevice = FT232H> class PyFTDIDevice : public FTDIDevice {
public:
    using FTDIDevice::FTDIDevice;

    bool start() override {
        PYBIND11_OVERLOAD(
            bool,
            FTDIDevice,
            start
        );
    }

    bool stop() override {
        PYBIND11_OVERLOAD(
            bool,
            FTDIDevice,
            stop
        );
    }

    bool write(char* data, int size) override {
        PYBIND11_OVERLOAD(
            bool,
            FTDIDevice,
            write,
            data,
            size
        );
    }

    char* read(int size) override {
        PYBIND11_OVERLOAD(
            char*,
            FTDIDevice,
            read,
            size
        );
    }

    void gpio_write(int pin, int state) override {
        PYBIND11_OVERLOAD(
            void,
            FTDIDevice,
            gpio_write,
            pin,
            state
        );
    }

    bool get_ack() override {
        PYBIND11_OVERLOAD(
            bool,
            FTDIDevice,
            get_ack
        );
    }

    void set_ack() override {
        PYBIND11_OVERLOAD(
            void,
            FTDIDevice,
            set_ack
        );
    }

    void set_nack() override {
        PYBIND11_OVERLOAD(
            void,
            FTDIDevice,
            set_nack
        );
    }
};
#endif

template <class DeviceCalibrationBase = DeviceCalibration> class PyDeviceCalibrationBase : public DeviceCalibrationBase
{
public:
    using DeviceCalibrationBase::DeviceCalibrationBase;
    double calibrate(int32_t counts) override { PYBIND11_OVERLOAD_PURE(double, DeviceCalibrationBase, calibrate, counts); }
    int32_t uncalibrate(double value) override { PYBIND11_OVERLOAD_PURE(int32_t, DeviceCalibrationBase, uncalibrate, value); }
};

template <class DevCal = DummyCalibration> class PyDevCal : public DevCal //public PyDeviceCalibration<DevCal>
{
public:
    using DevCal::DevCal;
    double calibrate(int32_t counts) override { PYBIND11_OVERLOAD(double, DevCal, calibrate, counts); }
    int32_t uncalibrate(double value) override { PYBIND11_OVERLOAD(int32_t, DevCal, uncalibrate, value); }
};

template <class ADCDeviceBase = ADCDevice> class PyADCDeviceBase : public ADCDeviceBase
{
public:
    using ADCDeviceBase::ADCDeviceBase;

    void setCalibration(std::shared_ptr<DeviceCalibration> calibration) {
        PYBIND11_OVERLOAD(
            void,
            ADCDeviceBase,
            setCalibration,
            calibration
        );
    }

    void setCalibration(uint8_t ch, std::shared_ptr<DeviceCalibration> calibration) {
        PYBIND11_OVERLOAD(
            void,
            ADCDeviceBase,
            setCalibration,
            ch,
            calibration
        );
    }

    double read() {
        PYBIND11_OVERLOAD(
            double,
            ADCDeviceBase,
            read
        );
    }

    double read(uint8_t ch) {
        PYBIND11_OVERLOAD(
            double,
            ADCDeviceBase,
            read,
            ch
        );
    }

    void read(const std::vector<uint8_t>& chs, std::vector<double>& data) {
        PYBIND11_OVERLOAD(
            void,
            ADCDeviceBase,
            read,
            chs,
            data
        );
    }

    int32_t readCount() override {
        PYBIND11_OVERLOAD_PURE(
            int32_t,
            ADCDeviceBase,
            readCount
        );
    }

    int32_t readCount(uint8_t channel) override {
        PYBIND11_OVERLOAD_PURE(
            int32_t,
            ADCDeviceBase,
            readCount,
            channel
        );
    }

    void readCount(const std::vector<uint8_t>& channels, std::vector<int32_t>& data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            ADCDeviceBase,
            readCount,
            channels,
            data
        );
    }
    
}; // class PyADCDeviceBase

template <class ADCDev = ADCDevice> class PyADCDev : public ADCDev
{
public:
    using ADCDev::ADCDev;

    int32_t readCount() override {
        PYBIND11_OVERLOAD(
            int32_t,
            ADCDev,
            readCount
        );
    }

    int32_t readCount(uint8_t channel) override {
        PYBIND11_OVERLOAD(
            int32_t,
            ADCDev,
            readCount,
            channel
        );
    }

    void readCount(const std::vector<uint8_t>& channels, std::vector<int32_t>& data) override {
        PYBIND11_OVERLOAD(
            void,
            ADCDev,
            readCount,
            channels,
            data
        );
    }
    
}; // class PyADCDev

template <class DACDeviceBase = DACDevice> class PyDACDeviceBase : public DACDeviceBase
{
public:
    using DACDeviceBase::DACDeviceBase;

    void setCalibration(std::shared_ptr<DeviceCalibration> calibration) {
        PYBIND11_OVERLOAD(
            void,
            DACDeviceBase,
            setCalibration,
            calibration
        );
    }

    void setCalibration(uint8_t ch, std::shared_ptr<DeviceCalibration> calibration) {
        PYBIND11_OVERLOAD(
            void,
            DACDeviceBase,
            setCalibration,
            ch,
            calibration
        );
    }

    void setCount(int32_t counts) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            DACDeviceBase,
            setCount,
            counts
        );
    }

    void setCount(uint8_t ch, int32_t counts) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            DACDeviceBase,
            setCount,
            ch,
            counts
        );
    }

    void setCount(const std::vector<uint8_t>& chs, const std::vector<int32_t>& counts) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            DACDeviceBase,
            setCount,
            chs,
            counts
        );
    }

    int32_t readCount() override {
        PYBIND11_OVERLOAD_PURE(
            int32_t,
            DACDeviceBase,
            readCount
        );
    }

    int32_t readCount(uint8_t ch) override {
        PYBIND11_OVERLOAD_PURE(
            int32_t,
            DACDeviceBase,
            readCount,
            ch
        );
    }

    void readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& data) override {
        PYBIND11_OVERLOAD_PURE(
            void,
            DACDeviceBase,
            readCount,
            chs,
            data
        );
    }

    double set(double value) {
        PYBIND11_OVERLOAD(
            double,
            DACDeviceBase,
            set,
            value
        );
    }

    double set(uint8_t ch, double value) {
        PYBIND11_OVERLOAD(
            double,
            DACDeviceBase,
            set,
            ch,
            value
        );
    }

    void set(const std::vector<uint8_t>& chs, const std::vector<double>& values) {
        PYBIND11_OVERLOAD(
            void,
            DACDeviceBase,
            set,
            chs,
            values
        );
    }

    double read() {
        PYBIND11_OVERLOAD(
            double,
            DACDeviceBase,
            read
        );
    }

    double read(uint8_t ch) {
        PYBIND11_OVERLOAD(
            double,
            DACDeviceBase,
            read,
            ch
        );
    }

    void read(const std::vector<uint8_t>& chs, std::vector<double>& data) {
        PYBIND11_OVERLOAD(
            void,
            DACDeviceBase,
            read,
            chs,
            data
        );
    }
    
}; // class PyDACDeviceBase

template <class DACDev = DACDevice> class PyDACDev : public DACDev
{
public:
    using DACDev::DACDev;

    void setCount(int32_t counts) override {
        PYBIND11_OVERLOAD(
            void,
            DACDev,
            setCount,
            counts
        );
    }

    void setCount(uint8_t ch, int32_t counts) override {
        PYBIND11_OVERLOAD(
            void,
            DACDev,
            setCount,
            ch,
            counts
        );
    }

    void setCount(const std::vector<uint8_t>& chs, const std::vector<int32_t>& counts) override {
        PYBIND11_OVERLOAD(
            void,
            DACDev,
            setCount,
            chs,
            counts
        );
    }

    int32_t readCount() override {
        PYBIND11_OVERLOAD(
            int32_t,
            DACDev,
            readCount
        );
    }

    int32_t readCount(uint8_t ch) override {
        PYBIND11_OVERLOAD(
            int32_t,
            DACDev,
            readCount,
            ch
        );
    }

    void readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& data) override {
        PYBIND11_OVERLOAD(
            void,
            DACDev,
            readCount,
            chs,
            data
        );
    }

}; // class PyDACDev

void register_devcom(py::module& m) {
  py::class_<ClimateSensor, PyClimateSensor, std::shared_ptr<ClimateSensor>>(m, "ClimateSensor")
    .def(py::init<>())
    .def("init", &ClimateSensor::init)
    .def("reset", &ClimateSensor::reset)
    .def("read", &ClimateSensor::read)
    .def("temperature", &ClimateSensor::temperature)
    .def("humidity", &ClimateSensor::humidity)
    .def("pressure", &ClimateSensor::pressure)
    .def("dewPoint", &ClimateSensor::dewPoint);
 
  py::class_<NTCSensor, PySensor<NTCSensor>, ClimateSensor, std::shared_ptr<NTCSensor>>(m, "NTCSensor")
    .def(py::init<uint8_t, std::shared_ptr<ADCDevice>, bool, float, float, float, bool, float, float>());

  py::class_<HTS221, PySensor_w_status<HTS221>, ClimateSensor, std::shared_ptr<HTS221>>(m, "HTS221")
    .def(py::init<std::shared_ptr<I2CCom>>())
    .def("status", &HTS221::status);

  py::class_<Si7021, PySensor<Si7021>, ClimateSensor, std::shared_ptr<Si7021>>(m, "Si7021")
    .def(py::init<std::shared_ptr<I2CCom>>());

  py::class_<SHT85, PySensor_w_status<SHT85>, ClimateSensor, std::shared_ptr<SHT85>>(m, "SHT85")
    .def(py::init<std::shared_ptr<I2CCom>>())
    .def("status", &SHT85::status);
  
  py::class_<HYT271, PySensor_w_status<HYT271>, ClimateSensor, std::shared_ptr<HYT271>>(m, "HYT271")
    .def(py::init<std::shared_ptr<I2CCom>>())
    .def("status", &HYT271::status);
  
  py::class_<HIH4000, PySensor<HIH4000>, ClimateSensor, std::shared_ptr<HIH4000>>(m, "HIH4000")
    .def(py::init<uint8_t, std::shared_ptr<ADCDevice>, std::shared_ptr<ClimateSensor>>())
    .def(py::init<uint8_t, std::shared_ptr<ADCDevice>, std::shared_ptr<ClimateSensor>, float, float>())
    .def(py::init<uint8_t, std::shared_ptr<ADCDevice>, std::shared_ptr<ClimateSensor>, float, float, float>());
  
  py::class_<HIH6130, PySensor_w_status<HIH6130>, ClimateSensor, std::shared_ptr<HIH6130>>(m, "HIH6130")
    .def(py::init<std::shared_ptr<I2CCom>>())
    .def("status", &HIH6130::status);

  py::class_<DeviceCom, PyPureVirtualCom<DeviceCom>, std::shared_ptr<DeviceCom>>(m, "DeviceCom")
    .def(py::init<>())
    .def("write_reg32", (void (DeviceCom::*)(uint32_t address, uint32_t data)) &DeviceCom::write_reg32)
    .def("write_reg16", (void (DeviceCom::*)(uint32_t address, uint16_t data)) &DeviceCom::write_reg16)
    .def("write_reg8", (void (DeviceCom::*)(uint32_t address, uint8_t data)) &DeviceCom::write_reg8)
    .def("write_reg32", (void (DeviceCom::*)(uint32_t data)) &DeviceCom::write_reg32)
    .def("write_reg16", (void (DeviceCom::*)(uint16_t data)) &DeviceCom::write_reg16)
    .def("write_reg8", (void (DeviceCom::*)(uint8_t data)) &DeviceCom::write_reg8)
    .def("write_block", (void (DeviceCom::*)(uint32_t address, const std::vector<uint8_t>& data)) &DeviceCom::write_block)
    .def("write_block", (void (DeviceCom::*)(const std::vector<uint8_t>& data)) &DeviceCom::write_block)
    .def("read_reg32", (uint32_t (DeviceCom::*)(uint32_t address)) &DeviceCom::read_reg32)
    .def("read_reg24", (uint32_t (DeviceCom::*)(uint32_t address)) &DeviceCom::read_reg24)
    .def("read_reg16", (uint16_t (DeviceCom::*)(uint32_t address)) &DeviceCom::read_reg16)
    .def("read_reg8", (uint8_t (DeviceCom::*)(uint32_t address)) &DeviceCom::read_reg8)
    .def("read_reg32", (uint32_t (DeviceCom::*)()) &DeviceCom::read_reg32)
    .def("read_reg24", (uint32_t (DeviceCom::*)()) &DeviceCom::read_reg24)
    .def("read_reg16", (uint16_t (DeviceCom::*)()) &DeviceCom::read_reg16)
    .def("read_reg8", (uint8_t (DeviceCom::*)()) &DeviceCom::read_reg8)
    .def("read_block", (void (DeviceCom::*)(uint32_t address, std::vector<uint8_t>& data)) &DeviceCom::read_block)
    .def("read_block", (void (DeviceCom::*)(std::vector<uint8_t>& data)) &DeviceCom::read_block);
  
  py::class_<I2CCom, PyPureVirtualCom<I2CCom>, DeviceCom, std::shared_ptr<I2CCom>>(m, "I2CCom")
    .def(py::init<uint8_t>())
    .def("setDeviceAddr", &I2CCom::setDeviceAddr)
    .def("deviceAddr", &I2CCom::deviceAddr);
  
  py::class_<I2CDevCom, PyCom<I2CDevCom>, I2CCom, std::shared_ptr<I2CDevCom>>(m, "I2CDevCom")
    .def(py::init<uint8_t, const std::string&>());
  
  py::class_<I2CDevComuino, PyCom<I2CDevComuino>, I2CCom, std::shared_ptr<I2CDevComuino>>(m, "I2CDevComuino")
    .def(py::init<uint8_t, std::shared_ptr<TextSerialCom>>());
  
#ifdef ENABLE_FTDI
  py::class_<I2CFTDICom, PyCom<I2CFTDICom>, I2CCom, std::shared_ptr<I2CFTDICom>>(m, "I2CFTDICom")
    .def(py::init<std::shared_ptr<FT232H>, uint8_t>());

  py::class_<MPSSEChip, PyMPSSEChip, std::shared_ptr<MPSSEChip>> mpssechip_class(m, "MPSSEChip");
  mpssechip_class.def(py::init<MPSSEChip::Protocol, MPSSEChip::Speed, MPSSEChip::Endianness, int, int, const std::string&, const std::string&>())
    .def("start", &MPSSEChip::start)
    .def("stop", &MPSSEChip::stop)
    .def("write", &MPSSEChip::write)
    .def("read", &MPSSEChip::read)
    .def("gpio_write", &MPSSEChip::gpio_write)
    .def("get_ack", &MPSSEChip::get_ack)
    .def("set_ack", &MPSSEChip::set_ack)
    .def("set_nack", &MPSSEChip::set_nack)
    .def("to_string", &MPSSEChip::to_string)
    .def("protocol2str", &MPSSEChip::protocol2str)
    .def("speed2str", &MPSSEChip::speed2str)
    .def("endianness2str", &MPSSEChip::endianness2str)
    .def("protocol", &MPSSEChip::protocol)
    .def("speed", &MPSSEChip::speed)
    .def("endianness", &MPSSEChip::endianness);

  py::enum_<MPSSEChip::Protocol>(mpssechip_class, "Protocol")
    .value("I2C", MPSSEChip::Protocol::I2C)
    .value("SPI0", MPSSEChip::Protocol::SPI0)
    .value("SPI1", MPSSEChip::Protocol::SPI1)
    .value("SPI2", MPSSEChip::Protocol::SPI2)
    .value("SPI3", MPSSEChip::Protocol::SPI3);

  py::enum_<MPSSEChip::Speed>(mpssechip_class, "Speed")
    .value("ONE_HUNDRED_KHZ", MPSSEChip::Speed::ONE_HUNDRED_KHZ)
    .value("FOUR_HUNDRED_KHZ", MPSSEChip::Speed::FOUR_HUNDRED_KHZ)
    .value("ONE_MHZ", MPSSEChip::Speed::ONE_MHZ)
    .value("TWO_MHZ", MPSSEChip::Speed::TWO_MHZ)
    .value("FIVE_MHZ", MPSSEChip::Speed::FIVE_MHZ)
    .value("SIX_MHZ", MPSSEChip::Speed::SIX_MHZ)
    .value("TEN_MHZ", MPSSEChip::Speed::TEN_MHZ)
    .value("TWELVE_MHZ", MPSSEChip::Speed::TWELVE_MHZ)
    .value("FIFTEEN_MHZ", MPSSEChip::Speed::FIFTEEN_MHZ)
    .value("THIRTY_MHZ", MPSSEChip::Speed::THIRTY_MHZ)
    .value("SIXTY_MHZ", MPSSEChip::Speed::SIXTY_MHZ);

  py::enum_<MPSSEChip::Endianness>(mpssechip_class, "Endianness")
    .value("MSBFirst", MPSSEChip::Endianness::MSBFirst)
    .value("LSBFirst", MPSSEChip::Endianness::LSBFirst);

  py::class_<FT232H, PyFTDIDevice<FT232H>, MPSSEChip, std::shared_ptr<FT232H>>(m, "FT232H")
    .def(py::init<MPSSEChip::Protocol, MPSSEChip::Speed, MPSSEChip::Endianness, const std::string&, const std::string&>());
#endif
  
  py::class_<PCA9548ACom, PyCom<PCA9548ACom>, I2CCom, std::shared_ptr<PCA9548ACom>>(m, "PCA9548ACom")
    .def(py::init<uint8_t, uint8_t, std::shared_ptr<I2CCom>>());

  // Converters (ADC/DAC) and calibrated devices
  py::class_<DeviceCalibration, PyDeviceCalibrationBase<>>(m, "DeviceCalibration");

  py::class_<DummyCalibration, DeviceCalibration, PyDevCal<>>(m, "DummyCalibration")
    .def(py::init<>())
    .def("calibrate", &DummyCalibration::calibrate)
    .def("uncalibrate", &DummyCalibration::uncalibrate);

  py::class_<LinearCalibration, DeviceCalibration, PyDevCal<LinearCalibration>>(m, "LinearCalibration")
    .def(py::init<double, uint32_t>())
    .def("calibrate", &LinearCalibration::calibrate)
    .def("uncalibrate", &LinearCalibration::uncalibrate);

  py::class_<FileCalibration, DeviceCalibration, PyDevCal<FileCalibration>>(m, "FileCalibration")
    .def(py::init<std::string>())
    .def("calibrate", &FileCalibration::calibrate)
    .def("uncalibrate", &FileCalibration::uncalibrate);

  // ADCDevice
  py::class_<ADCDevice, std::shared_ptr<ADCDevice>, PyADCDeviceBase<>>(m, "ADCDevice")
    .def(py::init<std::shared_ptr<DeviceCalibration>>())
    .def("setCalibration", static_cast<void (ADCDevice::*)(std::shared_ptr<DeviceCalibration>)>(&ADCDevice::setCalibration))
    .def("setCalibration", static_cast<void (ADCDevice::*)(uint8_t, std::shared_ptr<DeviceCalibration>)>(&ADCDevice::setCalibration))
    .def("read", static_cast<double (ADCDevice::*)()>(&ADCDevice::read))
    .def("read", static_cast<double (ADCDevice::*)(uint8_t)>(&ADCDevice::read))
    .def("read", static_cast<void (ADCDevice::*)(const std::vector<uint8_t>&, std::vector<double>&)>(&ADCDevice::read))
    .def("readCount", static_cast<int32_t (ADCDevice::*)()>(&ADCDevice::readCount))
    .def("readCount", static_cast<int32_t (ADCDevice::*)(uint8_t)>(&ADCDevice::readCount))
    .def("readCount", static_cast<void (ADCDevice::*)(const std::vector<uint8_t>&, std::vector<int32_t>&)>(&ADCDevice::readCount));


  py::class_<AD799X, ADCDevice, std::shared_ptr<AD799X>> ad799x_class(m, "AD799X");
  py::enum_<AD799X::Model>(ad799x_class, "Model")
    .value("AD7993", AD799X::Model::AD7993)
    .value("AD7994", AD799X::Model::AD7994)
    .value("AD7997", AD799X::Model::AD7997)
    .value("AD7998", AD799X::Model::AD7998);
  ad799x_class.def(py::init<double, AD799X::Model, std::shared_ptr<I2CCom>>());

  py::class_<ADCDevComuino, ADCDevice, std::shared_ptr<ADCDevComuino>>(m, "ADCDevComuino")
    .def(py::init<double, std::shared_ptr<TextSerialCom>>());

  py::class_<LTC2451, ADCDevice, std::shared_ptr<LTC2451>> ltc2451_class(m, "LTC2451");
  py::enum_<LTC2451::Speed>(ltc2451_class, "Speed")
    .value("Speed60Hz", LTC2451::Speed::Speed60Hz)
    .value("Speed30Hz", LTC2451::Speed::Speed30Hz);
  ltc2451_class.def(py::init<double, std::shared_ptr<I2CCom>>())
    .def("setSpeed", &LTC2451::setSpeed);

  py::class_<MAX11619, ADCDevice, std::shared_ptr<MAX11619>>(m, "MAX11619")
    .def(py::init<double, std::shared_ptr<SPICom>>());

  py::class_<MCP3425, ADCDevice, std::shared_ptr<MCP3425>> mcp3425_class(m, "MCP3425");
  py::enum_<MCP3425::Resolution>(mcp3425_class, "Resolution")
    .value("bit12", MCP3425::Resolution::bit12)
    .value("bit14", MCP3425::Resolution::bit14)
    .value("bit16", MCP3425::Resolution::bit16);
  py::enum_<MCP3425::ConversionMode>(mcp3425_class, "ConversionMode")
    .value("Shot", MCP3425::ConversionMode::Shot)
    .value("Cont", MCP3425::ConversionMode::Cont);
  py::enum_<MCP3425::Gain>(mcp3425_class, "Gain")
    .value("x1", MCP3425::Gain::x1)
    .value("x2", MCP3425::Gain::x2)
    .value("x4", MCP3425::Gain::x4)
    .value("x8", MCP3425::Gain::x8);
  mcp3425_class.def(py::init<MCP3425::Resolution, MCP3425::ConversionMode, MCP3425::Gain, std::shared_ptr<I2CCom>>())
    .def(py::init<std::shared_ptr<I2CCom>>());

  py::class_<MCP3428, ADCDevice, std::shared_ptr<MCP3428>> mcp3428_class(m, "MCP3428");
  py::enum_<MCP3428::Resolution>(mcp3428_class, "Resolution")
    .value("bit12", MCP3428::Resolution::bit12)
    .value("bit14", MCP3428::Resolution::bit14)
    .value("bit16", MCP3428::Resolution::bit16);
  py::enum_<MCP3428::ConversionMode>(mcp3428_class, "ConversionMode")
    .value("Shot", MCP3428::ConversionMode::Shot)
    .value("Cont", MCP3428::ConversionMode::Cont);
  py::enum_<MCP3428::Gain>(mcp3428_class, "Gain")
    .value("x1", MCP3428::Gain::x1)
    .value("x2", MCP3428::Gain::x2)
    .value("x4", MCP3428::Gain::x4)
    .value("x8", MCP3428::Gain::x8);
  mcp3428_class.def(py::init<MCP3428::Resolution, MCP3428::ConversionMode, MCP3428::Gain, std::shared_ptr<I2CCom>>())
    .def(py::init<std::shared_ptr<I2CCom>>())
    .def("setGain", &MCP3428::setGain);

  // DACDevice
  py::class_<DACDevice, std::shared_ptr<DACDevice>, PyDACDeviceBase<>>(m, "DACDevice")
    .def(py::init<std::shared_ptr<DeviceCalibration>>())
    .def("setCalibration", static_cast<void (DACDevice::*)(std::shared_ptr<DeviceCalibration>)>(&DACDevice::setCalibration))
    .def("setCalibration", static_cast<void (DACDevice::*)(uint8_t, std::shared_ptr<DeviceCalibration>)>(&DACDevice::setCalibration))
    .def("set", static_cast<double (DACDevice::*)(double)>(&DACDevice::set))
    .def("set", static_cast<double (DACDevice::*)(uint8_t, double)>(&DACDevice::set))
    .def("set", static_cast<void (DACDevice::*)(const std::vector<uint8_t>&, const std::vector<double>&)>(&DACDevice::set))
    .def("read", static_cast<double (DACDevice::*)()>(&DACDevice::read))
    .def("read", static_cast<double (DACDevice::*)(uint8_t)>(&DACDevice::read))
    .def("read", static_cast<void (DACDevice::*)(const std::vector<uint8_t>&, std::vector<double>&)>(&DACDevice::read))
    .def("setCount", static_cast<void (DACDevice::*)(int32_t)>(&DACDevice::setCount))
    .def("setCount", static_cast<void (DACDevice::*)(uint8_t, int32_t)>(&DACDevice::setCount))
    .def("setCount", static_cast<void (DACDevice::*)(const std::vector<uint8_t>&, const std::vector<int32_t>&)>(&DACDevice::setCount))
    .def("readCount", static_cast<int32_t (DACDevice::*)()>(&DACDevice::readCount))
    .def("readCount", static_cast<int32_t (DACDevice::*)(uint8_t)>(&DACDevice::readCount))
    .def("readCount", static_cast<void (DACDevice::*)(const std::vector<uint8_t>&, std::vector<int32_t>&)>(&DACDevice::readCount));

  py::class_<AD56X9, DACDevice, std::shared_ptr<AD56X9>> ad56x9_class(m, "AD56X9");
  py::enum_<AD56X9::Model>(ad56x9_class, "Model")
    .value("AD5629", AD56X9::AD5629)
    .value("AD5669", AD56X9::AD5669);
  ad56x9_class.def(py::init<double, AD56X9::Model, std::shared_ptr<I2CCom>>());

  py::class_<DAC5571, DACDevice, std::shared_ptr<DAC5571>>(m, "DAC5571")
    .def(py::init<float, std::shared_ptr<I2CCom>>());

  py::class_<DAC5574, DACDevice, std::shared_ptr<DAC5574>>(m, "DAC5574")
    .def(py::init<float, std::shared_ptr<I2CCom>>());

  py::class_<MCP4801, DACDevice, std::shared_ptr<MCP4801>>(m, "MCP4801")
    .def(py::init<std::shared_ptr<SPICom>>());

  m.def("listDevCom", &DeviceComRegistry::listDevCom);
}
