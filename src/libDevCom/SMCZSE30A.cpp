#include "SMCZSE30A.h"

#include "NotSupportedException.h"

#include <cmath>

#include "ClimateSensorRegistry.h"
REGISTER_CLIMATESENSOR(SMCZSE30A)

SMCZSE30A::SMCZSE30A(uint8_t chan, std::shared_ptr<ADCDevice> dev, float Aslope, float Amax, float Aintercept, float Rload)
: ClimateSensorAnalog("unknown"), m_chan(chan), m_Aslope(Aslope), m_Amax(Amax), m_Aintercept(Aintercept), m_Rload(Rload)
{ 
  setADC(dev);
}

SMCZSE30A::SMCZSE30A(std::string name) : ClimateSensorAnalog(name)
{ }

SMCZSE30A::~SMCZSE30A()
{ }

void SMCZSE30A::setConfiguration(const nlohmann::json& config)
{
  for (const auto &kv : config.items())
    {
      if(kv.key()=="chan")
        {
          m_chan=kv.value();
        }
      if(kv.key()=="Aslope")
        {
          m_Aslope=kv.value();
        }
      if(kv.key()=="Aintercept")
        {
          m_Aintercept=kv.value();
        }
      if(kv.key()=="Amax")
        {
          m_Amax=kv.value();
        }
      if(kv.key()=="Rload")
        {
          m_Rload=kv.value();
        }
    }
}

void SMCZSE30A::init()
{ }

void SMCZSE30A::reset()
{ }

void SMCZSE30A::read()
{
  if(m_adcdev==nullptr) throw std::runtime_error("Error (sensor "+m_name+"): pointer to ADC device is NULL.");

  m_pressure=0.;
  for(uint i=0;i<10;i++)
    m_pressure+=(float)m_adcdev->read(m_chan);
  m_pressure=raw_to_P(m_pressure);
}

float SMCZSE30A::temperature() const
{
  throw NotSupportedException("Temperature not supported for SMCZSE30A");
  return 0;
}

float SMCZSE30A::humidity() const
{
  throw NotSupportedException("Humidity not supported for SMCZSE30A");
  return 0;
}

float SMCZSE30A::pressure() const
{
  return m_pressure;
}

float SMCZSE30A::raw_to_P(float raw)
{
  raw = raw/10;
  // If there is a reading error, return a default value
  if(raw>m_Rload*m_Amax || raw<m_Rload*m_Aintercept) return -101;
  float res = (raw/m_Rload-m_Aintercept)/m_Aslope;
  if(res<-101) return -101;
  else return res;
}
