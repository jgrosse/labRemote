#include "ClassRegistry.h"
#include "ClimateSensor.h"

#include <string>
#include <vector>
#include <memory>
#include <functional>

// Configuration helpers and registry for specific ClimateSensor implementations
namespace EquipRegistry {
  /** Register new ClimateSensor type into registry */
  bool registerClimateSensor(const std::string& model,
                           std::function<std::shared_ptr<ClimateSensor>(const std::string&)> f);

  /** Get new instance of given power supply type */
  std::shared_ptr<ClimateSensor> createClimateSensor(const std::string& model, const std::string& name);

  /** List available Ps types */
  std::vector<std::string> listClimateSensor();  
}

#define REGISTER_CLIMATESENSOR(model) \
  static bool _registered_##model = \
    EquipRegistry::registerClimateSensor(#model, \
    std::function<std::shared_ptr<ClimateSensor>(const std::string&)>([](const std::string& name) { return std::shared_ptr<ClimateSensor>(new model(name)); }));
