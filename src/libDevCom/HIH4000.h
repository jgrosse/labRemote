#ifndef HIH4000_H
#define HIH4000_H

#include "ClimateSensorAnalog.h"

//! \brief An implementation of 'ClimateSensorAnalog' for reading an HIH4000 sensor
/**
 * Measures humidity using a HIH4000 sensor connected to one of the analog pins
 * on an Arduino via an ADC intereface.
 * Humidity requires temperature connection: according sensor has to be provided.
 * A better measurement accuracy down to 3.5% RH can be achieved with the pre-calibrated
 * models HIH4000-003 and -004 with a datasheet providing offset and slope of Vout vs. RH 
 * calibrated with a supply voltage of 5V. The same supply voltage has to be used for the 
 * connected HIH4000.
 * HIH4000 specs: https://sensing.honeywell.com/HIH-4000-001-humidity-sensors
 */

class HIH4000: public ClimateSensorAnalog
{
public:
  /**
   * \param chan The channel number of the analog channel connected to the HIH4000
   * \param dev The ADCDevice for interfacing with the Adruino or similar - should be calibrated
   *        to return measured voltage in Volts to match offset and slope
   * \param tempSens The Temperature sensor used for the temperature compensation
   * \param offset The zero offset of Vout vs RH of calibrated sensors at 5V
   * \param slope The Vout vs RH slope of calibrated sensors at 5V
   * \param Vsup The supply voltage
   */
  HIH4000(uint8_t chan, std::shared_ptr<ADCDevice> dev, std::shared_ptr<ClimateSensor> tempSens, float offset = 0.8, float slope = 0.031, float Vsup=5.0);

  /* \brief constructor needed for factory
   *
   */
  HIH4000(std::string name);

  virtual ~HIH4000();

  /* \brief Configure sensor based on JSON object
   *
   * Valid keys:
   *  - `chan`: The channel number of the analog channel connected to the HIH4000
   *
   * \param config JSON configuration
   */
  virtual void setConfiguration(const nlohmann::json& config);

  /**
   * \param tempSens pointer to the temperature sensor needed to temperature-correct hum. reading
   */
  void setTempSens(std::shared_ptr<ClimateSensor> tempSens);

  virtual void init();
  virtual void reset();
  virtual void read();

  virtual float temperature() const;
  virtual float humidity() const;
  virtual float pressure() const;

private:
  //! The channel of the ADC connected to the Pt
  uint8_t m_chan = 0;
  //! The temperature recorded in the last reading (defaults to -273.15)
  float m_temperature = -273.15;
  //! The humidity recorded in the last reading (defaults to 0)
  float m_humidity = 0.;
  //! Supply voltage
  float m_Vsup = 5.;
  //! Zero offset for calibrated sensor
  float m_offset = 0.8;
  //! Slope for calibrated sensor
  float m_slope = 0.031;
  //! ref. to the temperature sensor from which temp. correction is derived
  std::shared_ptr<ClimateSensor> m_tempSens=nullptr;
};

#endif // HIH4000_H
