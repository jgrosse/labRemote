#ifndef SHT85_H
#define SHT85_H

#include "ClimateSensorI2C.h"

#include <memory>

/**
 * The SHT85 climate sensor.
 * [Datasheet](https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/2_Humidity_Sensors/Datasheets/Sensirion_Humidity_Sensors_SHT85_Datasheet.pdf)
 */
class SHT85 : public ClimateSensorI2C
{
public:
  SHT85(std::shared_ptr<I2CCom> i2c);
  SHT85(std::string name);
  virtual ~SHT85();

  //! \brief Configure sensor based on JSON object
  virtual void setConfiguration(const nlohmann::json& config);

  /**
   * \param tempSens pointer to the temperature sensor needed to temperature-correct hum. reading
   */
  void setTempSens(std::shared_ptr<ClimateSensor> tempSens);

  virtual void init();
  virtual void reset();
  virtual void read();

  virtual unsigned status() const;
  virtual float temperature() const;
  virtual float humidity() const;
  virtual float pressure() const;

private:

  int m_status;
  float m_temperature;
  float m_humidity;
  //! ref. to the temperature sensor from which temp. correction is derived
  std::shared_ptr<ClimateSensor> m_tempSens=nullptr;

  uint8_t calcCRC(uint8_t byte0,uint8_t byte1,uint8_t crc) const;
};

#endif // SHT85_H
