#include "ClimateSensorRegistry.h"

#include <iostream>

namespace EquipRegistry
{

  typedef ClassRegistry<ClimateSensor, std::string> RegistryClimateSensor;

  static RegistryClimateSensor &registry()
  {
    static RegistryClimateSensor instance;
    return instance;
  }

  bool registerClimateSensor(const std::string& type, std::function<std::shared_ptr<ClimateSensor>(const std::string&)> f)
  {
    return registry().registerClass(type, f);
  }
  
  std::shared_ptr<ClimateSensor> createClimateSensor(const std::string& type, const std::string& name)
  {
    auto result = registry().makeClass(type, name);
    if(result == nullptr)
      std::cout << "No sensor (ClimateSensor) of type " << type << " found\n";

    return result;
  }
  
  std::vector<std::string> listClimateSensor()
  {
    return registry().listClasses();
  }

}

