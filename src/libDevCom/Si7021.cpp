#include "Si7021.h"
#include "DeviceComRegistry.h"
REGISTER_DEVCOM(Si7021, ClimateSensor)
#include "Logger.h"

#include <unistd.h>
#include <math.h>
#include <iomanip>

#include "NotSupportedException.h"
#include "ChecksumException.h"

#include "ClimateSensorRegistry.h"
REGISTER_CLIMATESENSOR(Si7021)

Si7021::Si7021(std::shared_ptr<I2CCom> i2c)
  : ClimateSensorI2C("unknown")
{
  setI2C(i2c);
}

Si7021::Si7021(std::string name)
  : ClimateSensorI2C(name)
{ }

Si7021::~Si7021()
{ }

void Si7021::setConfiguration(const nlohmann::json& config)
{ }

void Si7021::init()
{ }

void Si7021::reset()
{ 
  if(m_i2c==nullptr) throw std::runtime_error("Error (sensor "+m_name+"): pointer to ADC device is NULL.");

  //send reset command 
  m_i2c->write_reg8(0xFE);

  //read user register
  m_i2c->write_reg8(0xE7);
  uint8_t user_register = m_i2c->read_reg8();
  
  if (user_register!= 0x3A){
    logger(logWARNING)<<"Something wrong with reset command. User registers read: 0x"<< std::hex<< std::setw(4)<< std::setfill('0')<< user_register;    
  }
  
}

void Si7021::read()
{
  
  m_i2c->write_reg8(0xF5);
  uint32_t humidity_data = m_i2c->read_reg24();

  // Parse the data
  uint16_t humidata=(humidity_data   >> 8) & 0xffff;
  m_humidity = humidata;
  m_humidity *= 125;
  m_humidity /= 65536;
  m_humidity -= 6;
  logger (logDEBUG3) << __PRETTY_FUNCTION__ <<"Relative Humidity raw data: 0x" << std::hex << std::setw(4) << std::setfill('0') << humidata << std::dec << " Relative Humidity value: " << m_humidity <<  "%";

  m_i2c->write_reg8(0xE0);
  uint32_t temperature_data = m_i2c->read_reg24();

  uint16_t tempdata=(temperature_data>> 8) & 0xffff;
  
  m_temperature = tempdata;
  m_temperature *= 175.72;
  m_temperature /= 65536;
  m_temperature -= 46.85;
  logger (logDEBUG3) << __PRETTY_FUNCTION__ <<"Temperature raw data: 0x" << std::hex << std::setw(4) << std::setfill('0') << tempdata << std::dec << " Temperature value: " << m_temperature << " C" ;

}

float Si7021::temperature() const
{return m_temperature; }

float Si7021::humidity() const
{ return m_humidity; }

float Si7021::pressure() const
{ throw NotSupportedException("Si7021 does not have a pressure sensor"); return 0; }
