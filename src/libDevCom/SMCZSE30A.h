#ifndef SMCZSE30A_H
#define SMCZSE30A_H

#include <string>
#include <cstdint>

#include "ClimateSensorAnalog.h"

//! \brief An implementation of 'ClimateSensorAnalog' for reading an Pt temperature sensor
/**
 * Measures vacuum pressure using an SMC ZSE30A F (analog current output) connected to one of the 
 * analog channelss on an Arduino via an ADC intereface.
 * specs: https://docs.rs-online.com/5839/A700000008319184.pdf
 */
class SMCZSE30A : public ClimateSensorAnalog
{
public:
  /**
   * \param chan The channel number of the analog channel connected to the SMC ZSE30A
   * \param dev The ADCDevice for interfacing with the Adruino or similar - should be calibrated
   *        to return measured voltage as 0...100% of the operational voltage
   */
  SMCZSE30A(uint8_t chan, std::shared_ptr<ADCDevice> dev,  
	   float Aslope=-0.15841, float Amax=20, float Aintercept=4, float Rload=220);

  /* \brief constructor needed for factory
   *
   */
  SMCZSE30A(std::string name);

  virtual ~SMCZSE30A();

  /* \brief Configure sensor based on JSON object
   *
   * Valid keys:
   *  - `chan`: The channel number of the analog channel connected to the SMC ZSE30A F sensor
   *  - `Amax`: The maximum output current
   *  - `Aslope`: The slope of current output curve
   *  - `Aintercept`: The intercept of current output curve
   *  - `Rload`: The load resistance
   *
   * \param config JSON configuration
   */
  virtual void setConfiguration(const nlohmann::json& config);

  //! Does nothing (requried to implement ClimateSensor)
  virtual void init();
  //! Does nothing (required to implement ClimateSensor)
  virtual void reset();
  //! Reads the temperature from the NTC and stores it in m_temperature
  virtual void read();

  //! Throws a Not Supported exception (required to implement ClimateSensor)
  virtual float temperature() const;
  //! Throws a Not Supported exception (required to implement ClimateSensor)
  virtual float humidity() const;

  virtual float pressure() const;

private:
  float m_Aslope = -1.58416e-4;
  float m_Amax = 0.02;
  float m_Aintercept = 0.004; 
  float m_Rload = 220;
  uint8_t m_chan;
  float m_pressure;
  //! Converts from the raw readout of the Arduino channel to Pressure in kPa
  float raw_to_P(float volt);
};

#endif // SMCZSE30A_H
