#include "HS1101LF.h"

#include "NotSupportedException.h"

#include <cmath>

#include "ClimateSensorRegistry.h"
REGISTER_CLIMATESENSOR(HS1101LF)

HS1101LF::HS1101LF(uint8_t chan, std::shared_ptr<ADCDevice> dev, std::shared_ptr<ClimateSensor> tempSensA, 
		   std::shared_ptr<ClimateSensor> tempSensF, float Coffset, float Tslope, float Cnom, 
		   float Rosc1, float Rosc2)
: ClimateSensorAnalog("unknown"), m_tempSensA(tempSensA), m_tempSensF(tempSensF), m_chan(chan), 
  m_Coffset(Coffset), m_Tslope(Tslope), m_Cnom(Cnom), m_Rosc1(Rosc1), m_Rosc2(Rosc2)
{
   setADC(dev);
}

HS1101LF::HS1101LF(std::string name) : ClimateSensorAnalog(name), m_tempSensA(nullptr), m_tempSensF(nullptr)
{ }

HS1101LF::~HS1101LF()
{ }

void HS1101LF::setConfiguration(const nlohmann::json& config)
{
  for (const auto &kv : config.items())
    {
      if(kv.key()=="chan")
        {
          m_chan=kv.value();
        }
      if(kv.key()=="Coffset")
        {
          m_Coffset=kv.value();
        }
      if(kv.key()=="Cnom")
        {
          m_Cnom=kv.value();
        }
      if(kv.key()=="Tslope")
        {
          m_Tslope=kv.value();
        }
      if(kv.key()=="Rosc1")
	{
          m_Rosc1=kv.value();
	}
      if(kv.key()=="Rosc2")
	{
          m_Rosc2=kv.value();
	}
     }
}

void HS1101LF::setTempSensA(std::shared_ptr<ClimateSensor> tempSensA)
{
  m_tempSensA = tempSensA;
}

void HS1101LF::setTempSensF(std::shared_ptr<ClimateSensor> tempSensF)
{
  m_tempSensF = tempSensF;
}

void HS1101LF::init()
{ }

void HS1101LF::reset()
{ }

void HS1101LF::read()
{

  if(m_tempSensA!=nullptr){
    m_tempSensA->read();
    m_temperature=m_tempSensA->temperature();
  }

  if(m_tempSensF!=nullptr){
    m_tempSensF->read();
    m_temperatureOsc=m_tempSensF->temperature();
  }

  // update freq. if adc is defined, otherwise rely on this being set externally
  if(m_adcdev!=nullptr) m_freq += (float)m_adcdev->read(m_chan);

  // calculate capacity from frequency and correct for stray capacity
  float X = (1./m_freq/(m_Rosc2+2.*m_Rosc1)/log(2.)-m_Coffset)/m_Cnom;
  // TLC555 resistors' temperature correction
  X -= (m_temperatureOsc - 25.0) * m_Tslope;
  m_humidity = -3.4656e3*X*X*X+1.0732e4*X*X-1.0457e4*X+3.2459e3;
  // sanity checks
  if(m_humidity>99.999) m_humidity = 99.999;
  if(m_humidity<1.e-3) m_humidity = 1.e-3;

}

float HS1101LF::temperature() const
{
  return m_temperature;
}

float HS1101LF::humidity() const
{
  return m_humidity;
}

float HS1101LF::pressure() const
{
  throw NotSupportedException("Pressure not supported for HS1101LF");
  return 0;
}
