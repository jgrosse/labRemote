#include "PtSensor.h"
#include "DeviceComRegistry.h"
REGISTER_DEVCOM(PtSensor, ClimateSensor)

#include "NotSupportedException.h"

#include <cmath>

#include "ClimateSensorRegistry.h"
REGISTER_CLIMATESENSOR(PtSensor)

PtSensor::PtSensor(uint8_t chan, std::shared_ptr<ADCDevice> dev, float Rref, float Apt, float Bpt, 
		   float Rdiv, float Vsup, bool wOpAmp, float Ampl, float Offs)
: ClimateSensorAnalog("unknown"), m_chan(chan), m_Rref(Rref), m_Apt(Apt), m_Bpt(Bpt), m_Rdiv(Rdiv), m_Vsup(Vsup),
    m_wOpAmp(wOpAmp), m_Ampl(Ampl), m_Offs(Offs)
{ 
  setADC(dev);
}

PtSensor::PtSensor(std::string name) : ClimateSensorAnalog(name)
{ }

PtSensor::~PtSensor()
{ }

void PtSensor::setConfiguration(const nlohmann::json& config)
{
  for (const auto &kv : config.items())
    {
      if(kv.key()=="chan")
        {
          m_chan=kv.value();
        }
      if(kv.key()=="Rref")
        {
          m_Rref=kv.value();
        }
      if(kv.key()=="Rdiv")
        {
          m_Rdiv=kv.value();
        }
      if(kv.key()=="Apt")
        {
          m_Apt=kv.value();
        }
      if(kv.key()=="Bpt")
        {
          m_Bpt=kv.value();
        }
      if(kv.key()=="Vsup")
        {
          m_Vsup=kv.value();
        }
      if(kv.key()=="wOpAmp")
        {
          m_wOpAmp=kv.value();
        }
      if(kv.key()=="Ampl")
        {
          m_Ampl=kv.value();
        }
      if(kv.key()=="Offs")
        {
          m_Offs=kv.value();
        }
    }
}

void PtSensor::init()
{ }

void PtSensor::reset()
{ }

void PtSensor::read()
{
  if(m_adcdev==nullptr) throw std::runtime_error("Error (sensor "+m_name+"): pointer to ADC device is NULL.");

  float raw=0.;
  for(uint i=0;i<10;i++)
    raw+=(float)m_adcdev->read(m_chan);
  raw/=10.;
  m_temperature=raw_to_C(raw);
}

float PtSensor::temperature() const
{
  return m_temperature;
}

float PtSensor::humidity() const
{
  throw NotSupportedException("Humidity not supported for Pt");
  return 0;
}

float PtSensor::pressure() const
{
  throw NotSupportedException("Pressure not supported for Pt");
  return 0;
}

float PtSensor::raw_to_C(float raw)
{
  // If there is a reading error, return a default value
  if(raw>=0.99*m_Vsup || raw==0.) return -273.15;
  float res;
  float temp;
  if(m_wOpAmp){ // voltage read via OpAmp
    temp = 100.*raw / m_Ampl + m_Offs;
  } else { // voltage read from divider
    res = m_Rdiv*(m_Vsup/raw-1.);
    //std::cout << "PT resis. = " << res << " at raw ADC: " << raw << std::endl;
    temp = m_Apt*m_Apt-4.*m_Bpt*(1.-res/m_Rref);
    if(temp<0.) return -273.15;
    else temp = (std::sqrt(temp)-m_Apt)/2./m_Bpt;
  }
  return temp;
}
