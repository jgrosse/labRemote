#ifndef CLIMATESENSORI2C_H
#define CLIMATESENSORI2C_H

#include "ClimateSensor.h"
#include "I2CCom.h"

//! \brief Abstract implementation of a climate sensor with I2C read-out via ADC class
/** 
 * A climate sensor is defined as a sensor that can measure 
 * any one of the following properties.
 *  - temperature
 *  - humidity
 *  - pressure
 *
 * If a given sensor does not support a certain property, then
 * it should throw the `NotSupported` exception when trying to
 * retrieve the given property.
 *
 * This class inherits from ClimateSensor, but provides a generic
 * function to load an ADC interface needed for all I2C sensors
 * 
 * All classes that inherit from this class have Python bindings
 */
class ClimateSensorI2C : public ClimateSensor
{
public:
  ClimateSensorI2C(std::string name = "unknown");
  virtual ~ClimateSensorI2C() =default;

  //! \brief set ADC interface required for reading sensor
  void setI2C(std::shared_ptr<I2CCom> dev);

  //! \brief Configure sensor based on JSON object
  virtual void setConfiguration(const nlohmann::json& config) =0;

  //! \brief Initialize the sensor
  /**
   * Setup any necessary sensor configuration here.
   */
  virtual void init() =0;

  //! \brief Reset the sensor
  virtual void reset() =0;

  //! \brief Read the sensor measurement
  /**
   * Sensor climate measurements should be retrieved here
   * and stored inside private variables.
   */
  virtual void read() =0;

  //! \brief Last measured temperature (Celsius)
  /**
   * Raise `NotSupported` exception if the sensor 
   * cannot measure temperature.
   */
  virtual float temperature() const =0;

  //! \brief Last measured relative humidity (0 to 1)
  /**
   * Raise `NotSupported` exception if the sensor 
   * cannot measure humidity.
   */  
  virtual float humidity() const =0;

  //! \brief Last measured pressure (Pascal)
  /**
   * Raise `NotSupported` exception if the sensor 
   * cannot measure pressure.
   */  
  virtual float pressure() const =0;

 protected:
  //! Interface with the Arduino via the I2C protocol
  std::shared_ptr<I2CCom> m_i2c;
};

#endif // CLIMATESENSORANALOG_H
