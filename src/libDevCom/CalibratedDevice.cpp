#include "CalibratedDevice.h"

#include "NotSupportedException.h"

CalibratedDevice::CalibratedDevice(std::shared_ptr<DeviceCalibration> calibration)
  : m_calibration(calibration)
{ }

void CalibratedDevice::setCalibration(std::shared_ptr<DeviceCalibration> calibration)
{ m_calibration=calibration; }

void CalibratedDevice::setCalibration(uint8_t ch, std::shared_ptr<DeviceCalibration> calibration)
{ m_channelCalibration[ch]=calibration; }

std::shared_ptr<DeviceCalibration> CalibratedDevice::findCalibration(uint8_t ch) const
{
  return (m_channelCalibration.find(ch)==m_channelCalibration.end())?m_calibration:m_channelCalibration.at(ch);
}

double CalibratedDevice::read(uint8_t ch)
{ return findCalibration(ch)->calibrate(readCount(ch)); }

void CalibratedDevice::read(const std::vector<uint8_t>& chs, std::vector<double>& data)
{
  std::vector<int32_t> counts(chs.size());
  readCount(chs, counts);

  data.clear();
  for(uint i=0; i<chs.size(); i++)
    data.push_back(findCalibration(chs[i])->calibrate(counts[i]));
}

int32_t CalibratedDevice::readCount(uint8_t)
{ throw NotSupportedException("Read operation not supported."); }

void CalibratedDevice::readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& data)
{ throw NotSupportedException("Bulk read operation not supported."); }

double CalibratedDevice::write(double value, uint8_t ch)
{
  std::shared_ptr<DeviceCalibration> cal=findCalibration(ch);
  int32_t calval=cal->uncalibrate(value);

  writeCount(calval);

  return cal->calibrate(calval);
}

void CalibratedDevice::write(const std::vector<uint8_t>& chs, const std::vector<double>& values, std::vector<double>& actvalues)
{
  std::vector<int32_t> counts;

  actvalues.clear();
  for(uint32_t i=0;i<chs.size();i++)
    {
      std::shared_ptr<DeviceCalibration> cal=findCalibration(chs[i]);
      int32_t calval=cal->uncalibrate(values[i]);
      counts.push_back(calval);
      actvalues.push_back(cal->calibrate(calval));
    }

  writeCount(chs, counts);
}

void CalibratedDevice::writeCount(int32_t value, uint8_t ch)
{ throw NotSupportedException("Write operation not supported."); }

void CalibratedDevice::writeCount(const std::vector<uint8_t>& chs, const std::vector<int32_t>& values)
{ throw NotSupportedException("Bulk write operation not supported."); }

