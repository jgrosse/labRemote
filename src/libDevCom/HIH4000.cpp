#include "HIH4000.h"
#include "DeviceComRegistry.h"
REGISTER_DEVCOM(HIH4000, ClimateSensor)

#include "NotSupportedException.h"

#include <cmath>

#include "ClimateSensorRegistry.h"
REGISTER_CLIMATESENSOR(HIH4000)

HIH4000::HIH4000(uint8_t chan, std::shared_ptr<ADCDevice> dev, std::shared_ptr<ClimateSensor> tempSens, float offset, float slope, float Vsup)
: ClimateSensorAnalog("unknown"), m_tempSens(tempSens), m_chan(chan), m_offset(offset), m_slope(slope), m_Vsup(Vsup)
{
   setADC(dev);
}

HIH4000::HIH4000(std::string name) : ClimateSensorAnalog(name), m_tempSens(nullptr)
{ }

HIH4000::~HIH4000()
{ }

void HIH4000::setConfiguration(const nlohmann::json& config)
{
  for (const auto &kv : config.items())
    {
      if(kv.key()=="chan")
        {
          m_chan=kv.value();
        }
      if(kv.key()=="Vsup")
        {
          m_Vsup=kv.value();
        }
      if(kv.key()=="offset")
        {
          m_offset=kv.value();
        }
      if(kv.key()=="slope")
        {
          m_slope=kv.value();
        }
    }
}

void HIH4000::setTempSens(std::shared_ptr<ClimateSensor> tempSens)
{
  m_tempSens = tempSens;
}

void HIH4000::init()
{ }

void HIH4000::reset()
{ }

void HIH4000::read()
{
  if(m_adcdev==nullptr) throw std::runtime_error("Error (sensor "+m_name+"): pointer to ADC device is NULL.");

  if(m_tempSens!=nullptr){
    m_tempSens->read();
    m_temperature=m_tempSens->temperature();
  }
  m_humidity = 0.;
  const uint navg = 5;
  // output voltage
  for(uint i=0;i<navg;i++)
    m_humidity += (float)m_adcdev->read(m_chan);
  m_humidity/=(float)navg;

  // sensor humidity
  if((m_humidity * 5.0/m_Vsup)>m_offset){ 
    // calib. constants provided at 5 V, so need to scale to 5.0/m_Vsup
    m_humidity = (m_humidity * 5.0/m_Vsup - m_offset)/m_slope;
    // temperature correction -  use 20C if T-sensor not availoable
    double tempForCorr = 20.;
    if(m_temperature>-273) tempForCorr = m_temperature;
    m_humidity=m_humidity/(1.0546-0.00216*tempForCorr);
  } else // hum = offset -> sensor not connected, nothing to correct
    m_humidity = 0.;
  // lower limit for dewpoint calc.
  if(m_humidity<1.e-3) m_humidity=1.e-3;
}

float HIH4000::temperature() const
{
  return m_temperature;
}

float HIH4000::humidity() const
{
  return m_humidity;
}

float HIH4000::pressure() const
{
  throw NotSupportedException("Pressure not supported for HIH4000");
  return 0;
}
