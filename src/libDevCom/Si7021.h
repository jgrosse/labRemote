#ifndef AdafruitSi7021_H
#define AdafruitSi7021_H

#include "ClimateSensorI2C.h"

#include <memory>
/** \brief Si7021
 *
 * Implementation for the Si7021 humiidity/temperature sensor
 *
 * [Progamming Manual](https://www.silabs.com/documents/public/data-sheets/Si7021-A20.pdf)
 */

class Si7021 : public ClimateSensorI2C
{
public:
  Si7021(std::shared_ptr<I2CCom> i2c);
  Si7021(std::string name);
  virtual ~Si7021();

  //! \brief Configure sensor based on JSON object
  virtual void setConfiguration(const nlohmann::json& config);

  virtual void init();
  virtual void reset();
  virtual void read();

  virtual float temperature() const;
  virtual float humidity() const;
  virtual float pressure() const;
private:

  float m_temperature;
  float m_humidity;
  
  
};

#endif // Si7021_H

