#include "ClimateSensorI2C.h"

ClimateSensorI2C::ClimateSensorI2C(std::string name)
  : ClimateSensor(name), m_i2c(nullptr)
{
}
void ClimateSensorI2C::setI2C(std::shared_ptr<I2CCom> dev)
{
  m_i2c = dev;
}

