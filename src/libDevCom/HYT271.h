#ifndef HYT271_H
#define HYT271_H

#include "ClimateSensor.h"
#include "I2CCom.h"
#include "ComIOException.h"

#include <memory>

/**
 * The HYT271 climate sensor.
 * [Datasheet](https://www.ist-ag.com/sites/default/files/DHHYT271_E.pdf)
 */
class HYT271 : public ClimateSensor
{
public:
  HYT271(std::shared_ptr<I2CCom> i2c);
  virtual ~HYT271();

  //! \brief Configure sensor based on JSON object
  virtual void setConfiguration(const nlohmann::json& config);

  virtual void init();
  virtual void reset();
  virtual void read();

  virtual unsigned status() const;
  virtual float temperature() const;
  virtual float humidity() const;
  virtual float pressure() const;

private:
  std::shared_ptr<I2CCom> m_i2c;

  int m_status;
  float m_temperature;
  float m_humidity;
};

#endif // HYT271_H
