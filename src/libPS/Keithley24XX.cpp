#include "Keithley24XX.h"

#include <algorithm>
#include <thread>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(Keithley24XX)

Keithley24XX::Keithley24XX(const std::string& name) :
IPowerSupply(name, {"2410","2400"})
{ 
  std::string fname = "/tmp/keith24xx_"+m_name;
  m_fp = fopen(fname.c_str(), "r");
  if(m_fp!=nullptr){
    fclose(m_fp);
  } else {
    system(("touch /tmp/keith24xx_"+m_name+"; chmod 664 /tmp/keith24xx_"+m_name).c_str());
  }
  m_fp = fopen(fname.c_str(), "a");
  if(m_fp!=0 && flock(fileno(m_fp), LOCK_EX)!=0){ 
    if(errno==EBADF) logger(logERROR) << "fd is not an open file descriptor";
    else if(errno==EINTR) logger(logERROR) << "While waiting to acquire a lock, the  call  was  interrupted";
    else if(errno==EINVAL) logger(logERROR) << "operation is invalid";
    else if(errno==ENOLCK) logger(logERROR) << "kernel ran out of lock memory";
    else logger(logERROR) << "unknown flock error";
    fclose(m_fp);
    m_fp = 0;
  }
}

Keithley24XX::~Keithley24XX()
{
  if(m_fp!=0) flock(fileno(m_fp), LOCK_UN);
  fclose(m_fp);
}
bool Keithley24XX::ping()
{
  std::string result = m_com->sendreceive("*IDN?");
  return !result.empty();
}

void Keithley24XX::reset()
{
  m_com->send("OUTPUT OFF");
  m_com->send("*RST");
  m_com->send(":TRIGGER:COUNT 1");
  m_com->send(":FORMAT:ELEMENTS TIME,VOLT,CURR");
  m_com->send(":SYST:BEEP:STAT OFF");//disable beep

  if(!ping())
    throw std::runtime_error("No communication after reset.");
}

void Keithley24XX::turnOn(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  disarm(channel);

  m_com->send("OUTPUT ON");

  arm(channel);
}

void Keithley24XX::turnOff(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  disarm(channel);

  m_com->send("OUTPUT OFF");

  // arm is not allowed when off
  //arm(channel);
}

bool Keithley24XX::isOn(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  // search for substring in case value is returned with a terminator
  bool ison = (m_com->sendreceive(":OUTPUT?").find("1") == 0);
  return ison;
}

std::string Keithley24XX::identify()
{
  std::string idn=m_com->sendreceive("*IDN?");
  return idn;
}

void Keithley24XX::setCurrentLevel(double cur, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  disarm(channel);

  m_com->send(":SOURCE:FUNC CURR");

  m_com->send(":SOURCE:CURR " + to_string_with_precision(cur));  

  arm(channel);
}

double Keithley24XX::getCurrentLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  disarm(channel);
  double retval =  std::stod(m_com->sendreceive(":SOURCE:CURR?"));
  arm(channel);

  return retval;
}

void Keithley24XX::setCurrentProtect(double maxcur, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  disarm(channel);
  m_com->send(":SENSE:CURR:PROTECTION " + std::to_string(maxcur));
  if(isOn(channel)) arm(channel);
}

double Keithley24XX::getCurrentProtect(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  disarm(channel);
  double retval = std::stod(m_com->sendreceive(":SENSE:CURR:PROTECTION?"));
  arm(channel);

  return retval;
}

double Keithley24XX::measureCurrent(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  if(!isOn(channel))
    return 0.;

  disarm(channel);
  m_com->send(":SENS:CURR:RANG:AUTO ON");
  m_com->send(":FORMAT:ELEMENTS CURR");

  double value = std::stod(m_com->sendreceive(":READ?"));
  arm(channel);

  return value;
}

void Keithley24XX::setVoltageLevel(double volt, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  disarm(channel);

  m_com->send(":SOURCE:FUNC VOLT");
  
  //setVoltage
  m_com->send(":SOURCE:VOLT " + to_string_with_precision(volt));

  if(isOn(channel)) arm(channel);
}

double Keithley24XX::getVoltageLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  disarm(channel);

  double retval = std::stod(m_com->sendreceive(":SOURCE:VOLT?"));
  arm(channel);

  return retval;
}

void Keithley24XX::setVoltageRange(double maxvolt, unsigned channel)
{
  disarm(channel);
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  m_com->send(":SOUR:VOLT:RANG " + std::to_string(maxvolt));
  if(isOn(channel)) arm(channel);
}

void Keithley24XX::setVoltageProtect(double maxvolt, unsigned channel)
{
  disarm(channel);
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  m_com->send(":SENSE:VOLT:PROTECTION " + std::to_string(maxvolt));
  arm(channel);
}

double Keithley24XX::getVoltageProtect(unsigned channel)
{
  disarm(channel);
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  double retval = std::stod(m_com->sendreceive(":SENSE:VOLT:PROTECTION?"));
  arm(channel);

  return retval;
}

double Keithley24XX::measureVoltage(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  if(!isOn(channel))
    return 0.;

  disarm(channel);
  m_com->send(":SENS:CURR:RANG:AUTO ON");
  m_com->send(":FORMAT:ELEMENTS VOLT");

  double value =  std::stod(m_com->sendreceive(":READ?"));
  arm(channel);

  return value;
}

void Keithley24XX::disarm(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  //disarm
  m_com->send(":ABOR");
  m_com->send(":ARM:COUNT 1");
}

void Keithley24XX::arm(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  //arm
  m_com->send(":ARM:COUNT INF");
  m_com->send(":INIT");
}
