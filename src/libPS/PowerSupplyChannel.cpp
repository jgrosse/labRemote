#include "PowerSupplyChannel.h"

#include "Logger.h"

PowerSupplyChannel::PowerSupplyChannel(const std::string& name, std::shared_ptr<IPowerSupply> ps, unsigned channel)
  : m_name(name), m_ps(ps), m_channel(channel), m_ilname("none")
{ }

std::string PowerSupplyChannel::getName() const
{ return m_name; }

std::shared_ptr<IPowerSupply> PowerSupplyChannel::getPowerSupply() const
{ return m_ps; }

unsigned PowerSupplyChannel::getChannel() const
{ return m_channel; }

void PowerSupplyChannel::setProgram(const nlohmann::json& settings)
{ m_program=settings; }

void PowerSupplyChannel::program()
{
  for (const auto &cmd : m_program.items())
    {
      if (cmd.key()=="maxvoltage")
	{
	  setVoltageProtect(cmd.value());
	}
      else if (cmd.key()=="maxcurrent")
	{
	  setCurrentProtect(cmd.value());
	}
      else if (cmd.key()=="voltage")
	{
	  setVoltageLevel(cmd.value());
	}
      else if (cmd.key()=="current")
	{
	  setCurrentLevel(cmd.value());
	}
      else
	{
	  logger(logWARNING) << "Unknow program option "<<cmd.key()<<" " <<cmd.value();
	}	     
    }
}


  
void PowerSupplyChannel::turnOn()
{ m_ps->turnOn(m_channel); }

void PowerSupplyChannel::turnOff()
{ m_ps->turnOff(m_channel); }

bool PowerSupplyChannel::isOn()
{ return m_ps->isOn(m_channel); }

bool PowerSupplyChannel::isTripped()
{ return m_ps->isTripped(m_channel); }

void PowerSupplyChannel::rampCurrentLevel(double cur, double rate)
{ m_ps->rampCurrentLevel(cur, rate, m_channel); }  

void PowerSupplyChannel::setCurrentLevel(double cur)
{ m_ps->setCurrentLevel(cur, m_channel); }  

double PowerSupplyChannel::getCurrentLevel()
{ return m_ps->getCurrentLevel(m_channel); }  

void PowerSupplyChannel::setCurrentProtect(double cur)
{ m_ps->setCurrentProtect(cur, m_channel); }

double PowerSupplyChannel::getCurrentProtect()
{ return m_ps->getCurrentProtect(m_channel); }

double PowerSupplyChannel::measureCurrent()
{ return m_ps->measureCurrent(m_channel); }

void PowerSupplyChannel::rampVoltageLevel(double volt, double rate)
{ m_ps->rampVoltageLevel(volt, rate, m_channel); }  
 
void PowerSupplyChannel::setVoltageRange(double volt)
{ m_ps->setVoltageRange(volt, m_channel); }

void PowerSupplyChannel::setVoltageLevel(double volt)
{ m_ps->setVoltageLevel(volt, m_channel); }

double PowerSupplyChannel::getVoltageLevel()
{ return m_ps->getVoltageLevel(m_channel); }

void PowerSupplyChannel::setVoltageProtect(double volt)
{ m_ps->setVoltageProtect(volt, m_channel); }

double PowerSupplyChannel::getVoltageProtect()
{ return m_ps->getVoltageProtect(m_channel); }

double PowerSupplyChannel::measureVoltage()
{ return m_ps->measureVoltage(m_channel); }
