#include "SCPIPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"
#include "ScopeLock.h"
#include "StringUtils.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(SCPIPs)

SCPIPs::SCPIPs(const std::string& name, std::vector<std::string> models,  unsigned maxChannels) :
IPowerSupply(name, models), m_maxChannels(maxChannels)
{ }

bool SCPIPs::ping()
{
  std::string result = m_com->sendreceive("*IDN?");
  return !result.empty();
}

void SCPIPs::reset()
{
  send("*RST");

  if(!ping())
    throw std::runtime_error("No communication after reset.");
}

std::string SCPIPs::identify()
{
  std::string idn=m_com->sendreceive("*IDN?");
  return idn;
}

void SCPIPs::turnOn(unsigned channel)
{
  send("OUTPUT ON", channel);
}

void SCPIPs::turnOff(unsigned channel)
{
  send("OUTPUT OFF", channel);
}

void SCPIPs::setChannel(unsigned channel)
{
  if(m_maxChannels>0)
    { // In-range channel check
      if(channel>m_maxChannels)
	throw std::runtime_error("Invalid channel: "+std::to_string(channel));
    }

  if(m_maxChannels==1) return; // No need to change channel

  send("INST:NSEL " + std::to_string(channel));
}

void SCPIPs::setCurrentLevel(double cur, unsigned channel)
{
  send("CURRENT " + std::to_string(cur), channel);
}

double SCPIPs::getCurrentLevel(unsigned channel)
{
  return std::stod(sendreceive("CURRENT?", channel));
}

void SCPIPs::setCurrentProtect(double maxcur, unsigned channel)
{
  send("CURRENT " + std::to_string(maxcur), channel);
}

double SCPIPs::getCurrentProtect(unsigned channel)
{
  return getCurrentLevel();
}

double SCPIPs::measureCurrent(unsigned channel)
{
  return std::stod(sendreceive("MEAS:CURR?", channel));
}

void SCPIPs::setVoltageLevel(double volt, unsigned channel)
{
  send("VOLTAGE " + std::to_string(volt), channel);
}

double SCPIPs::getVoltageLevel(unsigned channel)
{
  return std::stod(sendreceive("VOLTAGE?", channel));
}

void SCPIPs::setVoltageProtect(double maxvolt, unsigned channel)
{
  send("VOLTAGE " + std::to_string(maxvolt), channel);
}

double SCPIPs::getVoltageProtect(unsigned channel)
{
  return getVoltageLevel();
}

double SCPIPs::measureVoltage(unsigned channel)
{
  return std::stod(sendreceive("MEAS:VOLT?", channel));
}

void SCPIPs::send(const std::string& cmd)
{
  ScopeLock lock(m_com);

  m_com->send(cmd);
  std::string opcreply=m_com->sendreceive("*OPC?");
  utils::rtrim(opcreply);
  if(opcreply!="1")
    throw std::runtime_error("SCPI command not completed");
}

void SCPIPs::send(const std::string& cmd, unsigned channel)
{
  if(m_maxChannels>0)
    { // In-range channel check
      if(channel>m_maxChannels)
	throw std::runtime_error("Invalid channel: "+std::to_string(channel));
    }

  m_com->lock();
  if(m_maxChannels!=1)
    m_com->send("INST:NSEL "+std::to_string(channel));

  send(cmd);
  m_com->unlock();
}

std::string SCPIPs::sendreceive(const std::string& cmd)
{
  return m_com->sendreceive(cmd);
}

std::string SCPIPs::sendreceive(const std::string& cmd, unsigned channel)
{
  if(m_maxChannels>0)
    { // In-range channel check
      if(channel>m_maxChannels)
	throw std::runtime_error("Invalid channel: "+std::to_string(channel));
    }

  m_com->lock();
  if(m_maxChannels!=1)
    m_com->send("INST:NSEL "+std::to_string(channel));

  std::string ret=sendreceive(cmd);
  m_com->unlock();
  return ret;
}
