#ifndef KEITHLEY24XX_H
#define KEITHLEY24XX_H

#include <chrono>
#include <iomanip>
#include <memory>
#include <string>
#include <sstream>

#include "IPowerSupply.h"

#include "SerialCom.h"

/** \brief Keithley 24xx
 *
 * Implementation for the Keithley 2410 power supply.
 *
 * [Programming Manual](https://download.tek.com/manual/2400S-900-01_K-Sep2011_User.pdf)
 *
 * Other Keithley 24xx power supplies might be supported too, but have
 * not been checked.
 */
class Keithley24XX : public IPowerSupply
{
public:
  Keithley24XX(const std::string& name);
  ~Keithley24XX();

  /** \name Communication
   * @{
   */

  virtual bool ping();

  virtual std::string identify();

  /** @} */

  /** \name Power Supply Control
   * @{
   */

  virtual void reset();
  virtual void turnOn(unsigned channel);
  virtual void turnOff(unsigned channel);
  virtual bool isOn(unsigned channel);

  /** @} */
  
  /** \name Current Control and Measurement
   * @{
   */

  virtual void   setCurrentLevel(double cur, unsigned channel = 0);
  virtual double getCurrentLevel(unsigned channel = 0);
  virtual void   setCurrentProtect(double maxcur , unsigned channel = 0);
  virtual double getCurrentProtect(unsigned channel = 0);  
  virtual double measureCurrent(unsigned channel = 0);

  /** @} */

  /** \name Voltage Control and Measurement
   * @{
   */

  virtual void   setVoltageLevel(double volt, unsigned channel = 0);
  virtual double getVoltageLevel(unsigned channel = 0);
  virtual void   setVoltageRange(double maxvolt , unsigned channel = 0 );
  virtual void   setVoltageProtect(double maxvolt , unsigned channel = 0 );
  virtual double getVoltageProtect(unsigned channel = 0);
  virtual double measureVoltage(unsigned channel = 0);

  /** @} */

private:
  /**
   * Convert number to scientific notation
   *
   * \param a_value number to convert
   * \param n number of digitcs after the decimal
   *
   * \return x.xxxxxxEyy
   */

  virtual void   disarm(unsigned channel = 0);
  virtual void   arm(unsigned channel = 0);

  template <typename T>
  std::string to_string_with_precision(const T a_value, const int n = 6)
  {
    std::ostringstream out;
    out << std::setprecision(n) << a_value;
    return out.str();
  }

  // dummy file handle to be able to use flock() for locking
  FILE *m_fp;

};

#endif

