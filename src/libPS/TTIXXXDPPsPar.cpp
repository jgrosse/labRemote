#include "TTIXXXDPPsPar.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(TTIXXXDPPsPar)

TTIXXXDPPsPar::TTIXXXDPPsPar(const std::string& name) :
TTIPs(name, {"PL303QMD-P"}, 1)
{ }
