#ifndef TTIXXXDPPSPAR_H
#define TTIXXXDPPSPAR_H

#include <chrono>
#include <memory>
#include <string>

#include "TTIPs.h"

/** \brief TTi XXXDP
 *
 * Implementation for the TTi dual channel power supplies
 * such as PL330DP.
 *
 */
class TTIXXXDPPsPar : public TTIPs
{
public:
  TTIXXXDPPsPar(const std::string& name);
  ~TTIXXXDPPsPar() =default;

};

#endif

