#ifndef RS_HMP4030_H
#define RS_HMP4030_H

#include <chrono>
#include <memory>
#include <string>

#include "SCPIPs.h"

#include "SerialCom.h"

/** \brief ROHDE&SWARZ HMP4030
 * Implementation for the ROHDE&SWARZ HMP4030 power supply.
 *
 * [Programming Manual](https://cdn.rohde-schwarz.com/pws/dl_downloads/dl_common_library/dl_manuals/gb_1/h/hmp_serie/HMP_SCPI_ProgrammersManual_en_01.pdf)
 *
 * Others in the HMP series (HMP2020, HMP2030, HMP4030 and HMP4040) might
 * be also supported, but have not been tested.
 */
class RS_HMP4030 : public SCPIPs
{
public:
  RS_HMP4030(const std::string& name);
  ~RS_HMP4030() =default;

};

#endif

