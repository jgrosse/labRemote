#include "RS_HMP4030.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(RS_HMP4030)

RS_HMP4030::RS_HMP4030(const std::string& name) :
SCPIPs(name, {"HMP4030"}, 3)
{ }

