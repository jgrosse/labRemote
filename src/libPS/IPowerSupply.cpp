#include "IPowerSupply.h"

#include <chrono>
#include <iostream>
#include <stdexcept>
#include <thread>

#include "Logger.h"

IPowerSupply::IPowerSupply(const std::string& name, std::vector<std::string> models)
{
  m_name = name;
  m_models = models;
}

void IPowerSupply::setCom(std::shared_ptr<ICom> com)
{
  if(!com->is_open())
    com->init();

  m_com = com;
  if(!ping())
    throw std::runtime_error("Failed communication with the PS");
}

void IPowerSupply::setConfiguration(const nlohmann::json& config)
{  
  m_config = config;
}

const nlohmann::json& IPowerSupply::getConfiguration() const
{
  return m_config;
}

void IPowerSupply::checkCompatibilityList()
{
  // get model connected to the PS
  std::string idn = identify();

  // get list of models
  std::vector<std::string> models = IPowerSupply::getListOfModels();

  if (models.empty()){
    logger(logINFO) << "No model identifier implemented for this power supply. No check is performed.";
    return;
  }

  for(const std::string& model : models)
    {
      if(idn.find(model) != std::string::npos)
	return;
    }
  
  logger(logERROR)<< "Unknown power supply: " << idn;
  throw std::runtime_error("Unknown power supply: " + idn);

}

std::vector<std::string> IPowerSupply::getListOfModels()
{
  return m_models;
}

void IPowerSupply::turnOnAll()
{
  logger(logWARNING) << "turnOnAll() not implemented for this PS.";
}

void IPowerSupply::turnOffAll()
{
  logger(logWARNING) << "turnOffAll() not implemented for this PS.";
}

bool IPowerSupply::isOn(unsigned channel)
{
  logger(logWARNING) << "isOn() not implemented for this PS.";
  return false;
}

bool IPowerSupply::isTripped(unsigned channel)
{
  logger(logWARNING) << "isTripped() not implemented for this PS.";
  return false;
}

void IPowerSupply::rampCurrentLevel(double curr, double rate, unsigned channel)
{
  if(rate<0)
    throw std::runtime_error("rampCurrentLevel: ramp rate must be positive");

  // Get starting point and direction
  double currentCurrent=measureCurrent(channel);
  double dir=(currentCurrent<curr)?+1:-1; // Direction of ramp

  // Ramp until you get as close as possible to desired current with
  // discrete `ramp` steps
  uint32_t nsteps=std::floor(std::fabs(curr-currentCurrent)/rate);
  for(uint32_t i=1;i<nsteps;i++)
    {
      setCurrentLevel(currentCurrent+i*dir*rate, channel);
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }
  // Set final current. The final step size should be less than rate
  setCurrentLevel(curr,channel);
}

void IPowerSupply::setCurrentProtect(double cur, unsigned channel)
{
  logger(logWARNING) << "setCurrentProtect() not implemented for this PS.";
}

double IPowerSupply::getCurrentProtect(unsigned channel)
{
  logger(logWARNING) << "getCurrentProtect() not implemented for this PS.";
  return 0;
}

void IPowerSupply::rampVoltageLevel(double volt, double rate, unsigned channel)
{
  if(rate<0)
    throw std::runtime_error("rampVoltageLevel: ramp rate must be positive");

  // Get starting point and direction
  double currentVoltage=measureVoltage(channel);
  double dir=(currentVoltage<volt)?+1:-1; // Direction of ramp

  // Ramp until you get as close as possible to desired voltage with
  // discrete `ramp` steps
  uint32_t nsteps=std::floor(std::fabs(volt-currentVoltage)/rate);
  for(uint32_t i=1;i<nsteps;i++)
    {
      setVoltageLevel(currentVoltage+i*dir*rate, channel);
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }
  // Set final voltage. The final step size should be less than rate
  setVoltageLevel(volt, channel);
}
void IPowerSupply::setVoltageRange(double volt, unsigned channel)
{
  logger(logWARNING) << "setVoltageRange() not implemented for this PS.";
}

void IPowerSupply::setVoltageProtect(double volt, unsigned channel)
{
  logger(logWARNING) << "setVoltageProtect() not implemented for this PS.";
}

double IPowerSupply::getVoltageProtect(unsigned channel)
{
  logger(logWARNING) << "getVoltageProtect() not implemented for this PS.";
  return 0;
}
