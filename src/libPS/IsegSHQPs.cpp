#include "IsegSHQPs.h"

#include <algorithm>
#include <thread>
#include <unistd.h>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(IsegSHQPs)

IsegSHQPs::IsegSHQPs(const std::string& name) :
IPowerSupply(name, {"SHQ-122M","SHQ-222M","SHQ-124M","SHQ-224M","SHQ-126L","SHQ-226L"}), 
  m_nchan(0), m_Vmax(2.e3), m_Imax(1.e-3)
{ }

bool IsegSHQPs::ping()
{
  m_com->sendreceive("#");
  std::string result = m_com->receive();
  return !result.empty();
}

void IsegSHQPs::reset()
{
  m_com->sendreceive("W=003"); // set break time to 3ms
  m_com->receive(); // device sends another \r\n

  if(!ping())
    throw std::runtime_error("No communication after reset.");
}

std::string IsegSHQPs::identify()
{
  m_com->sendreceive("#");
  std::string idn = m_com->receive();
  logger(logDEBUG1) << "IDN raw reply: " << idn;
  if(idn.size()>19) { // reply should be nnnnnn;m.mm;Vmax;Imax  (nn...=SN)
    m_Vmax = std::stod(idn.substr(12, 4)); // given in V
    m_Imax = std::stod(idn.substr(18, 1))*1.e-3; // given in mA
    logger(logDEBUG1) << "SHQ Vmax = " << m_Vmax << ", Imax = " << m_Imax;
    // check no. of channels
    m_nchan = 0;
    for(uint channel=1; channel<4; channel++){
      // check limit status, if chan exist, proper reply
      m_com->sendreceive("T"+std::to_string(channel)); 
      std::string repl = m_com->receive();
      logger(logDEBUG1) << "Tx raw reply: " << repl;
      if(repl=="?WCN") break;
      else{
	unsigned stbits = std::stoi(repl);
	double sign = -1.0;
	if(stbits&0x4) sign = 1.0;
	m_Vsign.push_back(sign);
	m_nchan++;
      }
    }
    if(m_Vmax==2000.){
      if(m_nchan==1)
	idn="SHQ-122M";
      else if(m_nchan==2)
	idn="SHQ-222M";
      else // unknow, should only have 1 or 2 channels
	idn="SHQ-x22M";
    } else if(m_Vmax==4000.){
      if(m_nchan==1)
	idn="SHQ-124M";
      else if(m_nchan==2)
	idn="SHQ-224M";
      else // unknow, should only have 1 or 2 channels
	idn="SHQ-x24M";
    } else if(m_Vmax==6000.){
      if(m_nchan==1)
	idn="SHQ-126L";
      else if(m_nchan==2)
	idn="SHQ-226L";
      else // unknow, should only have 1 or 2 channels
	idn="SHQ-x26L";
    } else {
      idn = "SHQ-???";
    }
  }
  for(unsigned i=0; i<m_nchan; i++){
    std::string fname = "/tmp/PSU_"+m_name+"_ch"+std::to_string(i+1)+"_state";
    FILE *f = fopen(fname.c_str(),"r");
    if(f==0){
      f = fopen(fname.c_str(),"w");
      fprintf(f, "OFF\n");
      logger(logDEBUG2) << "created status file " << fname;
    } else
      logger(logDEBUG2) << "existing status file " << fname;
    fclose(f);
    system(("chmod 664 "+fname).c_str());
    fname = "/tmp/PSU_"+m_name+"_ch"+std::to_string(i+1)+"_volt";
    f = fopen(fname.c_str(),"r");
    if(f==0){
      FILE *f = fopen(fname.c_str(),"w");
      fprintf(f, "0.0\n");
      logger(logDEBUG2) << "created status file " << fname;
    } else
      logger(logDEBUG2) << "existing status file " << fname;
    fclose(f);
    system(("chmod 664 "+fname).c_str());
  }
  return idn;
}

void IsegSHQPs::turnOn(unsigned channel)
{
  if(channel<1 || channel > m_nchan)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  if(getIntState(channel)==-1){
    throw std::runtime_error("Device has tripped, can't turn on; turn off first");
  } else {
    // check if front panel switch is off or status is trippen -> can't turn on on SW
    m_com->sendreceive("S"+std::to_string(channel)); 
    std::string repl = m_com->receive();
    logger(logDEBUG1) << "Sx raw reply: " << repl << ".";
    if(repl==("S"+std::to_string(channel)+"=OFF"))
      throw std::runtime_error("Front panel switcfh is in OFF, can't turn on from SW");
    if(repl==("S"+std::to_string(channel)+"=TRP")){
      setIntState(-1, channel);
      throw std::runtime_error("Device has tripped, can't turn on; turn off first");
    }
  }
  // ON/OFF only available via switch on device -> set to stored voltage
  setIntState(1, channel);
  setVoltageLevel(getIntVolt(channel), channel);
}

void IsegSHQPs::turnOnAll()
{
  for(unsigned channel=1; channel<=m_nchan; channel++) turnOn(channel);
}

void IsegSHQPs::turnOff(unsigned channel)
{
  if(channel<1 || channel > m_nchan)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  // ON/OFF only available via switch on device -> set V to 0
  // remember previous value to restore with turnOn later
  double tmpV = getIntVolt(channel);
  setVoltageLevel(0.0, channel);
  setIntState(0, channel);
  setIntVolt(tmpV, channel);
}

void IsegSHQPs::turnOffAll()
{
  for(unsigned channel=1; channel<=m_nchan; channel++) turnOff(channel);
}

bool IsegSHQPs::isOn(unsigned channel)
{
  if(channel<1 || channel > m_nchan)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->sendreceive("T"+std::to_string(channel)); 
  int stat = std::stoi(m_com->receive());
  if(!(stat&64 || stat&8)) 
    return getIntState(channel);
  else // ERR (-> trip) or front panel off -> can't do anything from SW
    return false;
}

bool IsegSHQPs::isTripped(unsigned channel)
{
  if(channel<1 || channel > m_nchan)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  bool trp = false;
  int state = getIntState(channel);
  if(state==1) { // is on
    m_com->sendreceive("S"+std::to_string(channel)); 
    std::string repl = m_com->receive();
    logger(logDEBUG1) << "Sx raw reply: " << repl;
    bool trp = (repl=="S"+std::to_string(channel)+"=TRP");
    // reading Sx will clear trip -> make sure channel stays off
    if(trp){
      turnOff(channel);
      setIntState(-1, channel);
    }
    return trp;
  } else
    return (state==-1); // tripped before, needs turnOff/On before it's reset
}

void IsegSHQPs::setCurrentLevel(double cur, unsigned channel)
{
  if(channel<1 || channel > m_nchan)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  // NB: this command sets the TRIP limit, if KILL switch
  // is ON; if OFF this is a constant current limit
  char icomm[10];
  sprintf(icomm,"LB%d=%05.0lf", channel, fabs(cur*1.e7)); // limit to be set in unit 0.1 uA
  logger(logDEBUG1) << "Sending command " << std::string(icomm);
  m_com->sendreceive(icomm);
  m_com->receive(); // reply includes another \r\n
}

double IsegSHQPs::getCurrentLevel(unsigned channel)
{
  if(channel<1 || channel > m_nchan)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->sendreceive("LB" + std::to_string(channel));
  std::string repl = m_com->receive();
  logger(logDEBUG1) << "LBx raw reply: " << repl;
  double Imax = IsegSHQPs::atod(repl);
  if(Imax==0.0) Imax = m_Imax; // if SW limit set 0, device HW limit is used
  return Imax;
}

void IsegSHQPs::setCurrentProtect(double maxcur, unsigned channel)
{
  // hard-coded on device -> do nothing
}

double IsegSHQPs::getCurrentProtect(unsigned channel)
{
  if(channel<1 || channel > m_nchan)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->sendreceive("N" + std::to_string(channel));
  std::string repl = m_com->receive();
  logger(logDEBUG1) << "Nx raw reply: " << repl;
  return (IsegSHQPs::atod(repl)/100.*m_Imax);
}

double IsegSHQPs::measureCurrent(unsigned channel)
{
  if(channel<1 || channel > m_nchan)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->sendreceive("I" + std::to_string(channel));
  std::string repl = m_com->receive();
  logger(logDEBUG1) << "Ix raw reply: " << repl;
  return m_Vsign.at(channel-1)*IsegSHQPs::atod(repl);
}

void IsegSHQPs::setVoltageLevel(double volt, unsigned channel)
{
  if(channel<1 || channel > m_nchan)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  setIntVolt(volt, channel);
  if(getIntState(channel)){
    char vcomm[20];
    sprintf(vcomm,"D%d=%07.2lf", channel, fabs(volt)); // NB: sign of voltage hard-coded in device via switch
    m_com->sendreceive(vcomm);
    m_com->receive(); // reply includes another \r\n
  }
}

double IsegSHQPs::getVoltageLevel(unsigned channel)
{
  if(channel<1 || channel > m_nchan)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  if(getIntState(channel)){
    // get polarity from module status
    m_com->sendreceive("T" + std::to_string(channel));
    std::string repl = m_com->receive();
    logger(logDEBUG1) << "Tx raw reply: " << repl;
    m_com->sendreceive("D" + std::to_string(channel));
    repl = m_com->receive();
    logger(logDEBUG1) << "Dx raw reply: " << repl;
    return m_Vsign.at(channel-1)*IsegSHQPs::atod(repl);
  } else {
    logger(logDEBUG1) << "Supply is OFF, returning stored value.";
    return getIntVolt(channel);
  }
}

void IsegSHQPs::setVoltageProtect(double maxvolt, unsigned channel)
{
  setVoltageLevel(maxvolt, channel);
}

double IsegSHQPs::getVoltageProtect(unsigned channel)
{
  return getVoltageLevel(channel);
}

double IsegSHQPs::measureVoltage(unsigned channel)
{
  if(channel<1 || channel > m_nchan)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  m_com->sendreceive("U" + std::to_string(channel));
  std::string repl = m_com->receive();
  logger(logDEBUG1) << "Ux raw reply: " << repl;
  return IsegSHQPs::atod(repl);
}
double IsegSHQPs::atod(std::string svalue_in){
  std::string sexp="0", svalue = svalue_in;
  int pos = svalue.find_last_of("-");
  if(pos==(int)std::string::npos){pos = svalue.find_last_of("+");}
  if(pos!=(int)std::string::npos){
    sexp = svalue.substr(pos,3);
    svalue = svalue.substr(0,pos);
  }
  double value = ((double)std::stoi(svalue))*(double)pow(10.,std::stoi(sexp));
  return value;
}
int IsegSHQPs::getIntState(unsigned channel){
  std::string fname = "/tmp/PSU_"+m_name+"_ch"+std::to_string(channel)+"_state";
  FILE *f = fopen(fname.c_str(),"r");
  int state=0;
  if(f!=0){
    fscanf(f, "%d\n", &state);
    fclose(f);
  }
  return state;
}
void IsegSHQPs::setIntState(int state, unsigned channel){
  // 0: off, 1: on, -1: tripped
  std::string fname = "/tmp/PSU_"+m_name+"_ch"+std::to_string(channel)+"_state";
  remove(fname.c_str());
  FILE *f = fopen(fname.c_str(),"w");
  if(f!=0){
    fprintf(f, "%d\n", state);
    fclose(f);
    system(("chmod 664 "+fname).c_str());
  }
  return;
}
double IsegSHQPs::getIntVolt(unsigned channel){
  double volts = 0.0;
  std::string fname = "/tmp/PSU_"+m_name+"_ch"+std::to_string(channel)+"_volt";
  FILE *f = fopen(fname.c_str(),"r");
  if(f!=0){
    fscanf(f, "%lf\n", &volts);
    fclose(f);
  }
  return volts;
}
void   IsegSHQPs::setIntVolt(double volt, unsigned channel){
  std::string fname = "/tmp/PSU_"+m_name+"_ch"+std::to_string(channel)+"_volt";
  remove(fname.c_str());
  FILE *f = fopen(fname.c_str(),"w");
  if(f!=0){
    fprintf(f, "%lf\n", volt);
    fclose(f);
    system(("chmod 664 "+fname).c_str());
  }
  return;
}
