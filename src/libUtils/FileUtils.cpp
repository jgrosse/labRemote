#include "FileUtils.h"
#include "FileDefs.h" // LABREMOTE_SCHEMA_FILE
#include "Logger.h"

// std/stl
#include <sstream>
#include <iostream>
#include <cstdlib> // std::getenv
#if __linux__
#include <sys/stat.h>
#include <unistd.h> // getcwd, getlogin_r
#include <stdio.h>
#elif (__APPLE__) or (__unix__)
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <libproc.h>
#include <sys/types.h>
#include <unistd.h>
#endif

namespace utils {

std::string labremote_schema_file() {
    std::string schema_path = "";

    // check environment
    char* env = std::getenv("LABREMOTE_RESOURCE_DIR");
    if(env) {
        std::string env_resource_dir(env);
        if (!path_exists(env_resource_dir)) {
            logger(logWARNING) << "Defined environment variable (LABREMOTE_RESOURCE_DIR) points to a non-existent path, cannot find labRemote schema file!";
            return "";
        }
        schema_path = env_resource_dir + "/schema/" + utils::defs::LABREMOTE_SCHEMA_FILENAME;
        if (!path_exists(schema_path)) {
            logger(logWARNING) << "Could not find expected labRemote schema file: " << schema_path;
            return "";
        }
        return schema_path;
    }

    // check labRemote build directory and install directory, if needed
    schema_path = utils::defs::LABREMOTE_BUILD_DIR + "/share/schema/" + utils::defs::LABREMOTE_SCHEMA_FILENAME;
    if (path_exists(schema_path)) {
        return schema_path;
    } else {
        schema_path = utils::defs::LABREMOTE_INSTALL_PREFIX + "/share/labRemote/schema/" + utils::defs::LABREMOTE_SCHEMA_FILENAME;
        if (path_exists(schema_path)) {
            return schema_path;
        }
    }
    return "";
}

bool path_exists(const std::string& path_name) {
    struct stat st;
    return (stat(path_name.c_str(), &st) == 0);
}

}; // namespace utils

