#include "ScopeLock.h"

ScopeLock::ScopeLock(ILockable *ptr)
  : m_ptr(ptr)
{ m_ptr->lock(); }

ScopeLock::ScopeLock(std::shared_ptr<ILockable> ptr)
  : m_ptr(ptr.get())
{ m_ptr->lock(); }

ScopeLock::~ScopeLock()
{ m_ptr->unlock(); }
