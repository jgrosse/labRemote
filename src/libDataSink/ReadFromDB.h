//! \brief read InfluxDB data
/**
 * \param std::shared_ptr<InfluxDBSink> infsk handle of InfluxDB connection
 * \param std::string DBmeasName name of the section in which data field are to be looked for
 * \param std::string sensName name of the field
 * \param std::string sensType type of the field
 * \param int timeoutW timeout to warn of obsolete data
 * \param int timeoutE timeout to throw error flag (ret. val.)
 * \param double readVal most recent value read from DB
 * \return `true` if an error occorred or data not recent
 */
int ReadFromDB(std::shared_ptr<InfluxDBSink> infsk, std::string DBmeasName, 
	       std::string sensName, std::string sensType, int timeoutW, int timeoutE, 
	       double &readVal, std::string tag="Sensor");
