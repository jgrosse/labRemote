#include "ConsoleSink.h"

#include <algorithm>
#include <iostream>
#include <iomanip>

#include "DataSinkRegistry.h"
REGISTER_DATASINK(ConsoleSink)

ConsoleSink::ConsoleSink(const std::string& name)
  : IDataSink(name)
{ }

void ConsoleSink::setConfiguration(const nlohmann::json& config)
{
    // iterate over configuration items
    for(const auto & item : config.items())
    {
        if(item.key() == "column_width")
        {
            m_col_width = static_cast<int>(item.value());
        }
    } // item
}

void ConsoleSink::init()
{ }

void ConsoleSink::setTag(const std::string& name, const std::string& value)
{
  checkTag(name);
  m_tagsString[name]=value;
}

void ConsoleSink::setTag(const std::string& name, int8_t  value)
{
  checkTag(name);
  m_tagsInt8 [name]=value;
}

void ConsoleSink::setTag(const std::string& name, int32_t value)
{
  checkTag(name);
  m_tagsInt32[name]=value;
}

void ConsoleSink::setTag(const std::string& name, int64_t value)
{
  checkTag(name);
  m_tagsInt64[name]=value;
}

void ConsoleSink::setTag(const std::string& name, double value)
{
  checkTag(name);
  m_tagsDouble[name]=value;
}

void ConsoleSink::startMeasurement(const std::string& measurement, std::chrono::time_point<std::chrono::system_clock> time)
{
  // Clean-up last measurement
  m_fields.clear();
  m_fieldsString.clear();
  m_fieldsInt8  .clear();
  m_fieldsInt32 .clear();
  m_fieldsInt64 .clear();
  m_fieldsDouble.clear();

  // State
  m_finalSchema=false;
  m_inMeasurement=true;

  m_currMeasurement=measurement;
}

void ConsoleSink::setField(const std::string& name, const std::string& value)
{
  addField(name);
  m_fieldsString[name]=value;
}

void ConsoleSink::setField(const std::string& name, int8_t  value)
{
  addField(name);
  m_fieldsInt8 [name]=value;
}

void ConsoleSink::setField(const std::string& name, int32_t value)
{
  addField(name);
  m_fieldsInt32[name]=value;
}

void ConsoleSink::setField(const std::string& name, int64_t value)
{
  addField(name);
  m_fieldsInt64[name]=value;
}

void ConsoleSink::setField(const std::string& name, double value)
{
  addField(name);
  m_fieldsDouble[name]=value;
}

void ConsoleSink::recordPoint()
{
  bool same=(m_currMeasurement==m_lastMeasurement)&&(m_fields==m_lastFields)&&(!m_dirty);

  //
  // Print the header
  if(!same)
    { // First time recording point for measurement, print header
      printHeader();
      m_dirty=false;
    }

  //
  // Print the fields
  for(const std::string& field : m_fields)
    {
      if(m_fieldsString.count(field)>0)
      {
    	std::cout << std::setw(m_col_width)
                  << m_fieldsString[field].substr(0, m_col_width-1) << " ";
      }

      if(m_fieldsInt8 .count(field)>0)
      {
        std::cout << std::setw(m_col_width) << m_fieldsInt8[field] << " ";
      }

      if(m_fieldsInt32.count(field)>0)
      {
        std::cout << std::setw(m_col_width) << m_fieldsInt32[field] << " ";
      }

      if(m_fieldsInt64.count(field)>0)
      {
        std::cout << std::setw(m_col_width) << m_fieldsInt64[field] << " ";
      }
      if(m_fieldsDouble.count(field)>0)
      {
        std::cout << std::setw(m_col_width) << std::setprecision(5) << m_fieldsDouble[field] << " ";
      }
    }
  std::cout << std::endl;

  // Update last configuration
  m_lastMeasurement=m_currMeasurement;
  m_lastFields=m_fields;

  m_finalSchema=true;
}

void ConsoleSink::endMeasurement()
{
  m_inMeasurement=false;
}

void ConsoleSink::checkTag(const std::string& name)
{
  if(checkReserved(name))
    throw std::runtime_error("Tag name \""+name+"\" is reserved.");

  if(std::find(m_tags.begin(), m_tags.end(), name)==m_tags.end())
    {
      if(m_inMeasurement)
	throw std::runtime_error("ConsoleSink: Cannot change tag during measurement.");

      m_tags.push_back(name);
    }

  m_dirty=true;
}

void ConsoleSink::addField(const std::string& name)
{
  if(checkReserved(name))
    throw std::runtime_error("Field name \""+name+"\" is reserved.");

  if(std::find(m_fields.begin(), m_fields.end(), name)==m_fields.end())
    {
      if(m_finalSchema)
	throw std::runtime_error("ConsoleSink: Cannot add new fields after the first recordPoint.");
      m_fields.push_back(name);
    }
}

void ConsoleSink::printHeader()
{
  // Print out the tags
  for(const std::string& tag : m_tags)
    {
      std::cout << tag << ": ";

      if(m_tagsString.count(tag)>0)
	std::cout << m_tagsString[tag];

      if(m_tagsInt8 .count(tag)>0)
	std::cout << m_tagsInt8  [tag];

      if(m_tagsInt32.count(tag)>0)
	std::cout << m_tagsInt32 [tag];

      if(m_tagsInt64.count(tag)>0)
	std::cout << m_tagsInt64 [tag];

      if(m_tagsDouble.count(tag)>0)
	std::cout << m_tagsDouble[tag];

      std::cout << std::endl;
    }

  // Print field names
  for(const std::string& field : m_fields)
    {
      std::cout << std::setw(m_col_width) << field << " ";
    }
  std::cout << std::endl;
}
