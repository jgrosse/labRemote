#include <string>
#include <InfluxDBSink.h>
#include <Logger.h>
#include <ReadFromDB.h> 

static std::time_t lastW=0;

int ReadFromDB(std::shared_ptr<InfluxDBSink> infsk, std::string DBmeasName, 
	       std::string sensName, std::string sensType, int timeoutW, int timeoutE, 
	       double &readVal, std::string tag)
{
  int status = 0;
  std::string resp;
  infsk->query(resp, "select * from "+DBmeasName+" where "+tag+" = '"+sensName+"' and time > now() - 60s ");
  auto infj = nlohmann::json::parse(resp);
  int time_id = -1, temp_id = -1;
  bool gti=false, gte=false;
  long unsigned int old_time = 0, new_time = 0;
  uint i=0;
  for(const auto& cols : infj["results"][0]["series"][0]["columns"].items()){
    if(cols.value() == "time") time_id = i;
    if(cols.value() == sensType) temp_id = i;
    i++;
  }
  if(time_id>=0){
    for(const auto& vals : infj["results"][0]["series"][0]["values"].items()){
      nlohmann::json jv = vals.value();
      new_time = jv[time_id];
      if(new_time>old_time){
	if(temp_id>=0){
	  try{
	    readVal = jv[temp_id]; 
	  } catch(...){
	    readVal = -273.15;
	  }
	}
	old_time = new_time;
      }
    }
    // sanity checks
    std::time_t ts = old_time/1e9; // InfluxDB timestamp is in 0.1 ns
    if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts)>timeoutE){
      //logger(logERROR) << "InfluxDB data not updated for more than "+std::to_string(timeoutE)+" s. ABORT!";
      status = 2;
    }
    else if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts)>timeoutW){
      // issue warning printout, max. 1 per minute
      if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-lastW)>60)
	logger(logWARNING) << "InfluxDB data not updated for more than "+std::to_string(timeoutW)+" s!";
      lastW = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
      status = 1;
    }
    
  }
  return status;
}
