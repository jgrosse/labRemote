#include <getopt.h>
#include <string.h>
#include <signal.h>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <thread>
#include <chrono>
#include <sys/stat.h> 
#include <map>
#include <nlohmann/json.hpp>

#include <Logger.h>
#include <ClimateSensorRegistry.h>
#include <ClimateSensorConf.h>
#include <ClimateSensorAnalog.h>
#include <ClimateSensor.h>
#include <DataSinkConf.h>
#include <IDataSink.h>

//------ SETTINGS
std::string configFile="env_monitor.json";
std::string streamName="Climate";
std::string label="all";
std::string DBmeasName="";
//---------------


bool quit=false;
void cleanup(int signum)
{ quit=true; }


std::map<std::string , std::shared_ptr<ClimateSensor> >  createMySensors(ClimateSensorConf cs, std::string label){
  std::map<std::string , std::shared_ptr<ClimateSensor> > mySensors;
  if(label=="all")
    {
      // create all sensors in list
      for (const auto& senscfg : cs.getHardwareConfig()["sensors"]) 
	{
	  std::shared_ptr<ClimateSensor> clsens = cs.getClimateSensor(senscfg["name"]);
	  if(clsens != nullptr) mySensors.insert(std::make_pair(senscfg["name"], clsens));
	}
    } 
  else 
    {
      // split provided name in individual sensors separated by ':'
      std::stringstream checkToken(label); 
      std::string sensName; 
      // Tokenizing w.r.t. ':' 
      while(getline(checkToken, sensName, ':')) 
	{ 
	  std::shared_ptr<ClimateSensor> clsens = cs.getClimateSensor(sensName);
	  if(clsens != nullptr) mySensors.insert(std::make_pair(sensName, clsens));
	}
    }
  return mySensors;
}

void usage(char *argv[])
{
  std::cout << "Usage: "<< argv[0] << 
    " [-c config file] [-s stream name] [-n sensor name(s)] [-m InfluxDB meas. name] [-l] [-o]"<< std::endl;
  std::cout << "   defaults: -c " << configFile << " -s " << streamName<< " -n " << label << std::endl;
  std::cout << "OR: " << argv[0] << " -l" << std::endl;
  std::cout << "   to list all available classes that can be created from json" << std::endl;
}
void listSensors()
{
  std::cout << "Avalable sensor classes:" << std::endl;
  std::vector<std::string> classList = EquipRegistry::listClimateSensor();
  for(std::vector<std::string>::iterator it = classList.begin(); it!=classList.end();it++)
    std::cout << *it << std::endl;
}
int main(int argc, char** argv)
{

  //Parse command-line  
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  bool loop = false;
  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"DBmeas",   required_argument, 0,  'm' },
      {"names",    required_argument, 0,  'n' },
      {"config",   required_argument, 0,  'c' },
      {"stream",   required_argument, 0,  's' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {"list",     no_argument      , 0,  'l' },
      {"loop",     no_argument      , 0,  'o' },
      {0,          0,                 0,  0 }
    };
    
    c = getopt_long(argc, argv, "m:s:c:n:t:dhlo", long_options, &option_index);
    if (c == -1)
      break;

    switch (c)
      {
      case 's':
	streamName = optarg;
	break;
      case 'm':
	DBmeasName = optarg;
	break;
      case 'c':
	configFile = optarg;
	break;
      case 'n':
	label = optarg;
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      case 'l':
	listSensors();
	return 1;
      case 'o':
	loop = true;
	break;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  // read json config.
//   nlohmann::json hardwareConfig;
//   std::ifstream i(configFile);
//   i >> hardwareConfig;
  ClimateSensorConf cs;
  cs.setHardwareConfig(configFile);
  std::map<std::string , std::shared_ptr<ClimateSensor> > mySensors = createMySensors(cs, label);
  
  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> stream = ds.getDataStream(streamName);

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  int oldNerr = 0, nerr=0; // number of consecutive errors
  while(!quit){
    // request data from selected measurement
    for(std::map< std::string,  std::shared_ptr<ClimateSensor> >::iterator it=mySensors.begin();
	it!=mySensors.end(); it++){
      try{
	it->second->read();
      } catch(std::runtime_error &err){
	std::cerr << "Caught unhandled runtime exception: " << err.what() << ", occurance " << nerr 
		  << ", sensor " << it->first << std::endl;
	nerr++;
	//      break;
      } catch(...){
	std::cerr << "Caught unhandled exception, occurance " << nerr << ", sensor " << it->first << std::endl;
	nerr++;
	//      break;
      }
      if(nerr>3) break;
      if(nerr==0){
	stream->setTag("Sensor", it->first);
	stream->startMeasurement((DBmeasName=="")?"environment":DBmeasName, std::chrono::system_clock::now());
	double measval;
	// write all values that are covered into stream
	try{
	  measval = it->second->temperature();
	  stream->setField("temperature", measval);
	} catch(...){}
	try{
	  measval = it->second->humidity();
	  stream->setField("humidity", measval);
	  measval = it->second->dewPoint();
	  stream->setField("dewpoint", measval);
	} catch(...){}
	try{
	  measval = it->second->pressure();
	  stream->setField("pressure", measval);
	} catch(...){}
	stream->recordPoint();
	stream->endMeasurement();
      }
    }
    if(!loop) quit = true;
  }

  return nerr;
}
