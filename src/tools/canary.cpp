#include <fstream>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <signal.h>
#include <getopt.h>

#include <nlohmann/json.hpp>

#include <DataSinkConf.h>
#include <IDataSink.h>
#include <TextSerialCom.h>
#include <Logger.h>

std::string configFile = "";
std::string streamName = "Climate";
std::string arduino = "Canary";
std::string DBmeasName="";

bool quit=false;
void cleanup(int signum)
{ quit=true; }

void usage(char *argv[])
{
  std::cout << "" << std::endl;
  std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cout << "List of options:" << std::endl; 
  std::cout << "-c, --config config-file" << std::endl;
  std::cout << "-s, --stream DB-stream name" << std::endl;
  std::cout << "-m, --DBmeas DB meas. name" << std::endl;
  std::cout << "-a, --arduino Arduino name (default: "<< arduino <<"" << std::endl;
 }

int main(int argc, char** argv){

  if(argc<2){
    usage(argv);
    return -1;
  }

  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"config", required_argument, 0,  'c' },
      {"stream", required_argument, 0,  's' },
      {"arduino",required_argument, 0,  'a' },
      {"DBmeas", required_argument, 0,  'm' },
      {"debug",  no_argument      , 0,  'd' },
      {"help",   no_argument      , 0,  'h' },
      {0,        0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "c:s:a:m:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
      case 'c':
	configFile = optarg;
	break;
      case 's':
	streamName = optarg;
	break;
      case 'a':
	arduino = optarg;
	break;
      case 'm':
	DBmeasName = optarg;
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	usage(argv);
	return 1;
      }
  }

  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> stream = ds.getDataStream(streamName);

  // read json config. for Arduino
  nlohmann::json hardwareConfig;
  std::ifstream i(configFile);
  i >> hardwareConfig;
  nlohmann::json arduConfig;
  for (const auto& hw : hardwareConfig["Com"]) {
    //check label 
    if (hw["name"] == arduino) {
      arduConfig = hw;
      break;
    }
  }
  std::shared_ptr<TextSerialCom> scc = std::make_shared<TextSerialCom>();
  scc->setConfiguration(arduConfig);
  scc->init();

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  while(!quit){
    std::string messg="";
    try{
      messg = scc->receive();
    } catch(...){
      // no data, nothing to worry about
      messg="";
    }
    std::istringstream origStream(messg);
    std::string curLine;
    while (std::getline(origStream, curLine))	{
      // Only process non empty lines.
      if (!curLine.empty()) {
	size_t pos=curLine.find("message: ");
	if(pos!=std::string::npos){
	  curLine.erase(pos, 9); // remove "message: "
	  auto sensors = nlohmann::json::parse(curLine);
	  for(auto& elem : sensors) {
	    std::string sensName = arduino+"-";
	    sensName += elem["sensor"];
	    try{
	      stream->setTag("Sensor", sensName);
	      stream->startMeasurement((DBmeasName=="")?"environment":DBmeasName, std::chrono::system_clock::now());
	      for(auto& vals : elem.items()){
		if(vals.key()!="sensor"){
		  double sensVal = vals.value();
		  stream->setField(vals.key(), sensVal);
		}
	      }
	      stream->recordPoint();
	      stream->endMeasurement();
	    } catch(std::runtime_error &err){
	      std::cerr << "Error writing to InfluxDB: " << std::string(err.what()) << std::endl;
	    } catch(...){
	      std::cerr << "Unknown error writing to InfluxDB" << std::endl;
	    }
	  }
	}
      }
    }
    // sleep 1s
    if(!quit) std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  }

  return 0;
}
