#include <TextSerialCom.h>
#include <thread>
#include <chrono>
#include <string>
#include <iostream>

void usage(char *argv[])
{
  std::cout << "" << std::endl;
  std::cout << "Usage: " << std::endl;
  std::cout <<  argv[0] << " -h: print this help" << std::endl;
  std::cout <<  argv[0] << " [port]: test relay on Arduino at port, e.g. /dev/ttyACM1" << std::endl;
 }
int main(int argc, char** argv){

  char Appath[20];
  sprintf(Appath,"/dev/ttyACM1");
  if(argc==2) {
    if(strcmp(argv[1],"-h")==0){
      usage(argv);
      return 1;
    } else
      sprintf(Appath,"%s", argv[1]);
  } else if(argc>2){
    usage(argv);
    return -1;
  }

  std::shared_ptr<TextSerialCom> sc(new TextSerialCom(Appath, B9600));
  sc->setTermination("\r\n");
  sc->setTimeout(1.);
  sc->init();
  
  std::this_thread::sleep_for(std::chrono::milliseconds(2000));

  // somehow needed to initiate communication to Arduino
  try{
    sc->sendreceive("???");
  } catch(...){
  }
  // now the real test
  try{
    std::cout << "Arduino init reply: " << sc->sendreceive("HELP") << std::endl;
  } catch(std::runtime_error &err){
    std::cerr << "testing Arduino communication: runtime exception: " << err.what() << std::endl;
    return -3;
  } catch(...){
    std::cerr << "testing Arduino communication: unknown exception" << std::endl;
    return -4;
  }

  std::cout << "Command ON - reply: " << sc->sendreceive("RELON") << std::endl;
  std::this_thread::sleep_for(std::chrono::milliseconds(2000));
  std::cout << "Command OFF - reply: " << sc->sendreceive("RELOFF") << std::endl;

  return 0;
}
