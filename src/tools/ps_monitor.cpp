#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <signal.h>

#include "Logger.h"
#include "EquipConf.h"
#include "DataSinkConf.h"
#include "PowerSupplyChannel.h"
#include "IDataSink.h"

//------ SETTINGS
std::string configFile;
std::string channelNames;
std::string streamName;
std::string DBmeasName;
//---------------

bool quit=false;
void cleanup(int signum)
{ quit=true; }

void usage(char *argv[])
{
  std::cerr << "Usage: " << argv[0] << " configfile.json datastreamName channelname(s) DBmeasName" << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -d, --debug       Enable more verbose printout, use multiple for increased debug level" << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "" << std::endl;
}

int main(int argc, char*argv[])
{

  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  //Parse command-line
  int c;
  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"debug",    no_argument      , 0,  'd' },
      {0,          0,                 0,  0 }
    };
    
    c = getopt_long(argc, argv, "t:d", long_options, &option_index);
    if (c == -1)
      break;

    switch (c)
      {
      case 'd':
	logIt::incrDebug();
	break;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  if(optind>argc-3)
    {
      logger(logERROR) << "Missing positional arguments.";
      usage(argv);
      return 1;
    }

  configFile   = argv[optind++];
  streamName   = argv[optind++];
  channelNames = argv[optind++];
  DBmeasName   = argv[optind++];

  logger(logDEBUG) << "Settings:";
  logger(logDEBUG) << " Config file: " << configFile;
  logger(logDEBUG) << " stream name: " << streamName;
  logger(logDEBUG) << " PS channel(s): " << channelNames;

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> stream = ds.getDataStream(streamName);

  // Create hardware database
  EquipConf hw;
  hw.setHardwareConfig(configFile);

  // split provided name in individual sensors separated by ':'
  std::map<std::string , std::shared_ptr<PowerSupplyChannel> > myChannels;
  std::stringstream checkToken(channelNames); 
  std::string chanName; 
  // Tokenizing w.r.t. ':' 
  while(getline(checkToken, chanName, ':')) 
    { 
      logger(logDEBUG) << "Configuring power-supply " << chanName << ".";
      std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(chanName);
      if(PS != nullptr) myChannels.insert(std::make_pair(chanName, PS));
    }

  for(std::map< std::string,  std::shared_ptr<PowerSupplyChannel> >::iterator it=myChannels.begin();
      it!=myChannels.end(); it++){
    try{
      stream->setTag("Channel", it->first);
      stream->startMeasurement(DBmeasName, std::chrono::system_clock::now());
      stream->setField("Voltage", it->second->measureVoltage());
      double curr = it->second->measureCurrent();
      if(it->first.find("HV")!=std::string::npos)
	stream->setField("Current", curr*1.e6);// write in micro-A
      else
	stream->setField("Current", curr);
      if(it->first.find("HV")!=std::string::npos)
	stream->setField("Status", 2*(int)it->second->isTripped());
      stream->recordPoint();
      stream->endMeasurement();
    } catch(std::runtime_error &err){
      std::cerr << "Error writing to InfluxDB: " << std::string(err.what()) << std::endl;
    } catch(...){
      std::cerr << "Unknown error writing to InfluxDB" << std::endl;
    }
  }

  return 0;
}
