#include "TextSerialCom.h"
#include "NTCSensor.h"
#include "Logger.h"
#include <iostream>
#include <getopt.h>
#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>
#include <cstdint>
#include <string>
#include <memory>

// Value to set eeprom to
uint8_t val=0;
// The address of the device port connected to the Arduino
std::string device=""; 

void usage(char *argv[])
{
  std::cerr << "" << std::endl;
  std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cerr << "List of possible COMMAND:" << std::endl;
  std::cerr << "  write -- A V   Write eeprom address A to value V" << std::endl;
  std::cerr << "  read  -- A     Read eeprom value for address A" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -a, --port ADDR   Set the address of the port connected to the Arduino" << std::endl;  
  std::cerr << " -d, --debug         Enable more verbose printout, use multiple for increased debug level"  << std::endl;
  std::cerr << " -h, --help          List the commands and options available"  << std::endl;
  std::cerr << "" << std::endl;
}


int main(int argc, char* argv[]) 
{ 
  //Parse command-line  
  if (argc < 1)
  {
    usage(argv);
    return 1;
   }
  
  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"port",  required_argument, 0,  'a' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "a:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
      case 'a':
	device = optarg;
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  if(device=="") {
      std::cerr << "No device port specified for the Arduino. Aborting." <<std::endl;
      usage(argv);
      return 1;
  }

  logger(logDEBUG) << "Device port: " << device;

  std::string command;
  std::vector<std::string> params;
  if (optind < argc)
    {
      command = argv[optind++];
      std::transform(command.begin(), command.end(), command.begin(), ::tolower);
      while (optind < argc)
        {
	  std::string p(argv[optind++]);
	  std::transform(p.begin(), p.end(), p.begin(), ::tolower);
          params.push_back(p);
        }
    }
  else
    {
      std::cerr << "Required command argument missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }  

  std::shared_ptr<TextSerialCom> com = std::make_shared<TextSerialCom>(device,B9600);
  com->setTermination("\r\n");
  com->setTimeout(20);
  com->init();
  
  logger(logDEBUG) << "Sending commend to arduino.";
  // interpret command

  if (command == "write")
    {
      if (params.size() != 2)
      {
	logger(logERROR) << "Invalid number of parameters to set-eeprom command.";
	logger(logERROR) << "";
	usage(argv);
	return 1;
      }
      
      std::string cmd="EEPROM WRITE ";
      cmd.append(params[0]);
      cmd.append(" ");
      cmd.append(params[1]);
      com->send(cmd);

    }
  if (command == "read")
    {
      if (params.size() != 1)
	{
	  logger(logERROR) << "Invalid number of parameters to get-eeprom command.";
	  logger(logERROR) << "";
	  usage(argv);
	  return 1;
	}

      std::string cmd="EEPROM READ ";
      cmd.append(params[0]);
      com->send(cmd);
      std::string response = com->receive();

      std::cout << response << std::endl;
    }

  return 0;
}
