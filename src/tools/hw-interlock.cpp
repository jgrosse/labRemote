#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <thread>
#include <signal.h>
#include <getopt.h>
#include <sys/file.h>

#include <nlohmann/json.hpp>

#include <ComRegistry.h>
#include <DataSinkConf.h>
#include <IDataSink.h>
#include <InfluxDBSink.h>
#include <TextSerialCom.h>
#include <Logger.h>

bool quit=false;
void cleanup(int signum)
{ quit=true; }

void usage(char *argv[])
{
  std::cout << "" << std::endl;
  std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cout << "List of options:" << std::endl; 
  std::cout << "-c, --config config-file" << std::endl;
  std::cout << "-n, --name interlock name in config file" << std::endl;
 }

int main(int argc, char** argv){

  if(argc<3){
    usage(argv);
    return -1;
  }

  std::string configFile = "";
  std::string DBmeasName = "";
  std::string sinkName   = "db";
  std::string interlName = "";
  std::string ardname   = "";

  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"config",   required_argument, 0,  'c' },
      {"name",     required_argument, 0,  'n' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "c:n:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
      case 'c':
	configFile = optarg;
	break;
      case 'n':
	interlName = optarg;
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	usage(argv);
	return 1;
      }
  }

  //check if config file is provided, if not exit
  if(configFile == ""){
    logger(logERROR) << "no config file name provided -> exit";
    usage(argv);
    return -2;
  }

  // read json config.
  nlohmann::json hardwareConfig;
  std::ifstream i(configFile);
  i >> hardwareConfig;
  for (const auto& coolit : hardwareConfig["interlock"]) {
    //check label 
    if (coolit["name"] == interlName){
      for (const auto& coolcfg : coolit.items()) {
	//std::cout << "Key " << coolcfg.key() << " - " << coolcfg.value() << std::endl;
	if(coolcfg.key()=="sink") {
	  sinkName = coolcfg.value();
	}
	if(coolcfg.key()=="dbmeas") {
	  DBmeasName = coolcfg.value();
	}
	if(coolcfg.key()=="Arduino") {
	  ardname = coolcfg.value();
	}
      }
    }
  }

  //check if mandatory information is provided, if not exit
  if(ardname == ""){
    logger(logERROR) << "no Arduino name provided -> exit";
    return -3;
  }

  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  std::shared_ptr<InfluxDBSink> infsk = std::dynamic_pointer_cast<InfluxDBSink>(sink);
  if(infsk==nullptr){
    logger(logERROR) << "Couldn't find InfluxDB in config. ";
    return -4;
  }

  // create serial interface for Arduino controlling interlock
  logger(logDEBUG2) << "Connecting to Arduino " << ardname;
  // create serial interface for Arduino controlling relay
  std::shared_ptr<ICom> sca(nullptr);
  for (const auto& config : hardwareConfig["Com"]) {
    //check label 
    if (config["name"] == ardname){
      sca=EquipRegistry::createCom(config["protocol"]);
      sca->setConfiguration(config);
      sca->init();  
      std::this_thread::sleep_for(std::chrono::milliseconds(2000));
      // somehow needed to initiate communication to Arduino
      //try{
	//sca->sendreceive("???");
      //} catch(...){
      //}
      break;
    }
  }
  // now the real test
  if(sca==nullptr){
    logger(logERROR) << "Interlock Arduino not found in config";
    return -5;
  }
  std::this_thread::sleep_for(std::chrono::milliseconds(2000));
  // somehow needed to initiate communication to Arduino
  std::string ardrepl="no reply yet";
  try{
    sca->receive();
    logger(logINFO) << "Arduino communication established";
  } catch(std::runtime_error &err){
    logger(logERROR) << "testing Arduino communication: runtime exception: " << err.what();
    return -5;
  } catch(...){
    logger(logERROR) << "testing Arduino communication: unknown exception";
    return -6;
  }
  logger(logDEBUG2) << "Connected to Arduino at port " << ardname;
  // read old messages
  for(int i=0;i<20;i++){
    try{
      sca->receive();
    } catch(...){
      break;
    }
  }

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  // write status to DB
  int oldStatus = -1;
  int status = -1;
  sink->setTag("Interlock", interlName);
  sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
  sink->setField("status", status);
  sink->recordPoint();
  sink->endMeasurement();

  int nerr = 0;
  while(!quit){
    status = 2;
    std::string excmsg="";
    try{
      ardrepl = sca->receive();
      logger(logDEBUG1) << "Arduino replies: ";
      logger(logDEBUG1) << ardrepl;
      nerr = 0;
      // translate response into 0=OK, 1=warning, 2=error, -1 = off
      size_t ipos=ardrepl.find("status");
      if(ipos!=std::string::npos){
	ipos+=9;
	size_t epos = ardrepl.substr(ipos,ardrepl.length()-ipos).find("\n"); 
	logger(logDEBUG1) << "beginning of string: " << ipos << ", end of string: " << epos;
	std::string sstatus = ardrepl.substr(ipos, epos-1);
	if(sstatus == "OFF") status = -1;
	if(sstatus == "GREEN") status = 0;
	if(sstatus == "YELLOW") status = 2;
	if(sstatus == "RED") status = 2;
	logger(logDEBUG) << "Arduino replies status : " << sstatus << ", sent to DB as " << status;
	if(oldStatus!=2 && status==2) { // new alarm: print last information
	  logger(logERROR) << "Arduino reply when going into RED:";
	  logger(logERROR) << ardrepl;
	}
	oldStatus = status;
	// write status to DB
	sink->setTag("Interlock", interlName);
	sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
	sink->setField("status", status);
	sink->recordPoint();
	sink->endMeasurement();
      }
    } catch(std::runtime_error &err){
      excmsg = err.what();
      nerr++;
    } catch(...){
      excmsg = "unknown exception";
      nerr++;
    }
    if(nerr>10){
      logger(logERROR) << "More than 10 errors from Arduino communication, stop - last exception message: " << excmsg;
      return -7;
    }

    // sleep 1s
    if(!quit) std::this_thread::sleep_for(std::chrono::milliseconds(900));

  }

  // write status to DB
  status = -1;
  sink->setTag("Interlock", interlName);
  sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
  sink->setField("status", status);
  sink->recordPoint();
  sink->endMeasurement();
  std::this_thread::sleep_for(std::chrono::milliseconds(1500));
  sink->setTag("Interlock", interlName);
  sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
  sink->setField("status", status);
  sink->recordPoint();
  sink->endMeasurement();

  return 0;
}
