#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <signal.h>

#include <nlohmann/json.hpp>

#include <DataSinkConf.h>
#include <IDataSink.h>
#include <InfluxDBSink.h>
#include <Logger.h>

bool quit=false;
void cleanup(int signum)
{ quit=true; }

void usage(char *argv[])
{
  std::cout << "Usage: "<< argv[0] << " config-file DB-measurement-name ValTag ValName FieldName [-d]"<< std::endl;
}


int main(int argc, char** argv){

  if(argc<6){
    usage(argv);
    return -2;
  }

  std::string configFile = argv[1];
  std::string DBmeasName=argv[2];
  std::string ValTag = argv[3];
  std::string ValName = argv[4];
  std::string FieldName = argv[5];
  std::string sinkName = "db";

  // enable debug print-out
  if(argc==5 && strcmp(argv[4],"-d")==0)
    logIt::incrDebug();

  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  std::shared_ptr<InfluxDBSink> infsk = std::dynamic_pointer_cast<InfluxDBSink>(sink);
  if(infsk==nullptr){
    logger(logERROR) << "Couldn't find InfluxDB in config. ";
    return -3;
  }

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

    std::string resp;
    infsk->query(resp, "select * from "+ DBmeasName+" where "+ValTag+" = '"+ValName+"' and time > now() - 60s ");
    //std::cout<<resp<<std::endl;
    
    auto infj = nlohmann::json::parse(resp);
    int time_id = -1, temp_id = -1;
    double T = -273.15;
    long unsigned int last_time = 0, old_time = 0, new_time = 0;
    uint i=0;
    for(const auto& cols : infj["results"][0]["series"][0]["columns"].items()){
      if(cols.value() == "time") time_id = i;
      if(cols.value() == FieldName) temp_id = i;
      i++;
    }
    if(time_id>=0){
      double Told = -273.15;
      T = -273.15;
      for(const auto& vals : infj["results"][0]["series"][0]["values"].items()){
	nlohmann::json jv = vals.value();
	if(temp_id>=0){
	  try{
	    T = jv[temp_id]; 
	  } catch(...){
	    T = Told;
	  }
	}
	Told = T;
      }
      std::cout << T <<  std::endl;
    }

  return 0;
}
