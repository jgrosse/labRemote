// std/stl
#include <iostream>
#include <iomanip>
#include <fstream>
#include <getopt.h>

// json
#include <nlohmann/json-schema.hpp>
using nlohmann::json;
using nlohmann::json_schema::json_validator;

// labremote
#include "FileUtils.h"

void usage(char* argv[]) {
    std::cerr << "Usage: " << argv[0] << " [OPTIONS] <input> [<schema>] \n";
    std::cerr << "\n";
    std::cerr << "Options:\n";
    std::cerr << " -v, --verbose    Increase verbosity of output\n";
    std::cerr << "Required positional arguments:\n";
    std::cerr << " input            JSON file to validate against the provided schema\n";
    std::cerr << "Optional positional arguments:\n";
    std::cerr << " schema           File containing JSON schema specification (if not provided, will use default labRemote schema)\n";
    std::cerr << "\n";
}

int main(int argc, char* argv[])
{
    bool _verbose = false;
    static struct option long_options[] = {
        {"verbose", no_argument, NULL, 'v'},
        {"help", no_argument, NULL, 'h'},
        {0,0,0,0}
    };
    int c;
    while((c = getopt_long(argc, argv, "vh", long_options, NULL)) != -1) {
        switch(c) {
            case 'v':
                _verbose = true;
                break;
            case 'h':
                usage(argv);
                return 0;
            case '?':
                std::cerr << "Invalid option \"" << (char)(c) << "\" provided\n";
                return 1;
        } // switch
    } // while

    if((argc-optind)>2) {
        std::cerr << "Too many positional arguments provided (expect exactly 2)\n";
        return 1;
    }
    else if((argc-optind) < 1) {
        std::cerr << "Too few positional arguments provided (require exactly 2)\n";
        return 1;
    }

    std::string input_filename = argv[optind++];
    std::string schema_filename = "";
    if((argc-optind) == 2) {
        schema_filename = argv[optind++];
    } else {
        schema_filename = utils::labremote_schema_file();
        if(schema_filename.empty()) {
            std::cerr << "ERROR Failed to find schema file (did it get installed properly during the cmake configuration?)\n";
            return 1;
        }
    }
    if(_verbose) {
        std::cout << "Using schema definition: " << schema_filename << "\n";
    }

    // check that the input files exist
    std::ifstream ifs_schema(schema_filename, std::ios::in);
    if(!ifs_schema.good()) {
        std::cerr << "ERROR Could not open schema file (=" << schema_filename << ")\n";
        return 1;
    }

    std::ifstream ifs_input(input_filename, std::ios::in);
    if(!ifs_input.good()) {
        std::cerr << "ERROR Could not open input file (=" << input_filename << ")\n";
        return 1;
    }

    // parse the json objects from the schema and provided input file
    json j_schema;
    json j_input;
    try {
        j_schema = json::parse(ifs_schema);
    } catch(std::exception& e) {
        std::cerr << "ERROR Unable to parse JSON schema: " << e.what() << "\n";
        return 1;
    }
    
    try {
        j_input = json::parse(ifs_input);
    } catch(std::exception& e) {
        std::cerr << "ERROR Unable to parse input JSON file: " << e.what() << "\n";
        return 1;
    }

    // validate
    json_validator validator;
    try {
        validator.set_root_schema(j_schema);
    } catch(std::exception& e) {
        std::cerr << "ERROR Provided schema is invalid: " << e.what() << "\n";
        return 1;
    }

    try {
        validator.validate(j_input);
        if(_verbose) {
            std::cout << "Validation successful\n";
        }
        return 0;
    } catch(std::exception& e) {
        std::cerr << "ERROR Input fails schema check: " << e.what() << "\n";
        return 1;
    }
}
