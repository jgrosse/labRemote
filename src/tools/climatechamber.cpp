#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <thread>
#include <signal.h>
#include <getopt.h>
#include <sys/file.h>

#include <nlohmann/json.hpp>

#include <EquipConf.h>
#include <IPowerSupply.h>
#include <DataSinkConf.h>
#include <IDataSink.h>
#include <InfluxDBSink.h>
#include <TextSerialCom.h>
#include <Logger.h>
#include <PID_v1.h>
#include <ReadFromDB.h>
#include <simpati.h>

bool quit=false;
void cleanup(int signum)
{ quit=true; }

void usage(char *argv[])
{
  std::cout << "" << std::endl;
  std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cout << "List of options:" << std::endl; 
  std::cout << "-c, --config config-file" << std::endl;
  std::cout << "-n, --name chamber name in config file" << std::endl;
}
void printHelp(std::string clchName, std::map<int, std::string> chnames, std::map<int, std::string> units){
  logger(logINFO) << "Write commands to /tmp/cooling_"<<clchName<< " as follows:";
  logger(logINFO) << "START -> start manual chamber control";
  logger(logINFO) << "STARTPRG [id] -> start automatic chamber control via program [id]";
  logger(logINFO) << "STOP -> stop program or manual chamber control";
  logger(logINFO) << "OFF -> terminate chamber control program";
  logger(logINFO) << "<double> <id> -> set T_chamber for chamber id - possible ids:";
  for(std::map<int, std::string>::iterator it=chnames.begin(); it!=chnames.end(); it++) {
    std::map<int, std::string>::iterator u_it = units.find(it->first);
    if(u_it!= units.end() && units[it->first].length()>1 && units[it->first].c_str()[1]=='C')
      std::cout << it->first << ": " << it->second << std::endl;
  }
  std::cout << std::endl;
}
int main(int argc, char** argv){

  if(argc<2){
    usage(argv);
    return -1;
  }

  std::string configFile = "";
  std::string DBmeasName = "";
  std::vector<int> digChanIds;
  std::vector<std::string> channelNames;
  std::string scdChan = "none";
  std::string lvName = "";
  std::string hvName = "";
  std::string sinkName = "db";
  int         clchid   = 1;
  int         clchport = 7777;
  std::string clchip  = "192.168.1.105";
  std::string ntcName = "";
  std::string dewName = "Ambient_Mod1";
  std::string clchName = "";
  std::string interlockName = "none";

  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"config",   required_argument, 0,  'c' },
      {"name",     required_argument, 0,  'n' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "c:n:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
      case 'c':
	configFile = optarg;
	break;
      case 'n':
	clchName = optarg;
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	usage(argv);
	return 1;
      }
  }

  //check if config file is provided, if not exit
  if(configFile == ""){
    logger(logERROR) << "no config file name provided -> exit";
    usage(argv);
    return -1;
  }

  // read json config.
  nlohmann::json hardwareConfig;
  std::ifstream i(configFile);
  i >> hardwareConfig;
  bool clchFOund = false;
  for (const auto& coolit : hardwareConfig["cooling"]) {
    //check label 
    if (coolit["name"] == clchName){
      clchFOund = true;
      for (const auto& coolcfg : coolit.items()) {
	//std::cout << "Key " << coolcfg.key() << std::endl;
	if(coolcfg.key()=="sink") {
	  sinkName = coolcfg.value();
	}
	if(coolcfg.key()=="dbmeas") {
	  DBmeasName = coolcfg.value();
	}
	if(coolcfg.key()=="NTC") {
	  ntcName = coolcfg.value();
	}
	if(coolcfg.key()=="ChambChan") {
	  for (const auto& pses : coolcfg.value().items()) {
	    channelNames.push_back(pses.value());
	  }	  
	}
	if(coolcfg.key()=="DigChanIds") {
	  for (const auto& pses : coolcfg.value().items()) {
	    digChanIds.push_back(pses.value());
	  }	  
	}
	if(coolcfg.key()=="ChamberPort") {
	  clchport = coolcfg.value();
	}
	if(coolcfg.key()=="ChamberID") {
	  clchid = coolcfg.value();
	}
	if(coolcfg.key()=="ChamberIP") {
	  clchip = coolcfg.value();
	}
	if(coolcfg.key()=="interlock") {
	  interlockName = coolcfg.value();
	}
      }
    }
  }

  if(!clchFOund){
    logger(logERROR) << "No configuration found to climate chamber name " << clchName;
    return -2;
  }

  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  std::shared_ptr<InfluxDBSink> infsk = std::dynamic_pointer_cast<InfluxDBSink>(sink);
  if(infsk==nullptr){
    logger(logERROR) << "Couldn't find InfluxDB in config. ";
    return -3;
  }

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  // communication with the climate chamber
  std::shared_ptr<simpati> mychamber;
  try{
    mychamber = std::make_shared<simpati>( clchip,  clchport,  clchid);
    mychamber->stopMon(); // we'll do monitoring ourselves
  }catch(...){
    logger(logERROR) << "Unknown exception while calling simpati constructor";
    return -4;
  }

  logger(logINFO) << "Climate chamber communication established.";
  std::map<int, std::string> names = mychamber->getNames();
  std::map<int, std::string> units = mychamber->getUnits();
  printHelp(clchName, names, units);

  bool caughtError=false;
  try{ // generally encapsulating main part to catch any exception

  // start monitoring (and later control) loop
  while(!quit){
    // check current state of interlock
    bool ilOK = true;
    if(interlockName!="none"){
      // check if interlock is active -> safe to start
      std::string fname = "/tmp/interlock_"+interlockName;
      FILE *m_fp = fopen(fname.c_str(), "a");
      if(m_fp!=0 && flock(fileno(m_fp), LOCK_EX | LOCK_NB)!=0){ 
	if(errno!=EWOULDBLOCK){
	  logger(logDEBUG) << "Interlock is not locking tmp-file";
	  flock(fileno(m_fp), LOCK_UN);
	  fclose(m_fp);
	  ilOK = false;
	} else {
	  logger(logDEBUG) << "Interlock is locking tmp-file";
	  fclose(m_fp);
	}
      } else {
	flock(fileno(m_fp), LOCK_UN);
	fclose(m_fp);
	logger(logDEBUG) << "Interlock tmp-file does not exist or is not locked";
	ilOK = false;
      }
    }

    // check for external commands
    FILE *in = fopen(("/tmp/cooling_"+clchName).c_str(),"r");
    if(in!=0){
      char line[100];
      if(fgets(line, 99, in)!=0){
	if(line[strlen(line)-1]=='\n') line[strlen(line)-1]='\0';
	std::string sline = line;
	logger(logDEBUG1) << "Read from /tmp/cooling_"+clchName+": " << sline;
	if(sline=="HELP"){
	  printHelp(clchName, names, units);
	} else if(sline=="OFF"){
	  // if in auto mode, stop program
	  if(mychamber->getProgramStatus()) mychamber->stopProgram();
	  // stop everything
	  mychamber->setEnabled(digChanIds, false);
	  quit=true;
	  logger(logINFO) << "Terminating (external command input).";
	} else if(sline=="START"){
	  if(ilOK){
	    // enable all channels from config
	    mychamber->setEnabled(digChanIds, true);
	    logger(logINFO) << "Started chamber control";
	  } else {
	    logger(logWARNING) << "Interlock is preventing this process from starting the chamber";
	  }
	} else if(sline.substr(0,9)=="STARTPRG "){
	  if(ilOK){
	    int pid;
	    sscanf(line, "STARTPRG %d", &pid);
	    // start program
	    logger(logINFO) << "Starting automatic chamber control, program " << pid;
	    mychamber->startProgram(pid);
	    logger(logINFO) << "Started automatic chamber control, program " << pid;
	  } else {
	    logger(logWARNING) << "Interlock is preventing this process from starting the chamber";
	  }
	} else if(sline=="STOP"){
	  // if in auto mode, stop program
	  if(mychamber->getProgramStatus()) mychamber->stopProgram();
	  // stop everything
	  mychamber->setEnabled(digChanIds, false);
	  logger(logINFO) << "Stopped chamber control";
	} else {
	  if(ilOK){
	    double newT;
	    int chid;
	    sscanf(line,"%lf %d\n", &newT, &chid);
	    mychamber->setT(newT, chid);
	    logger(logINFO) << "Setting chamber " << chid << " to T=" << newT << " °C";
	  } else
	    logger(logWARNING) << "Interlock is preventing this process from setting T";
	}
      }
      fclose(in);
      remove(("/tmp/cooling_"+clchName).c_str());
    }

    // retrieve monitored values
    mychamber->updateReadings();
    std::map<int, std::pair<double, double> > vals = mychamber->getVals();

    if(vals.size()>0){
      // store set and measured parameters of chamber
      sink->setTag("Sensor", clchName);
      sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
      for(std::map<int, std::string>::iterator it=names.begin(); it!=names.end(); it++){
	if(vals.find(it->first)!=vals.end()){
	  sink->setField(it->second+"_set", vals.at(it->first).first);
	  sink->setField(it->second+"_meas", vals.at(it->first).second);
	  logger(logDEBUG) << it->second << "(meas)" << vals.at(it->first).second;
	}
      }
      sink->recordPoint();
      sink->endMeasurement();
    }

    // store status information
    sink->setTag("Interlock", clchName);
    sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
    int status, chstat = mychamber->getChamberStatus();
    // translate into Grafana convention
    status = 2;
    if(chstat & 0x8)       // error
      status = 2;
    else if(chstat & 0x4)  // warning
      status = 1;
    else if(chstat & 0x2)  // running
      status = 0;
    else if(chstat == 0x1) // idle
      status = -1;
    else                   // unknown
      status = -999; 
    sink->setField("status", status+10*(int)mychamber->getProgramStatus());
    sink->recordPoint();
    sink->endMeasurement();
    logger(logDEBUG) << "Status: " << status;

    // sleep for 1s
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  }

  } catch(std::runtime_error &err){
    logger(logERROR) << "ERROR unhandled std runtime exception: " << err.what();
    caughtError = true;
  } catch(std::exception& err) {
    logger(logERROR) << "ERROR unhandled std exception: " << err.what();
    caughtError = true;
  } catch(...){
    logger(logERROR) << "ERROR unhandled unknown exception: ";
    caughtError = true;
  }

  // disable all channels from config
  mychamber->setEnabled(digChanIds, false);

  if(caughtError){
    quit = false;
    logger(logINFO) << "In waiting loop due to unexpected error, see above - press Ctrl-C to quit.";
    while(!quit){
      // sleep for 0.5s
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
    return -1;
  }

  return 0;
}
