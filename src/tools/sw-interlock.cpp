#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <thread>
#include <signal.h>
#include <getopt.h>
#include <sys/file.h>

#include <nlohmann/json.hpp>

#include <EquipConf.h>
#include <IPowerSupply.h>
#include <DataSinkConf.h>
#include <IDataSink.h>
#include <InfluxDBSink.h>
#include <TextSerialCom.h>
#include <Logger.h>
#include <ComGate.h>
#include <Logger.h>
#include <ReadFromDB.h>

bool quit=false;
void cleanup(int signum)
{ quit=true; }

void powerDown(std::string configFile, std::string PSname, int tsleep=200){
  bool needRamp = false;
  try{
    EquipConf hw;
    hw.setHardwareConfig(configFile);
    std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(PSname);
    double vmeas = PS->measureVoltage();
    logger(logDEBUG) << "PS channel " << PSname << " voltage at " << vmeas;
    if(fabs(vmeas)>10.0){
      needRamp = true;
      //      PS->rampVoltageLevel(0.0, 5.0);
    } else {
      PS->turnOff();
      PS->getCurrentLevel();
      logger(logINFO) << "PS channel " << PSname << " is off";
    }
  } catch(...){
    logger(logERROR) << "Can't connect to power supply of channel: " << PSname;
  }
  if(needRamp){
    double vstep = 10.0;
    double vcurr = 0.0;
    {
      EquipConf hw;
      hw.setHardwareConfig(configFile);
      std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(PSname);
      vcurr = PS->measureVoltage();
    }
    logger(logDEBUG) << "PS channel " << PSname << " will be ramped down from " << vcurr << " before turning off";
    if(vcurr<0.0){
      vstep *= (-1.0);
      for(double vset = vcurr;vset<=vstep;vset-=vstep){
	EquipConf hw;
	hw.setHardwareConfig(configFile);
	std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(PSname);
	PS->setVoltageLevel(vset);
	double vmeas = PS->measureVoltage();
	logger(logDEBUG) << "PS channel " << PSname << " voltage at " << vmeas;
	std::this_thread::sleep_for(std::chrono::milliseconds(tsleep));
      }
    } else if (vcurr>0.0){
      for(double vset = vcurr;vset>=vstep;vset-=vstep){
	EquipConf hw;
	hw.setHardwareConfig(configFile);
	std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(PSname);
	PS->setVoltageLevel(vset);
	std::this_thread::sleep_for(std::chrono::milliseconds(tsleep));
      }
    }
    EquipConf hw;
    hw.setHardwareConfig(configFile);
    std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(PSname);
    PS->turnOff();
    PS->getCurrentLevel();
    logger(logINFO) << "PS channel " << PSname << " is off";
  }
}

void usage(char *argv[])
{
  std::cout << "" << std::endl;
  std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cout << "List of options:" << std::endl; 
  std::cout << "-c, --config config-file" << std::endl;
  std::cout << "-n, --name interlock name in config file" << std::endl;
 }

int main(int argc, char** argv){

  if(argc<3){
    usage(argv);
    return -1;
  }

  double Tupperlim = 40.0;
  double Tlowerlim = -300.0;
  double dTlowerlim = 2.0;
  double dTsoftlim = 5.0;
  double IbiasMax = 110.;
  double IbiasVarMax = 0.2;

  int timeoutW = 5;
  int timeoutE = 10;

  std::string configFile = "";
  std::string DBmeasName = "";
  std::string DBpwrName = "";
  std::vector<std::string> PSNames1;
  std::vector<std::string> PSNames2;
  std::vector<std::string> PSNames3;
  std::string sinkName = "db";
  std::string coolname = "none";
  std::string daname = "STOP";
  std::string ntcName = "Module";
  std::string dewName = "Ambient";
  std::string chkName = "none";
  std::string cdpName = "none";
  std::string hvName = "none";
  std::string interlName = "";

  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"config",   required_argument, 0,  'c' },
      {"name",     required_argument, 0,  'n' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "c:m:n:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
      case 'c':
	configFile = optarg;
	break;
      case 'n':
	interlName = optarg;
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	usage(argv);
	return 1;
      }
  }

  //check if config file is provided, if not exit
  if(configFile == ""){
    logger(logERROR) << "no config file name provided -> exit";
    usage(argv);
    return -2;
  }

  // read json config.
  nlohmann::json hardwareConfig;
  std::ifstream i(configFile);
  i >> hardwareConfig;
  for (const auto& coolit : hardwareConfig["interlock"]) {
    //check label 
    if (coolit["name"] == interlName){
      for (const auto& coolcfg : coolit.items()) {
	//std::cout << "Key " << coolcfg.key() << " - " << coolcfg.value() << std::endl;
	if(coolcfg.key()=="sink") {
	  sinkName = coolcfg.value();
	}
	if(coolcfg.key()=="dbmeas") {
	  DBmeasName = coolcfg.value();
	}
	if(coolcfg.key()=="dbpwr") {
	  DBpwrName = coolcfg.value();
	}
	if(coolcfg.key()=="NTC") {
	  ntcName = coolcfg.value();
	}
	if(coolcfg.key()=="dewpt") {
	  dewName = coolcfg.value();
	}
	if(coolcfg.key()=="chuck") {
	  chkName = coolcfg.value();
	}
	if(coolcfg.key()=="coldplate") {
	  cdpName = coolcfg.value();
	}
	if(coolcfg.key()=="bias") {
	  hvName = coolcfg.value();
	}
	if(coolcfg.key()=="PSchan1") {
	  for (const auto& pses : coolcfg.value().items()) {
	    PSNames1.push_back(pses.value());
	  }	  
	}
	if(coolcfg.key()=="PSchan2") {
	  for (const auto& pses : coolcfg.value().items()) {
	    PSNames2.push_back(pses.value());
	  }	  
	}
	if(coolcfg.key()=="PSchan3") {
	  for (const auto& pses : coolcfg.value().items()) {
	    PSNames3.push_back(pses.value());
	  }	  
	}
	if(coolcfg.key()=="cooling") {
	  coolname = coolcfg.value();
	}
	if(coolcfg.key()=="dewaction") {
	  daname = coolcfg.value();
	}
	if(coolcfg.key()=="Tupperlim") {
	  Tupperlim = coolcfg.value();
	}
	if(coolcfg.key()=="Tlowerlim") {
	  Tlowerlim = coolcfg.value();
	}
	if(coolcfg.key()=="dTlowerlim") {
	  dTlowerlim = coolcfg.value();
	}
	if(coolcfg.key()=="dTsoftlim") {
	  dTsoftlim = coolcfg.value();
	}
	if(coolcfg.key()=="IbiasMax") {
	  IbiasMax = coolcfg.value();
	}
	if(coolcfg.key()=="IbiasVarMax") {
	  IbiasVarMax = coolcfg.value();
	}
	if(coolcfg.key()=="timeoutW") {
	  timeoutW = coolcfg.value();
	}
	if(coolcfg.key()=="timeoutE") {
	  timeoutE = coolcfg.value();
	}
      }
    }
  }

  if(ntcName == ""){
    logger(logERROR) << "no NTC sensor name provided -> exit";
    return -2;
  }
  if(dewName == ""){
    logger(logERROR) << "no dewpoint sensor name provided -> exit";
    return -3;
  }

  // create lock file
  std::string fname = "/tmp/interlock_"+interlName;
  FILE *m_fp = fopen(fname.c_str(), "r");
  if(m_fp!=nullptr){
    fclose(m_fp);
    m_fp=0;
    system(("chmod 664 "+fname).c_str());
  } else {
    system(("touch "+fname+"; chmod 664 "+fname).c_str());
  }

  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  std::shared_ptr<InfluxDBSink> infsk = std::dynamic_pointer_cast<InfluxDBSink>(sink);
  if(infsk==nullptr){
    logger(logERROR) << "Couldn't find InfluxDB in config. ";
    return -5;
  }

  // Module PSU controlled by interlock: check connection
  // NB: don't create supplies permanently, otherwise some (e.g. Keithley) will be locked!
  {
    EquipConf hw;
    hw.setHardwareConfig(configFile);
    for(std::vector<std::string>::iterator PSit=PSNames1.begin(); PSit!=PSNames1.end(); PSit++){
      try{
	hw.getPowerSupplyChannel(*PSit);
      } catch(...){
	logger(logERROR) << "Can't connect to power supply of channel: " << (*PSit);
	return -6;
      }
    }
  }

  // locking lock file to signal "powersupply" we're ready to go
  m_fp = fopen(fname.c_str(), "a");
  if(m_fp!=0 && flock(fileno(m_fp), LOCK_EX)!=0){ 
    if(errno==EBADF) logger(logERROR) << "fd is not an open file descriptor";
    else if(errno==EINTR) logger(logERROR) << "While waiting to acquire a lock, the  call  was  interrupted";
    else if(errno==EINVAL) logger(logERROR) << "operation is invalid";
    else if(errno==ENOLCK) logger(logERROR) << "kernel ran out of lock memory";
    else logger(logERROR) << "unknown flock error";
    fclose(m_fp);
    m_fp = 0;
    return -7;
  }

  // print information:
  logger(logINFO) << "Interlock \"" << interlName << "\" is now active, acting on the following channels or devices:";
  std::string msg = "Overheating: PSU channels ";
  for(std::vector<std::string>::iterator PSit=PSNames1.begin(); PSit!=PSNames1.end(); PSit++){
    msg += (*PSit)+", ";
  }
  if(PSNames1.size()>0) logger(logINFO) << msg;
  msg = "Dewpoint: PSU channels ";
  for(std::vector<std::string>::iterator PSit=PSNames2.begin(); PSit!=PSNames2.end(); PSit++){
    msg += (*PSit)+", ";
  }
  if(PSNames2.size()>0) logger(logINFO) << msg;
  msg = "Bias current: PSU channels ";
  for(std::vector<std::string>::iterator PSit=PSNames3.begin(); PSit!=PSNames3.end(); PSit++){
    msg += (*PSit)+", ";
  }
  if(PSNames3.size()>0) logger(logINFO) << msg;
  if(coolname!="none") logger(logINFO) << "Cooling: " << coolname;
  logger(logINFO) << "Press Ctrl-C to stop interlock process.";

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  double Tntc = -273.15;
  double Tdew = -273.15;
  double Tchk = -273.15;
  double Tcdp = -273.15;
  double Ibias = 0.0;
  double Vbias = 0.0;
  double Vbias_prev = 0.0;
  const int ilPts=10;
  double IbiasArr[ilPts];
  int nIbiasPt = 0;
  int nVbiasPt = 0;
  int iIbiasPt = 0;
  double IbiasAvg = 0.1;

  int powerOff=0, status=0, HVstatus=0, Ndew=0;
  // write status to DB
  sink->setTag("Interlock", interlName);
  sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
  sink->setField("status", status);
  sink->recordPoint();
  sink->endMeasurement();

  while(!quit){

    /****************
     retrieve values from InfluxDB for interlock decision
    ****************/

    // dewpoint readings
    status = ReadFromDB(infsk, DBmeasName, dewName, "dewpoint", timeoutW, timeoutE, Tdew);
    logger(logDEBUG1) << "Read value T_dew=" << Tdew << "C";
    if(status==2){ 
      quit = true;
      powerOff = 1; // no data -> go for overhat scenario
    }
    // NTC readings
    if(!quit) {
      status = ReadFromDB(infsk, DBmeasName, ntcName, "temperature", timeoutW, timeoutE, Tntc);
      if(status==2){ 
	quit = true;
	powerOff = 1; // no data -> go for overhat scenario
      }
      logger(logDEBUG1) << "Read value T_NTC=" << Tntc;
    }

    // chuck T readings
    if(!quit && chkName!="none") {
      status = ReadFromDB(infsk, DBmeasName, chkName, "temperature", timeoutW, timeoutE, Tchk);
      if(status==2){ 
	quit = true;
	powerOff = 1; // no data -> go for overhat scenario
      }
      logger(logDEBUG1) << "Read value T_chuck=" << Tchk;
    }

    // cold plate T readings
    if(!quit && cdpName!="none") {
      status = ReadFromDB(infsk, DBmeasName, cdpName, "temperature", timeoutW, timeoutE, Tcdp);
      if(status==2){ 
	quit = true;
	powerOff = 1; // no data -> go for overhat scenario
      }
      logger(logDEBUG1) << "Read value T_coldpl=" << Tcdp;
    }

    // cooling status readings
    if(!quit && coolname!="none") {
      double cstat=-1;
      status = ReadFromDB(infsk, "Interlock", coolname, "status", timeoutW, timeoutE, cstat);
      if(status==0){ // cooling control might be off, so ignore obsolete valuers
	if(cstat==2){
	  quit = true; // quits loop and thus triggers power off
	  status = 2;
	  powerOff = 1;
	}
	logger(logDEBUG1) << "Read cooling status: " << cstat;
      }
    }
    if(!quit && hvName!="none") {
      // for Ibias checks, check if HV is ramped
      char rname[20];
      sprintf(rname, "/tmp/%s_hvramp", interlName.c_str());
      FILE *rampf = fopen(rname,"r");
      if(rampf!=0){
	nVbiasPt = 0;
	nIbiasPt = 0;
	fclose(rampf);
      }
      // read from DB
      status = ReadFromDB(infsk, DBpwrName, hvName, "Voltage", timeoutW, timeoutE, Vbias, "Channel");
      status = ReadFromDB(infsk, DBpwrName, hvName, "Current", timeoutW, timeoutE, Ibias, "Channel");
      if(Ibias>-273.0 && Vbias>-273.0){ // exclude erroneous readings
	if(status==2){ 
	  quit = true;
	  powerOff = 1; // no data -> go for overhat scenario
	}
	logger(logDEBUG1) << "Read value V_bias=" << Vbias << "V";
	logger(logDEBUG1) << "Read value I_bias=" << Ibias << "μA";
	nVbiasPt++;
// 	if((1.05*Vbias_prev)>Vbias || Vbias>(0.95*Vbias_prev) || fabs(Vbias_prev)<0.1)
// 	  nVbiasPt = 0;
	if(nVbiasPt>10){  // HV is stable (no ramping)
	  // store Ibias in array
	  IbiasArr[iIbiasPt] = Ibias;
	  if(nIbiasPt<ilPts) nIbiasPt++;
	  iIbiasPt++; if(iIbiasPt>=ilPts) iIbiasPt=0;
	}
	logger(logDEBUG2) << "Read "<< nIbiasPt << " values of I_bias, now at index " << iIbiasPt << " with " << nVbiasPt
			  << " stable voltage points";
	Vbias_prev = Vbias;
	if(nIbiasPt==ilPts){
	  IbiasAvg = 0.;
	  for(int i=0;i<ilPts;i++){
	    IbiasAvg += IbiasArr[i];
	  }
	  IbiasAvg/=(double)ilPts;
	  logger(logDEBUG1) << "Read value average I_bias=" << IbiasAvg << "μA";
	}
      } else {
	status = 1;
	logger(logWARNING) << "Problem reading bias voltage or current from DB: V=" << Vbias << ", I=" << Ibias;
	Vbias = Vbias_prev;
	Ibias = IbiasAvg;
      }
    }

    /***************************
     the actual interlock checks
    ****************************/

    // NTC or coldplate must not be too hot (coldplate not effective if not declared)
    if(Tntc>Tupperlim || (Tcdp>Tupperlim && cdpName!="none")){
      logger(logERROR) << "SW interlock fired (overheating) -> will now shut down power supplies"
		       << ((coolname!="none")?" and cooling":" ");
      if(cdpName!="none")
	logger(logINFO) << "Last read values: T_NTC = " << Tntc << "C, T_coldpl = " << Tcdp << "C";
      else
	logger(logINFO) << "Last read values: T_NTC = " << Tntc << "C";
      quit = true; // quits loop and thus triggers power off
      status = 2;
      powerOff = 1;
    }

    // NTC must not get too cold (actually also covers disconnection)
    if(!quit && Tntc<Tlowerlim){
      logger(logERROR) << "SW interlock fired (too cold) -> will now shut down power supplies"
		       << ((coolname!="none")?" and cooling":" ");
      logger(logINFO) << "Last read values: T_NTC = " << Tntc << "C";
      quit = true; // quits loop and thus triggers power off
      status = 2;
      powerOff = 1;
    }

    // chuck must not get too close to dewpoint (not effective if no chuck sensor named)
    if(!quit && (Tchk-Tdew)<dTlowerlim && chkName!="none"){
      logger(logERROR) << "SW interlock fired (dewpoint) -> will now shut down power supplies"
		       << ((coolname!="none")?" and cooling":" ");
      logger(logINFO) << "Last read values: T_chuck = " << Tchk << "C, T_dew = " << Tdew << "C";
      quit = true; // quits loop and thus  triggers power off
      status = 2;
      powerOff = 2;
    }

    // Ibias must be low enough
    if(!quit && fabs(Ibias)>IbiasMax && nVbiasPt>10 && hvName!="none"){
      logger(logERROR) << "SW interlock fired (Ibias too high) -> will now shut down power supplies"
		       << ((coolname!="none")?" and cooling":" ");
      logger(logINFO) << "Last read values: I_bias = " << Ibias << "μA";
      if(status<2) status = 1;
      HVstatus = 1;
      // Ileak issue: just power off HV
      for(std::vector<std::string>::iterator PSit=PSNames3.begin(); PSit!=PSNames3.end(); PSit++){
	powerDown(configFile, *PSit, 2000);
	// write status to DB
	sink->setTag("Interlock", interlName);
	sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
	sink->setField("status", status);
	sink->recordPoint();
	sink->endMeasurement();
      }
    }
    // Ibias must not fluctuate too much in case we have sufficient current (>0.1uA)
    if(!quit && fabs(IbiasAvg)>0.1 && fabs(Ibias-IbiasAvg)>IbiasVarMax*fabs(IbiasAvg) && 
       nIbiasPt==ilPts && nVbiasPt>10 && hvName!="none"){
      logger(logERROR) << "SW interlock fired (Ibias fluctuation) -> will now shut down power supplies"
		       << ((coolname!="none")?" and cooling":" ");
      logger(logINFO) << "Last read values: I_bias = " << Ibias << "μA with avg. = " << IbiasAvg << "μA";
      if(status<2) status = 1;
      HVstatus = 1;
      // Ileak issue: just power off HV
      for(std::vector<std::string>::iterator PSit=PSNames3.begin(); PSit!=PSNames3.end(); PSit++){
	powerDown(configFile, *PSit, 2000);
	// write status to DB
	sink->setTag("Interlock", interlName);
	sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
	sink->setField("status", status);
	sink->recordPoint();
	sink->endMeasurement();
      }
    }

    // soft action with wider limit
    if(!quit && (Tchk-Tdew)<dTsoftlim && chkName!="none"){
      if(Ndew>10) Ndew=0;
      if(Ndew==0){
	if(status<2) status = 1;
	logger(logWARNING) << "SW interlock requested cooling temperature change to avoid dewpoint issues.";
	FILE *cin = fopen(("/tmp/cooling_"+coolname).c_str(), "w");
	fprintf(cin, "+ 5.0\n");
	fclose(cin);
      }
      Ndew++;
    } else {
      Ndew = 0;
    }
    // remember bad HV status as warning
    if(HVstatus) status = 1;

    // write status to DB
    sink->setTag("Interlock", interlName);
    sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
    sink->setField("status", status);
    sink->recordPoint();
    sink->endMeasurement();

    // sleep 1s
    if(!quit) std::this_thread::sleep_for(std::chrono::milliseconds(700));

  }

  logger(logDEBUG) << "Power-off mode: " << powerOff;

  if(powerOff){
    // release lock if set to indicate power-on is blocked
    if(m_fp!=0){
      flock(fileno(m_fp), LOCK_UN);
      fclose(m_fp);
    }
    // power off all PS registered with this interlock task
    if(powerOff == 1){ // overheat: just power off
      for(std::vector<std::string>::iterator PSit=PSNames1.begin(); PSit!=PSNames1.end(); PSit++){
	powerDown(configFile, *PSit);
	// write status to DB
	sink->setTag("Interlock", interlName);
	sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
	sink->setField("status", status);
	sink->recordPoint();
	sink->endMeasurement();
      }
    } else if(powerOff == 2){ // dew point: leave LV on until NTC warmed up
      uint nPS = PSNames2.size();
      if(nPS>0){
	for(uint iPS = 0; iPS<(nPS-1); iPS++){
	  powerDown(configFile, PSNames2.at(iPS));
	  // write status to DB
	  sink->setTag("Interlock", interlName);
	  sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
	  sink->setField("status", status);
	  sink->recordPoint();
	  sink->endMeasurement();
	}
	// last one is LV (by convention) -> wait for NTC to have reached >20C
	quit = !(nPS>0);
	logger(logINFO) << "Waiting for warm up to power down " << Tntc << "; press Ctrl-C to proceed";
	while(!quit && Tntc<20.0){
	  quit = ReadFromDB(infsk, DBmeasName, ntcName, "temperature", timeoutW, timeoutE, Tntc);
	  logger(logDEBUG1) << "Read value T_NTC=" << Tntc;
	  // write status to DB
	  sink->setTag("Interlock", interlName);
	  sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
	  sink->setField("status", status);
	  sink->recordPoint();
	  sink->endMeasurement();
	  // sleep 1s
	  if(!quit) std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
	powerDown(configFile, PSNames2.at(nPS-1));
      }
      // write status to DB
      sink->setTag("Interlock", interlName);
      sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
      sink->setField("status", status);
      sink->recordPoint();
      sink->endMeasurement();
    }
    // communication to cooling system via files
    if(coolname!="none"){
      std::vector<std::string> cmdstrs;
      std::istringstream f(daname);
      std::string s;    
      while (getline(f, s, ';')) cmdstrs.push_back(s);
      for(std::vector<std::string>::iterator it = cmdstrs.begin(); it!=cmdstrs.end();it++){
	FILE *cin = fopen(("/tmp/cooling_"+coolname).c_str(), "w");
	fprintf(cin, "%s\n", it->c_str());
	logger(logDEBUG) << "Cooling command " << (*it) << " sent";
	fclose(cin);
	// wait for file to be read
	std::this_thread::sleep_for(std::chrono::milliseconds(5000));
      }
      logger(logINFO) << "Cooling off sent to " << coolname;
    }
    // tell user and enable waiting loop to be stopped only manually
    quit = false;
    logger(logWARNING) << "Power-on prevented on all controlled devices: re-start interlock process manually.";
  }

  if(!quit) logger(logWARNING) << "Press Ctrl-C to quit interlock process";
  while(!quit){
    sink->setTag("Interlock", interlName);
    sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
    sink->setField("status", status);
    sink->recordPoint();
    sink->endMeasurement();
    // sleep 1s
    if(!quit) std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  }

  // set status back to 0 -> process is off
  sink->setTag("Interlock", interlName);
  sink->startMeasurement(DBmeasName, std::chrono::system_clock::now());
  sink->setField("status", -1);
  sink->recordPoint();
  sink->endMeasurement();
  return 0;
}
