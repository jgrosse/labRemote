#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <thread>
#include <signal.h>
#include <getopt.h>
#include <sys/file.h>

#include <nlohmann/json.hpp>

#include <ComRegistry.h>
#include <EquipConf.h>
#include <IPowerSupply.h>
#include <DataSinkConf.h>
#include <IDataSink.h>
#include <InfluxDBSink.h>
#include <TextSerialCom.h>
#include <Logger.h>
#include <PID_v1.h>
#include <ComGate.h>
#include <ReadFromDB.h>

bool quit=false;
void cleanup(int signum)
{ quit=true; }

void usage(char *argv[])
{
  std::cout << "" << std::endl;
  std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cout << "List of options:" << std::endl; 
  std::cout << "-c, --config config-file" << std::endl;
  std::cout << "-n, --name peltier name in config file" << std::endl;
  std::cout << "-t, --target target-T (default: 20)" << std::endl;
 }

void printHelp(std::string peltName){
  logger(logINFO) << "Write commands to /tmp/cooling_"<<peltName<< " as follows:";
  logger(logINFO) << "HELP -> print this text";
  logger(logINFO) << "START -> start Peltier control";
  logger(logINFO) << "STOP -> stop Peltier control";
  logger(logINFO) << "OFF -> terminate Peltier control program";
  logger(logINFO) << "RES -> reset stauts of Peltier control program";
  logger(logINFO) << "<double> [double] -> set T_target (and optionally T_chiller)";
  logger(logINFO) << "+ <double> [double] -> increase T_target (and optionally T_chiller) by given value";
  logger(logINFO) << "- <double> [double] -> decrease T_target (and optionally T_chiller) by given value";
  std::cout << std::endl;
}

double setTchl(double Ttarg)
{
  double Tchl = Ttarg;
  if(Ttarg>15.)       Tchl -= 10.0;
  else if(Ttarg>10.)  Tchl -= 5.0;
  else if(Ttarg<-10.) Tchl += 5.0;
  else if(Ttarg<-15.) Tchl += 10.0;

  if(Tchl<-45.0) Tchl = -45.0;

  return Tchl;
}
int main(int argc, char** argv){

  if(argc<3){
    usage(argv);
    return -1;
  }

  double kp = 1.0;
  double ki = 0.1;
  double kd = 0.2;
  int timeoutW = 5;
  int timeoutE = 10;
  bool executePID=false;

  double Ttarg = 20.;
  std::string configFile = "";
  std::string DBmeasNameE = "";
  std::string DBmeasNameP = "";
  std::vector<std::string> channelNames;
  std::string scdChan = "none";
  std::string lvName = "";
  std::string hvName = "";
  std::string sinkName = "db";
  std::string ardname = "Arduino";
  std::string chname = "none";
  std::string ntcName = "";
  std::string dewName = "Ambient_Mod1";
  std::string peltName = "";
  std::string interlockName = "none";

  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"config",   required_argument, 0,  'c' },
      {"ttarget",  required_argument, 0,  't' },
      {"name",     required_argument, 0,  'n' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "c:t:n:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
      case 'c':
	configFile = optarg;
	break;
      case 't':
	Ttarg = std::atof(optarg);
	break;
      case 'n':
	peltName = optarg;
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	usage(argv);
	return 1;
      }
  }

  //check if config file is provided, if not exit
  if(configFile == ""){
    logger(logERROR) << "no config file name provided -> exit";
    usage(argv);
    return -1;
  }

  // read json config.
  nlohmann::json hardwareConfig;
  std::ifstream i(configFile);
  i >> hardwareConfig;
  bool peltFound = false;
  for (const auto& coolit : hardwareConfig["cooling"]) {
    //check label 
    if (coolit["name"] == peltName){
      peltFound = true;
      for (const auto& coolcfg : coolit.items()) {
	//std::cout << "Key " << coolcfg.key() << std::endl;
	if(coolcfg.key()=="sink") {
	  sinkName = coolcfg.value();
	}
	if(coolcfg.key()=="dbmeasE") {
	  DBmeasNameE = coolcfg.value();
	}
	if(coolcfg.key()=="dbmeasP") {
	  DBmeasNameP = coolcfg.value();
	}
	if(coolcfg.key()=="NTC") {
	  ntcName = coolcfg.value();
	}
	if(coolcfg.key()=="PeltChan") {
	  for (const auto& pses : coolcfg.value().items()) {
	    channelNames.push_back(pses.value());
	  }	  
	}
	if(coolcfg.key()=="Arduino") {
	  ardname = coolcfg.value();
	}
	if(coolcfg.key()=="Chiller") {
	  chname = coolcfg.value();
	}
	if(coolcfg.key()=="interlock") {
	  interlockName = coolcfg.value();
	}
	if(coolcfg.key()=="kp") {
	  kp = coolcfg.value();
	}
	if(coolcfg.key()=="ki") {
	  ki = coolcfg.value();
	}
	if(coolcfg.key()=="kd") {
	  kd = coolcfg.value();
	}
	if(coolcfg.key()=="timeoutW") {
	  timeoutW = coolcfg.value();
	}
	if(coolcfg.key()=="timeoutE") {
	  timeoutE = coolcfg.value();
	}
      }
    }
  }

  if(!peltFound){
    logger(logERROR) << "No configuration found to peltier name " << peltName;
    return -2;
  }
  if(channelNames.size()==0){
    logger(logERROR) << "no PSU channel name provided -> exit";
    return -2;
  }
  if(ntcName == ""){
    logger(logERROR) << "no NTC sensor name provided -> exit";
    return -2;
  }
  if(DBmeasNameE == ""){
    logger(logERROR) << "no DB envir.-label provided -> exit";
    return -2;
  }
  if(DBmeasNameP == ""){
    logger(logERROR) << "no DB pwr.-label provided -> exit";
    return -2;
  }

  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  std::shared_ptr<InfluxDBSink> infsk = std::dynamic_pointer_cast<InfluxDBSink>(sink);
  if(infsk==nullptr){
    logger(logERROR) << "Couldn't find InfluxDB in config. ";
    return -3;
  }

  // create serial interface for chiller and chiller itself
  double Tchl = Ttarg;
  //std::shared_ptr<TextSerialCom> scc;
  std::shared_ptr<ICom> scc(nullptr);
  std::shared_ptr<IChiller> chiller = nullptr;
  logger(logDEBUG2) << "Connecting to chiller " << chname;
  if(chname!="none"){
    for (const auto& config : hardwareConfig["Com"]) {
      //check label 
      if (config["name"] == chname){
	scc=EquipRegistry::createCom(config["protocol"]);
	scc->setConfiguration(config);
	scc->init();  
	chiller = std::make_shared<ComGate>();
	chiller->setCom(scc);
	try{
	  chiller->init();
	  if(!chiller->getStatus()){
	    logger(logERROR) << "Chiller is in bad status -> return";
	    return -4;
	  }
	} catch(...){
	  logger(logERROR) << "Chiller is in bad status -> return";
	  return -4;
	}
	logger(logINFO) << "Chiller communication established";
	chiller->turnOn();
	chiller->setTargetTemperature(Tchl);
	logger(logDEBUG2) << "Connected to chiller " << chname;
	break;
      }
    }
    if(scc==nullptr) logger(logERROR) << "Chiller config not found";
  } else
    logger(logINFO) << "No chiller specified";

  // create serial interface for Arduino controlling relay
  std::shared_ptr<ICom> sca(nullptr);
  for (const auto& config : hardwareConfig["Com"]) {
    //check label 
    if (config["name"] == ardname){
      sca=EquipRegistry::createCom(config["protocol"]);
      sca->setConfiguration(config);
      sca->init();  
      std::this_thread::sleep_for(std::chrono::milliseconds(2000));
      // somehow needed to initiate communication to Arduino
      try{
	sca->sendreceive("???");
      } catch(...){
      }
      break;
    }
  }
  // now the real test
  if(sca==nullptr){
    logger(logERROR) << "Relay-Arduino not found in config";
    return -5;
  }
  try{
    sca->sendreceive("???");
    logger(logINFO) << "Relay-Arduino communication established";
  } catch(std::runtime_error &err){
    logger(logERROR) << "testing Arduino communication: runtime exception: " << err.what();
    return -5;
  } catch(...){
    logger(logERROR) << "testing Arduino communication: unknown exception";
    return -6;
  }

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  logger(logINFO) << "T_target: " << Ttarg;
  double Iset, Imax=8.0, Istep = 0.05, Vmax = 15.0;
  double Tntc = 20.0;
  double Tdew = -273.15;
  // set up PID class
  PID pid(&Tntc, &Iset, &Ttarg, kp, ki, kd, P_ON_E, 1);
  pid.SetOutputLimits((-1.)*Imax, Imax);
  pid.SetMode(1);

  // initialise power supplies for Peltier
  logger(logINFO) << "Using Peltier PSU channels";
  for(std::vector<std::string>::iterator it=channelNames.begin();
      it!=channelNames.end(); it++) logger(logINFO) << (*it);
  {
    EquipConf hw;
    hw.setHardwareConfig(configFile);
    Iset = 0.;
    for(std::vector<std::string>::iterator it=channelNames.begin();
	it!=channelNames.end(); it++) {
      try{
	logger(logDEBUG1) << "Connecting to Peltrier PSU channel " << (*it);
	std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel((*it));
	PS->setVoltageLevel(Vmax);
	PS->setCurrentLevel(0.);
	double curr = PS->getCurrentLevel();
	logger(logDEBUG1) << "Current on channel: " << curr;
      } catch(...){
	logger(logERROR) << "Can't connect to Peltier supply(/ies): " << (*it);
	return -5;
      }
    }
    std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(channelNames.at(0));
    if(channelNames.size()==1){
      PS->turnOn();
    } else {
      logger(logDEBUG1) << "Turning all Peltrier PSU channels on.";
      PS->getPowerSupply()->turnOnAll();
    }
    double curr = PS->measureCurrent();
    logger(logDEBUG1) << "Meas. current on channel: " << curr;
  }
  printHelp(peltName);

  bool atTarg = false;
  int status = 0;
  int oldStatus = 0;
  while(!quit){

    // check if interlock is active -> safe to power on
    bool ilOK = true;
    if(interlockName!="none"){
      std::string fname = "/tmp/interlock_"+interlockName;
      FILE *m_fp = fopen(fname.c_str(), "a");
      if(m_fp!=0 && flock(fileno(m_fp), LOCK_EX | LOCK_NB)!=0){ 
	if(errno!=EWOULDBLOCK){
	  logger(logDEBUG) << "Interlock is not locking tmp-file";
	  fclose(m_fp);
	  flock(fileno(m_fp), LOCK_UN);
	  ilOK = false;
	} else {
	  logger(logDEBUG) << "Interlock is locking tmp-file";
	  fclose(m_fp);
	}
      } else {
	flock(fileno(m_fp), LOCK_UN);
	fclose(m_fp);
	logger(logDEBUG) << "Interlock tmp-file does not exist or is not locked";
	ilOK = false;
      }
    }
    
    FILE *in = fopen(("/tmp/cooling_"+peltName).c_str(),"r");
    if(in!=0){
      double newTmod, newTchl;
      char line[100];
      if(fgets(line, 99, in)!=0){
	if(strlen(line)>1 && line[strlen(line)-1]=='\n') line[strlen(line)-1]='\0';
	std::string sline = line;
	logger(logDEBUG1) << "Read from /tmp/cooling_"+peltName+": " << sline;
	if(sline=="HELP"){
	  printHelp(peltName);
	} else if(sline=="OFF"){
	  quit=true;
	  logger(logINFO) << "Terminating (external command input).";
	} else if(sline=="RES"){
	  oldStatus = 0;
	  logger(logINFO) << "Reset SW status";
	} else if(sline=="START"){
	  if(!ilOK){
	    logger(logWARNING) << "Interlock is preventing this process from starting";
	  } else if(oldStatus==2) {
	    logger(logINFO) << "START command ignored, we're in error state";
	  } else {
	    executePID = true;
	    logger(logINFO) << "Starting Peltier control";
	    // makse sure Peltier PSU are actually on
	    EquipConf hw;
	    hw.setHardwareConfig(configFile);
	    std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(channelNames.at(0));
	    if(channelNames.size()==1){
	      PS->turnOn();
	    } else {
	      PS->getPowerSupply()->turnOnAll();
	    }
	    double curr = PS->measureCurrent();
	  }
	} else if(sline=="STOP"){
	  if(oldStatus==2) {
	    logger(logINFO) << "STOP command ignored, we're in error state";
	  } else {
	    executePID = false;
	    logger(logINFO) << "Stopping Peltier control";
	  }
	} else if(sline.length()>2 && sline.substr(0,2)=="+ "){
	  if(!ilOK){
	    logger(logWARNING) << "Interlock is preventing this process from setting T";
	  } else if(oldStatus==2) {
	    logger(logINFO) << "Set-T command ignored, we're in error state";
	  } else {
	    sline = sline.substr(2, sline.length()-2);
	    logger(logDEBUG1) <<"T_target add triggered: " << sline;
	    if(sline.find(" ")!=std::string::npos){
	      sscanf(sline.c_str(),"%lf %lf\n", &newTmod, &newTchl);
	      Ttarg += newTmod;
	      Tchl += newTchl;
	    } else {
	      sscanf(sline.c_str(), "%lf\n", &newTmod);
	      Ttarg += newTmod;
	      Tchl = setTchl(Ttarg);
	    }
	  }
	} else if(sline.length()>2 && sline.substr(0,2)=="- "){
	  if(!ilOK){
	    logger(logWARNING) << "Interlock is preventing this process from setting T";
	  } else if(oldStatus==2) {
	    logger(logINFO) << "T-set command ignored, we're in error state";
	  } else {
	    sline = sline.substr(2, sline.length()-2);
	    logger(logDEBUG1) <<"T_target subtract triggered: " << sline;
	    if(sline.find(" ")!=std::string::npos){
	      sscanf(sline.c_str(),"%lf %lf\n", &newTmod, &newTchl);
	      Ttarg -= newTmod;
	      Tchl -= newTchl;
	    } else {
	      sscanf(sline.c_str(), "%lf\n", &newTmod);
	      Ttarg -= newTmod;
	      Tchl = setTchl(Ttarg);
	    }
	  }
	} else {
	  if(!ilOK){
	    logger(logWARNING) << "Interlock is preventing this process from setting T";
	  } else if(oldStatus==2) {
	    logger(logINFO) << "T-set command ignored, we're in error state";
	  } else {
	    if(sline.find(" ")!=std::string::npos){
	      sscanf(line,"%lf %lf\n", &newTmod, &newTchl);
	      Ttarg = newTmod;
	      Tchl = newTchl;
	    } else {
	      sscanf(line, "%lf\n", &newTmod);
	      Ttarg = newTmod;
	      Tchl = setTchl(Ttarg);
	    }
	  }
	}
      }
      fclose(in);
      remove(("/tmp/cooling_"+peltName).c_str());
      if(!quit && executePID){
	atTarg = false;
	if(chiller!=nullptr){
	  logger(logINFO) << "New T_target: " << std::fixed << std::setprecision(1) << Ttarg << 
	    ", T_chiller = " << std::fixed << std::setprecision(1) << Tchl;
	  chiller->setTargetTemperature(Tchl);
	} else
	  logger(logINFO) << "New T_target: " << std::fixed << std::setprecision(1) << Ttarg;
      }
    }
    
    // read from InfluxDB - if already in bad state, set timeoutW very high to not be swamped with further warnings
    status = ReadFromDB(infsk, DBmeasNameE, ntcName, "temperature", (oldStatus==2)?1000:timeoutW, timeoutE, Tntc);
    if(status==2 && oldStatus<2) logger(logERROR) << "No NTC data from InfluxdDB for more than " << timeoutE << " s";

    // new precaution: abort if chiller is in a bad state
    if(chiller!=nullptr && !chiller->getStatus()){
      if(oldStatus<2) logger(logERROR) << "Chiller is in bad status.";
      status = 2;
    }

    // issue warning in case we're stuck in a bad state and stop operation of Peltier
    if(status==2){
      executePID=false;
      // turn off associated equipment
      EquipConf hw;
      hw.setHardwareConfig(configFile);
      std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(channelNames.at(0));
      if(channelNames.size()==1)
	PS->turnOff();
      else
	PS->getPowerSupply()->turnOffAll();
      PS->getCurrentLevel();
      sca->sendreceive("RELOFF");
      if(chiller!=nullptr) chiller->turnOff();
    }
    if(oldStatus<2){
      oldStatus = status;
      if(status==2){
	logger(logWARNING) << "Peltier control forced to stop (loop still active for monitoring) -> ";
	logger(logWARNING) << "Fix problem, then reset Peltier SW to continue";
      }
    }

    EquipConf hw;
    hw.setHardwareConfig(configFile);
    bool IisNeg = false;
    if(!quit && oldStatus<2){
      if(fabs(Tntc-Ttarg)<1.0){
	if(!atTarg){
	  FILE *lock = fopen(("/tmp/cooling_"+peltName+"_attrg").c_str(),"w");
	  fclose(lock);
	}
	atTarg = true;
      }
      if(pid.Compute() && executePID){
	if(chiller!=nullptr)
	  logger(logDEBUG) << "PID changed current to " << std::fixed << std::setprecision(2) << Iset << 
	    " at T_chl = " << std::setprecision(1) << chiller->measureTemperature() <<
	    ", T_NTC = " << std::setprecision(1) << Tntc;
	else
	  logger(logDEBUG) << "PID changed current to " << std::fixed << std::setprecision(2) << Iset << 
	    " at T_NTC = " << std::setprecision(1) << Tntc;
	double IsetDev = Iset / (double) channelNames.size();
	// polarity switching relay
	if(Iset<0.0) IisNeg = true;
	try{
	  sca->sendreceive(IisNeg?"RELON":"RELOFF");
	} catch(...){
	  IsetDev = 0.0; // make sure no current when we can't control polarity
	  logger(logWARNING) << "Connection to relay lost";
	}
	// set current on the actual power supply
	logger(logDEBUG1) << "Current per Peltrier PSU channel: " << std::fixed << std::setprecision(2) << IsetDev << " A";
	for(std::vector<std::string>::iterator it=channelNames.begin();
	    it!=channelNames.end(); it++) {
	  std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel((*it));
	  PS->setCurrentLevel(fabs(IsetDev));
	  double curr = PS->getCurrentLevel();
	  logger(logDEBUG1) << "Current on channel: " << curr;
	  curr = PS->measureCurrent();
	  logger(logDEBUG1) << "Meas. current on channel: " << curr;
	}
      }
      if(!executePID){
	double IsetDev = 0.;
	for(std::vector<std::string>::iterator it=channelNames.begin();
	    it!=channelNames.end(); it++) {
	  std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel((*it));
	  PS->setCurrentLevel(fabs(IsetDev));
	  double curr = PS->getCurrentLevel();
	  logger(logDEBUG1) << "Current on channel: " << curr;
	  curr = PS->measureCurrent();
	  logger(logDEBUG1) << "Meas. current on channel: " << curr;
	}
      }
    }

    // store voltage and current of Peltier in InfluxDB
    sink->setTag("Channel", channelNames.at(0));
    sink->startMeasurement(DBmeasNameP, std::chrono::system_clock::now());
    double volts = 0.;
    double curr = 0.;
    for(std::vector<std::string>::iterator it=channelNames.begin();
	it!=channelNames.end(); it++) {
      std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel((*it));
      volts += PS->measureVoltage();
      curr += PS->measureCurrent();
    }
    volts /= (double)channelNames.size();
    if(IisNeg){
      volts *= -1.;
      curr *= -1.;
    }
    sink->setField("Voltage", volts);
    sink->setField("Current", curr);
    sink->recordPoint();
    sink->endMeasurement();

    // also store chiller measured temperature
    if(chiller!=nullptr){
      sink->setTag("Sensor", "ChillerFluid");
      sink->startMeasurement(DBmeasNameE, std::chrono::system_clock::now());
      sink->setField("temperature", chiller->measureTemperature());
      sink->recordPoint();
      sink->endMeasurement();
    }

    // store status information
    sink->setTag("Interlock", peltName);
    sink->startMeasurement(DBmeasNameE, std::chrono::system_clock::now());
    if(oldStatus==2)
      sink->setField("status", 2);
    else
      sink->setField("status", executePID?status:(-1));
    sink->recordPoint();
    sink->endMeasurement();

    // sleep 0.5s
    if(!quit) std::this_thread::sleep_for(std::chrono::milliseconds(500));
  }

  // turn off associated equipment
  EquipConf hw;
  hw.setHardwareConfig(configFile);
  std::shared_ptr<PowerSupplyChannel> PS = hw.getPowerSupplyChannel(channelNames.at(0));
  if(channelNames.size()==1)
    PS->turnOff();
  else
    PS->getPowerSupply()->turnOffAll();
  PS->getCurrentLevel();
  sca->sendreceive("RELOFF");
  if(chiller!=nullptr) chiller->turnOff();

  while(!quit){
    sink->setTag("Interlock", peltName);
    sink->startMeasurement(DBmeasNameE, std::chrono::system_clock::now());
    sink->setField("status", status);
    sink->recordPoint();
    sink->endMeasurement();
    // sleep 1s
    if(!quit) std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  }
  
  //clean up
  remove(("/tmp/cooling_"+peltName+"_attrg").c_str());

  // store status information: off
  sink->setTag("Interlock", peltName);
  sink->startMeasurement(DBmeasNameE, std::chrono::system_clock::now());
  sink->setField("status", -1);
  sink->recordPoint();
  sink->endMeasurement();
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  sink->setTag("Interlock", peltName);
  sink->startMeasurement(DBmeasNameE, std::chrono::system_clock::now());
  sink->setField("status", -1);
  sink->recordPoint();
  sink->endMeasurement();

  return 0;
}
