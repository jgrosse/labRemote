#include <getopt.h>
#include <pwd.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/file.h>

#include <chrono>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>

#include "Logger.h"
#include "EquipConf.h"
#include "IPowerSupply.h"

#include <nlohmann/json.hpp>
using json = nlohmann::json;


//------ SETTINGS
std::string name("PS");
int channel = 1;
std::string channelName;
std::string configFile;
//---------------


void usage(char *argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] command [parameters]" << std::endl;
  std::cerr << "List of possible COMMAND:" << std::endl;
  std::cerr << "  set-current -- I [V]   Set current I [A] with maximum voltage V [V]" << std::endl;
  std::cerr << "  ramp-current -- I rate Ramp voltage I [A] at rate [A/s]" << std::endl;
  std::cerr << "  get-current            Get set current level [A]" << std::endl;
  std::cerr << "  meas-current           Get reading of current [A]" << std::endl;
  std::cerr << "  set-voltage-range -- V Set voltage range V [V]" << std::endl;
  std::cerr << "  set-voltage -- V [I]   Set voltage V [V] with maximum current I [A]" << std::endl;
  std::cerr << "  ramp-voltage -- V rate Ramp voltage V [V] at rate [V/s]" << std::endl;
  std::cerr << "  get-voltage            Get set voltage level [V]" << std::endl;
  std::cerr << "  meas-voltage           Get reading of voltage [V]" << std::endl;
  std::cerr << "  program [on]           Execute the program block and optionally turn on the power supply" << std::endl;
  std::cerr << "  power-on [V I]         Power ON PS, optionally setting voltage to V in Volts and current to I in Ampere" << std::endl;
  std::cerr << "  power-off              Power OFF PS" << std::endl;
  std::cerr << "  is-on                  Check power-on/off state" << std::endl;
  std::cerr << "  is-tripped             Check for power trip state" << std::endl;
  std::cerr << "  identify               Identify device" << std::endl;
  std::cerr << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -n, --name PS     Name of the power supply from equipment list (default: " << name << ")" << std::endl;  
  std::cerr << " -c, --channel Ch  Set PS channel (name or number when used in conjunction with --name) to control (default: " << channel << ")" << std::endl;
  std::cerr << " -e, --equip       config.json Configuration file with the power supply definition (default: " << configFile << ")" << std::endl;
  std::cerr << " -d, --debug       Enable more verbose printout, use multiple for increased debug level"  << std::endl;
  std::cerr << " -h, --help        List the commands and options available"  << std::endl;
  std::cerr << std::endl;
  std::cerr << std::endl;
}

int main(int argc, char*argv[])
{
  // get default hardware config file from ~/.labRemote
  std::string homedir;
  if (getenv("HOME")==NULL)
    homedir = getpwuid(getuid())->pw_dir;
  else
    homedir = getenv("HOME");

  if( access( (homedir+"/.labRemote/hardware.json").c_str(), F_OK ) != -1 )
    configFile=homedir+"/.labRemote/hardware.json";

  //
  //Parse command-line  
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"name",     required_argument, 0,  'n' },
      {"channel",  required_argument, 0,  'c' },
      {"equip",    required_argument, 0,  'e' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
    
    c = getopt_long(argc, argv, "n:c:p:e:dh", long_options, &option_index);
    if (c == -1)
      break;

    switch (c)
      {
      case 'n':
	name = optarg;
	break;
      case 'c':
	try
	  {
	    channel    = std::stoi(optarg);
	  }
	catch(const std::invalid_argument& e)
	  {
	    channelName= optarg;
	  }
	break;
      case 'e':
	configFile = optarg;
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  std::string command;
  std::vector<std::string> params;
  if (optind < argc)
    {
      command = argv[optind++];
      std::transform(command.begin(), command.end(), command.begin(), ::tolower);
      while (optind < argc)
	{
	  std::string p(argv[optind++]);
	  std::transform(p.begin(), p.end(), p.begin(), ::tolower);
	  params.push_back(p);
	}
    }
  else
    {
      std::cerr << "Required command argument missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  //check if config file is provided, if not create JSON config
  EquipConf hw;
  if (configFile.empty())
    {
      logger(logERROR) << "No input config file, either create default or use --equip.";
      return 1;
    }
  else
    {
      hw.setHardwareConfig(configFile);
    }

  logger(logDEBUG) << "Configuring power-supply.";
  std::shared_ptr<PowerSupplyChannel> PS;
  if(channelName.empty())
    {
      std::shared_ptr<IPowerSupply> PSreal = hw.getPowerSupply(name);
      PS = std::make_shared<PowerSupplyChannel>(name+std::to_string(channel), PSreal, channel);
    }
  else
    {
      PS = hw.getPowerSupplyChannel(channelName);
    }

  logger(logDEBUG) << "Settings:";
  logger(logDEBUG) << " PS channel: " << channel;
  
  //Now interpret command
  logger(logDEBUG) << "Sending command to power-supply.";

  if (command == "set-current")
    {
      if (params.size() != 1 && params.size() != 2)
	{
	  logger(logERROR) << "Invalid number of parameters to set-current command.";
	  logger(logERROR) << "";
	  usage(argv);
	  return 1;
	}
      logger(logDEBUG) << "Set current to "<< params[0];
      PS->setCurrentLevel(std::stod(params[0]));
      if(params.size()>=2)
	{
	  logger(logDEBUG) << "Set maximum voltage to "<< params[1];
	  PS->setVoltageProtect(std::stod(params[1]));
	}
    }  
  else if (command == "ramp-current")
    {
      if (params.size() != 2)
	{
	  logger(logERROR) << "Invalid number of parameters to ramp-current command.";
	  logger(logERROR) << "";
	  usage(argv);
	  return 1;
	}
      logger(logDEBUG) << "Ramp current to "<< params[0] << " A at rate " << params[1] << " A/s";
      PS->rampCurrentLevel(std::stod(params[0]), std::stod(params[1]));
    }
  else if (command == "get-current")
    {
      if (logIt::loglevel >= logDEBUG) std::cout << "Current: ";
      std::cout << PS->getCurrentLevel();
      if (logIt::loglevel >= logDEBUG) std::cout << " A";
      std::cout << std::endl;
    }
  else if (command == "meas-current")
    {
      if (logIt::loglevel >= logDEBUG) std::cout << "Current: ";
      std::cout << PS->measureCurrent();
      if (logIt::loglevel >= logDEBUG) std::cout << " A";
      std::cout << std::endl;
    }
  else if (command == "set-voltage-range")
    {
      if (params.size() != 1 && params.size() != 2)
	{
	  logger(logERROR) << "Invalid number of parameters to set-voltage command.";
	  logger(logERROR) << "";
	  usage(argv);
	  return 1;
	}
      logger(logDEBUG) << "Set voltage range to "<< params[0];
      PS->setVoltageRange(std::stod(params[0]));
    }
  else if (command == "set-voltage")
    {
      if (params.size() != 1 && params.size() != 2)
	{
	  logger(logERROR) << "Invalid number of parameters to set-voltage command.";
	  logger(logERROR) << "";
	  usage(argv);
	  return 1;
	}
      logger(logDEBUG) << "Set voltage to "<< params[0];
      PS->setVoltageLevel(std::stod(params[0]));
      if(params.size()>=2)
	{
	  logger(logDEBUG) << "Set maximum current to "<< params[1];
	  PS->setCurrentProtect(std::stod(params[1]));
	}
    }
  else if (command == "ramp-voltage")
    {
      if (params.size() != 2)
	{
	  logger(logERROR) << "Invalid number of parameters to ramp-voltage command.";
	  logger(logERROR) << "";
	  usage(argv);
	  return 1;
	}
      logger(logDEBUG) << "Ramp voltage to "<< params[0] << " V at rate " << params[1] << " V/s";
      PS->rampVoltageLevel(std::stod(params[0]), std::stod(params[1]));
    }
  else if (command == "get-voltage")
    {
      if (logIt::loglevel >= logDEBUG) std::cout << "Voltage: ";    
      std::cout << PS->getVoltageLevel();
      if (logIt::loglevel >= logDEBUG) std::cout << " V";
      std::cout << std::endl;
    }
  else if (command == "meas-voltage")
    {
      if (logIt::loglevel >= logDEBUG) std::cout << "Voltage: ";    
      std::cout << PS->measureVoltage();
      if (logIt::loglevel >= logDEBUG) std::cout << " V";
      std::cout << std::endl;
    }
  else if (command == "program")
    {
      logger(logDEBUG) << "Programming power-supply.";
      PS->program();
      if (params.size() > 0)
	{ // set both voltage and current
	  if (params.size() != 1 || params[0]!="on")
	    {
	      logger(logERROR) << "Invalid number of parameters to program command.";
	      logger(logERROR) << "";
	      usage(argv);
	      return 1;
	    }
	  logger(logDEBUG) << "Powering ON.";
	  PS->turnOn();
	}
    }  
  else if (command == "power-on")
    {
      bool ilOK = true; // allow power-on if no interlock specified
      if(PS->getInterlockName()!="none"){
	// check if interlock is active -> safe to power on
	std::string fname = "/tmp/interlock_"+PS->getInterlockName();
	FILE *m_fp = fopen(fname.c_str(), "a");
	if(m_fp!=0 && flock(fileno(m_fp), LOCK_EX | LOCK_NB)!=0){ 
	  if(errno==EWOULDBLOCK){
	    ilOK = true;
	  }
	  fclose(m_fp);
	} else {
	  logger(logWARNING) << "Interlock is preventing this channel from being powered on.";
	  ilOK = false;
	}
	flock(fileno(m_fp), LOCK_UN);
      }
      if(ilOK){
	logger(logDEBUG) << "Initializing power-supply.";
	if (params.size() > 0)
	  { // set both voltage and current
	    if (params.size() != 2)
	      {
		logger(logERROR) << "Invalid number of parameters to power-on command.";
		logger(logERROR) << "";
		usage(argv);
		return 1;
	      }
	    PS->setVoltageLevel  (std::stod(params[0]));
	    PS->setCurrentProtect(std::stod(params[1]));
	  }
	logger(logDEBUG) << "Powering ON.";
	PS->turnOn();
      } else
	return 1;
      
    }
  else if (command == "power-off")
    {
      logger(logDEBUG) << "Powering OFF.";
      PS->turnOff();
    }  
  else if (command == "is-on")
    {
      logger(logDEBUG) << "checking state";
      std::cout << (PS->isOn()?"ON":"OFF") << std::endl;
    }  
  else if (command == "is-tripped")
    {
      logger(logDEBUG) << "checking state";
      std::cout << (PS->isTripped()?"TRP":"OK") << std::endl;
    }  
  else if (command == "identify")
    {
      logger(logDEBUG) << "Identifying device.";
      std::cout << PS->getPowerSupply()->identify();
      std::cout << std::endl;
    }  
  else
    {
      usage(argv);
    }

  logger(logDEBUG) << "All done.";
  return 0;
}
