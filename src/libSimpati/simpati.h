#ifndef SIMPATI_H
#define SIMPATI_H

#include <map>
#include <vector>
#include <string>
#include <thread>

class simpati {

 public:
  simpati(std::string ipaddr = "192.168.1.103", int port = 7777, int chid=2);
  ~simpati();

  void setT(double tset, int id=1);
  void setCage(bool top=true, bool wait=true);
  void setEnabled(std::vector<int> ids, bool setOn=true);
  void startProgram(int pid);
  void startProgram(std::string pname);
  void stopProgram();
  bool getProgramStatus();
  
  std::map<int, std::string> getNames(){return m_measNames;};
  std::map<int, std::string> getUnits(){return m_measUnits;};
  std::map<int, std::pair<double, double> > getVals(){return m_measVals;};
  void stopMon(){m_runReadings=false;};
  void updateReadings();
  void updateReadingsLoop();
  int getChamberStatus();

 private:
  std::string getSimAnswer(std::string cmd, std::vector<std::string> args);

  std::map<int, std::string> m_measNames;
  std::map<int, std::string> m_measUnits;
  std::map<int, std::pair<double, double> > m_measVals;
  std::string m_ipaddr;
  int m_port;
  int m_chid;
  bool m_runReadings;
  std::thread m_roThread;
};

#endif // SIMPATI_H
