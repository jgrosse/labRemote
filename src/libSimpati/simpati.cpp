#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <simpati.h>

#include <Logger.h>

simpati::simpati(std::string ipaddr, int port, int chid) : m_ipaddr(ipaddr), m_port(port), m_chid(chid){
  std::vector<std::string> args;
  std::string answer = getSimAnswer("11018", args);

  uint nmeas = (uint)atoi(answer.c_str());
  for(uint i=1;i<nmeas+1;i++){
    std::stringstream a;
    a << i;
    args.clear();
    args.push_back(a.str());
    answer = getSimAnswer("11026", args);
    if(answer.length()>2 && answer.substr(0,2)!="  "){
      m_measNames[i] = answer;
      m_measVals[i] = std::make_pair(-9999., -9999.);
      answer = getSimAnswer("11023", args);
      m_measUnits[i] = answer;
    }
  }
  m_runReadings = true;
  m_roThread = std::thread(&simpati::updateReadingsLoop, this);
}
simpati::~simpati(){
  m_runReadings = false;
  if(m_roThread.joinable()) m_roThread.join();
}
void simpati::setT(double tset, int id){
  std::vector<std::string> args;
  std::stringstream a, b;
  a << tset;
  b << id;
  args.push_back(b.str());
  args.push_back(a.str());
  getSimAnswer("11001", args);
}
void simpati::setCage(bool top, bool wait){
  std::vector<std::string> args;

  args.push_back("4");
  args.push_back(top?"1":"2");
  getSimAnswer("11001", args);
  if(wait){
    double cagepos = top?2:1;
    while(cagepos!=(top?1:2)) {
      std::pair<double, double> vals = getVals()[1];
      cagepos = vals.second;
      std::this_thread::sleep_for (std::chrono::milliseconds(1000));
    }
  }
}
void simpati::setEnabled(std::vector<int> ids, bool setOn){
  std::vector<std::string> args;
  // manual operation
  args.push_back("0x20");
  getSimAnswer("19050", args);
  // set main and used dig. channel
  for(std::vector<int>::iterator it=ids.begin(); it!=ids.end(); it++){
    std::vector<std::string> argsc;
    std::stringstream a;
    a<<(*it);
    argsc.push_back(a.str());
    argsc.push_back(setOn?"1":"0");
    getSimAnswer("14001", argsc);
  }
}
void simpati::startProgram(int pid){
  std::vector<std::string> args;
  // start program no. pid
  std::stringstream a;

  a << pid;
  args.push_back(a.str());
  args.push_back("0");
  std::string answer = getSimAnswer("19014", args);
  // wait until started
  answer="0";
  uint nwc=0;
  const uint nwcmax=50;
  args.clear();
  while(answer=="0" && nwc<nwcmax){
    answer = getSimAnswer("19062", args);
    nwc++;
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));
  }
  if(nwc<nwcmax)
    logger(logDEBUG) << "Program " << pid << " started";
  else{
    logger(logERROR) << "Error starting program on chamber!";
  }
}
void simpati::startProgram(std::string pname){
  std::vector<std::string> args;
  // start burn in program pname
  std::stringstream a;
  a << pname;
  args.push_back(a.str());
  args.push_back("0");
  std::string answer = getSimAnswer("19014", args);
  // wait until started
  answer="0";
  uint nwc=0;
  const uint nwcmax=50;
  args.clear();
  while(answer=="0" && nwc<nwcmax){
    answer = getSimAnswer("19062", args);
    nwc++;
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));
  }
  if(nwc<nwcmax)
    logger(logDEBUG) << "Program " << pname << " started";
  else{
    logger(logERROR) << "Error starting program on chamber!";
  }
}
void simpati::stopProgram(){
  std::vector<std::string> args;
  getSimAnswer("19015", args);
  std::string answer = "1";
  uint nwc=0;
  const uint nwcmax=50;
  while(answer=="1" && nwc<nwcmax){
    answer = getSimAnswer("19062", args);
    nwc++;
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));
  }
  if(nwc<nwcmax)
    logger(logDEBUG) << "Program stopped";
  else{
    logger(logERROR) << "Error stopping program on chamber!";
  }
}

bool simpati::getProgramStatus(){
  std::vector<std::string> args;
  std::string answer = getSimAnswer("19062", args);
  return (answer=="1");
}

int simpati::getChamberStatus(){
  std::vector<std::string> args;
  std::string answer = getSimAnswer("10012", args);
  logger(logDEBUG) << "Status string: " << answer;
  int ianswer = 0;
  try{
    ianswer = std::stoi(answer);
  } catch(...) {
    ianswer = 0;
  }
  return ianswer;
}
std::string simpati::getSimAnswer(std::string cmd, std::vector<std::string> args){
  boost::asio::io_service ios;
  boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(m_ipaddr), m_port);
  boost::asio::ip::tcp::socket socket(ios);
  size_t len;
  const size_t len_max=128;
  unsigned char bufc[len_max], bufr[len_max];
  char cchid[2];
  try{
    sprintf(cchid,"%d",m_chid);
    for(uint i=0;i<5;i++)
      bufc[i] = cmd.c_str()[i];
    bufc[5]=182;
    bufc[6]=cchid[0];
    len = 7;
    for(uint i=0;i<args.size();i++){
      bufc[len]=182;
      len++;
      for(uint k=0;k<args[i].length() && len<(len_max-1);k++){
	bufc[len]=args[i].c_str()[k];
	len++;
      }
    }
    bufc[len]='\r';
    len++;
    //std::cout << "Sending command " << std::string((char*)bufc) << std::endl;
    socket.connect(endpoint);
    size_t len2 = socket.send(boost::asio::buffer(bufc, len));
    if(len2==len){
      std::this_thread::sleep_for (std::chrono::milliseconds(50));
      if(socket.available()!=0){
	len = socket.receive(boost::asio::buffer(bufr));
	socket.close();
	//std::cout << "Have data of length " << len << "!" << std::endl;
	//std::cout << "   --> " << std::string((char*)bufr) << std::endl;
	if(bufr[0]=='1' && bufr[1]==182){
	  char answer[len_max];
	  uint i=0;
	  for(i=2;i<len && i<len_max;i++){
	    if(bufr[i]=='\r'){
	      answer[i-2]='\0';
	      break;
	    } else
	      answer[i-2]=bufr[i];
	  }
	  answer[i-1] = '\0';
	  //std::cout << std::string(answer) << std::endl;
	  return std::string(answer);
	}
      } else
	socket.close();
    } else
      logger(logERROR) << "Sent "<<len2<<" bits, but should have been " << len;
  } catch(...){
    socket.close();
    logger(logERROR) << "Caught unhandled exception in simpati::getSimAnswer()";
  }
  return "";
}
void simpati::updateReadingsLoop(){
  while(m_runReadings){
    updateReadings();
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));
  }
}
void simpati::updateReadings(){
  std::map<int, std::pair<double, double> > measVals;
  int doUpdate=0;
  for(std::map<int, std::string>::iterator it = m_measNames.begin();
      it!=m_measNames.end();it++){
    std::stringstream a;
    std::vector<std::string> args;
    std::string answer;
    a << it->first;
    args.push_back(a.str());
    try {
      answer = getSimAnswer("11002", args);
      double soll = (double)atof(answer.c_str());
      answer = getSimAnswer("11004", args);
      double ist = (double)atof(answer.c_str());
      measVals[it->first] = std::make_pair(soll, ist);
      if(soll!=0.0 || ist!=0.0) doUpdate++;
    } catch(...){}
  }
  // if too many readings are 0: internal reading error -> ignore
  if(doUpdate>(m_measVals.size()-2)) m_measVals = measVals;
}
