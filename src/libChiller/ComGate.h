#ifndef UNISTAT_H
#define UNISTAT_H

#include <string>
#include <stdexcept>
#include <memory>

#include "Logger.h"
#include "TextSerialCom.h"
#include "IChiller.h"

//! \brief Object to communicate with a Huber Unistat series chiller with ComG@te interface
/**
 * The TextSerialCom passed to this object should already be 
 * initialized, be set to the same baud rate as the chiller, 
 * and have the termination set to "\r".
 *
 * # Example
 *
 * ```
 * std::shared_ptr<TextSerialCom> com = 
 *   std::make_shared<TextSerialCom>("/dev/ttyUSB0",B9600);
 * com->setTermination("\r\n");
 * com->init();
 *
 * ComGate chiller(com);
 *
 * chiller.init();
 * chiller.setSetTemperature(15.0);
 * ```
 *
 * The operator's manual for these chillers can be found at 
 * https://www.huber-online.com/download/manuals/archive/unistats_EN.pdf
 * (pp 83-86 describes the communication)
 */
class ComGate : public IChiller
{
public:
  /**
   * \param com The serial communication interface connected
   *   to the chiller
   */ 
  ComGate();
  ~ComGate();

  //! Initialize the serial communication channel
  void init();
  //! Turn the chiller on
  void turnOn();
  //! Turn the chiller off
  void turnOff();
  //! Set the target temperature of the chiller
  /**
   * \param temp The target temperature in Celsius
   */
  void setTargetTemperature(float temp);
  //! Return the temperature that the chiller is set to in Celsius
  float getTargetTemperature();
  //! Return the current temperature of the chiller in Celsius
  float measureTemperature();
  //! Get the status of the chiller
  /**
   * \return true if chiller is in "run" mode, and false if 
   *   it's in "standby" mode
   */
  bool getStatus();

private:

};

#endif
