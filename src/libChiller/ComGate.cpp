#include "ComGate.h"
#include <chrono>
#include <thread>

ComGate::ComGate() : IChiller()
{
  // nothing to be done
}

ComGate::~ComGate()
{
  // nothing to be done
}

void ComGate::init()
{
  // nothing to be done
}
void ComGate::turnOn()
{
  m_com->send("START");
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
}
void ComGate::turnOff()
{
  m_com->send("STOP");
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
}
void ComGate::setTargetTemperature(float temp)
{
  char cmd[50];
  sprintf(cmd, "OUT_SP_00 %.2f", temp);
  m_com->send(cmd);
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
}
float ComGate::getTargetTemperature()
{
  std::string response = m_com->sendreceive("IN_SP_00");
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  return std::stof(response);
}
float ComGate::measureTemperature()
{
  std::string response = m_com->sendreceive("IN_PV_00");
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  return std::stof(response);
}
bool ComGate::getStatus()
{
  std::string response = m_com->sendreceive("STATUS");
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  if(response=="-1") return false; // only error state the device has
  return true;
}
