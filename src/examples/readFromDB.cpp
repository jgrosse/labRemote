#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <signal.h>

#include <nlohmann/json.hpp>

#include <DataSinkConf.h>
#include <IDataSink.h>
#include <InfluxDBSink.h>
#include <Logger.h>

bool quit=false;
void cleanup(int signum)
{ quit=true; }

void usage(char *argv[])
{
  std::cout << "Usage: "<< argv[0] << " config-file DB-measurement-name sensor-name [-d]"<< std::endl;
}

int main(int argc, char** argv){

  if(argc<4){
    usage(argv);
    return -2;
  }

  std::string configFile = argv[1];
  std::string DBmeasName=argv[2];
  std::string sensName = argv[3];
  std::string sinkName = "db";

  // enable debug print-out
  if(argc==5 && strcmp(argv[4],"-d")==0)
    logIt::incrDebug();

  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  std::shared_ptr<InfluxDBSink> infsk = std::dynamic_pointer_cast<InfluxDBSink>(sink);
  if(infsk==nullptr){
    logger(logERROR) << "Couldn't find InfluxDB in config. ";
    return -3;
  }

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  while(!quit){

    std::string resp;
    infsk->query(resp, "select * from "+ DBmeasName+" where Sensor = '"+sensName+"' and time > now() - 45s ");
    std::cout<<resp<<std::endl;
    
    auto infj = nlohmann::json::parse(resp);
    int time_id = -1, temp_id = -1, dp_id=-1;
    double T = -273.15, DP = -273.15;
    long unsigned int last_time = 0, old_time = 0, new_time = 0;
    uint i=0;
    for(const auto& cols : infj["results"][0]["series"][0]["columns"].items()){
      if(cols.value() == "time") time_id = i;
      if(cols.value() == "temperature") temp_id = i;
      if(cols.value() == "dewpoint") dp_id = i;
      i++;
    }
    if(time_id>=0){
      for(const auto& vals : infj["results"][0]["series"][0]["values"].items()){
	nlohmann::json jv = vals.value();
	new_time = jv[time_id];
	if(new_time>old_time){
	  if(temp_id>=0){
	    try{
	      T = jv[temp_id]; 
	    } catch(...){
	      T = -273.15;
	    }
	  }
	  if(dp_id>=0){
	    try{
	      DP = jv[dp_id]; 
	    } catch(...){
	      DP = -273.15;
	    }
	  }
	  old_time = new_time;
	}
      }
      std::time_t ts = old_time/1e9; // InfluxDB timestamp is in ns
      std::cout << "T = " << T << ", DP = " << DP << " at " << std::asctime(std::localtime(&ts))
		<< " dT = " << (std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts) << std::endl;
      if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts)>15)
	logger(logERROR) << "Data not updated for more than 15s!";
      last_time = old_time;
    }

    // sleep 2s
    if(!quit) std::this_thread::sleep_for(std::chrono::milliseconds(2000));
  }

  return 0;
}
