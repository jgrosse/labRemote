#include <getopt.h>
#include <string.h>
#include <signal.h>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <thread>
#include <chrono>
#include <sys/stat.h> 
#include <map>
#include <nlohmann/json.hpp>

#include <TextSerialCom.h>
#include <ADCDevComuino.h>
#include <NTCSensor.h>
#include <PtSensor.h>
#include <HIH4000.h>
#include <DataSinkConf.h>
#include <IDataSink.h>

//------ SETTINGS
uint32_t tsleep = 1000;
std::string configFile="temp_hum_monitor.json";
std::string streamName="Climate";
std::string label="Arduino1";
//---------------


bool quit=false;
void cleanup(int signum)
{ quit=true; }


void usage(char *argv[])
{
  std::cout << "Usage: "<< argv[0] << " [-c config file] [-s stream name] [-n dev. name] [-t sleep time (ms)]"<< std::endl;
  std::cout << "   defaults: -c " << configFile << " -s " << streamName<< " -n " << label << " -t " << tsleep << std::endl;
}
int main(int argc, char** argv)
{

  //Parse command-line  
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"name",     required_argument, 0,  'n' },
      {"config",   required_argument, 0,  'c' },
      {"stream",   required_argument, 0,  's' },
      {"tsleep",   required_argument, 0,  't' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
    
    c = getopt_long(argc, argv, "s:c:n:t:dh", long_options, &option_index);
    if (c == -1)
      break;

    switch (c)
      {
      case 's':
	streamName = optarg;
	break;
      case 'c':
	configFile = optarg;
	break;
      case 'n':
	label = optarg;
	break;
      case 't':
	tsleep = atoi(optarg);
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  // read json config.
  nlohmann::json hardwareConfig;
  std::ifstream i(configFile);
  i >> hardwareConfig;

  nlohmann::json arduConfig;
  for (const auto& hw : hardwareConfig["Com"]) {
    //check label 
    if (hw["name"] == label) {
      arduConfig = hw;
      break;
    }
  }
  // consistency check
  if(arduConfig.empty() || arduConfig["communication"].empty() || arduConfig["sensors"].empty()){
    std::cerr << "ERROR reading json content" << std::endl;
    return -2;
  }

  // open serial device
  std::shared_ptr<TextSerialCom> sc(new TextSerialCom);
  sc->setConfiguration(arduConfig["communication"]);
  sc->init();

  // somehow needed to initiate communication to Arduino
  try{
    sc->send("???");
    sc->receive();
  } catch(...){
  }
  // now the real test
  try{
    sc->send("???");
    sc->receive();
  } catch(std::runtime_error &err){
    std::cerr << "testing Arduino communication: runtime exception: " << err.what() << std::endl;
    return -3;
  } catch(...){
    std::cerr << "testing Arduino communication: unknown exception" << std::endl;
    return -4;
  }

  // create Arduino as ADC device
  double reference = 1.;
  if(!arduConfig["reference"].empty())
    reference = arduConfig["reference"];
  std::shared_ptr<ADCDevice> dev(new ADCDevComuino(reference,sc));

  // create NTC
  std::shared_ptr<NTCSensor> ntc(new NTCSensor("NTC"));
  ntc->setADC(dev);
  // create Pt1000
  std::shared_ptr<PtSensor> pt(new PtSensor("Pt1000"));
  pt->setADC(dev);
  // create HIH4000
  std::shared_ptr<HIH4000> hum(new HIH4000("HIH4000"));
  // need a way to do this from json:
  hum->setTempSens(std::static_pointer_cast<ClimateSensor>(pt));
  hum->setADC(dev);

  for (const auto& senscfg : arduConfig["sensors"].items()) {
    //std::cout << "Found " << senscfg.key() << std::endl;
    nlohmann::json spConfig = senscfg.value();
    ClimateSensor *clsens=nullptr;
    if(spConfig["type"]=="PtSensor")
      clsens = pt.get();
    else if(spConfig["type"]=="NTCSensor")
      clsens = ntc.get();
    else if(spConfig["type"]=="HIH4000")
      clsens = hum.get();
    if(clsens!=nullptr){
      clsens->setConfiguration(spConfig);
      clsens->init();
    }
  }

  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> stream = ds.getDataStream(streamName);

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  // request data from selected measurement
  while(!quit)
    {
      // Ambient T and H
      hum->read();
      stream->setTag("Sensor", "Ambient");
      stream->startMeasurement("environment", std::chrono::system_clock::now());
      stream->setField("temperature", hum->temperature()); // actual reading from Pt1000
      stream->setField("humidity", hum->humidity());
      stream->recordPoint();
      stream->endMeasurement();
      // module T
      ntc->read();
      stream->setTag("Sensor", "Module");
      stream->startMeasurement("environment", std::chrono::system_clock::now());
      stream->setField("temperature", ntc->temperature());
      stream->setField("dewpoint", hum->dewPoint());
      stream->recordPoint();
      stream->endMeasurement();
      std::this_thread::sleep_for(std::chrono::milliseconds(tsleep));
    }

  return 0;
}
