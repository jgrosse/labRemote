#include <unistd.h>
#include <boost/asio.hpp>
#include <boost/array.hpp>

#include <iostream>
#include <sstream>
#include <exception>
#include <string>
#include <string.h>
#include <thread>
#include <chrono>

#include "simpati.h"

int main(int /*argc*/, char **/*argv*/)
{
  std::vector<int> digch;
  digch.push_back(1);
  std::cerr << " Start == : Hello Baida "<<std::endl;
  simpati *mychamber = NULL;
    try{
    mychamber = new simpati();
    }catch(...){
    std::cerr << "Unknown exception while executing" << std::endl;
    return -1;
    }
  std::map<int, std::string> names = mychamber->getNames();
  //mychamber->setEnabled(true);
  for(int i=0;i<5;i++){
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));
    std::cout << "Name     \tSet\tMeas."<<std::endl;
    for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
      std::pair<double, double> vals = mychamber->getVals()[it->first];
      std::cout << it->second<<"\t"<<vals.first<<"\t"<<vals.second<<std::endl;
    }
    std::cout << std::endl;
  }
  std::cout << "changing setting" << std::endl;
  //mychamber->setCage(false, true);
  //mychamber->setTwarm(22.);
  mychamber->setT(10.);
  //mychamber->setEnabled(true);

  std::this_thread::sleep_for (std::chrono::milliseconds(1000));
  std::cout << "Name     \tSet\tMeas."<<std::endl;
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    std::pair<double, double> vals = mychamber->getVals()[it->first];
    std::cout << it->second<<"\t"<<vals.first<<"\t"<<vals.second<<std::endl;
  }
  std::cout << std::endl;

  std::cout << "changing setting back" << std::endl;
  //mychamber->setCage(true, true);
  //mychamber->setTwarm(22.);
  //mychamber->setTcold(-10.);
  mychamber->setEnabled(digch, true);
  std::this_thread::sleep_for (std::chrono::milliseconds(1000));
  std::cout << "Name     \tSet\tMeas."<<std::endl;
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    std::pair<double, double> vals = mychamber->getVals()[it->first];
    std::cout << it->second<<"\t"<<vals.first<<"\t"<<vals.second<<std::endl;
  }
  std::cout << std::endl;

  delete mychamber;

  return 0;
}
