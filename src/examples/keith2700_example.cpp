#include <iostream>
#include <fstream>
#include <unistd.h>
#include <nlohmann/json.hpp>
#include <pwd.h>
#include <getopt.h>
#include "Logger.h"
#include "Keithley2700.h"

using json = nlohmann::json;

/* 
 * possible COMMAND given in config:
 * meas-currentDC           Get reading of currentDC [A]
 * meas-voltageDC           Get reading of voltageDC [V]
 * meas-frequency         Get reading of Frequency [Hz]
 * meas-resistance        Get reading of Resistance [Ohm]
 * meas-capacitance       Get reading of Capacitance [F]
 */
 
void usage(char *argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cerr << "List of options:" << std::endl;
  //std::cerr << " -n, --name Multimeter     Name of the multimeter from equipment list (default: " << name << ")" << std::endl;  
  std::cerr << " -c, --channel Ch  Set Multimeter channel to control" << std::endl;
  std::cerr << " -e, --equip       config.json Configuration file with the power supply definition" << std::endl;
  std::cerr << " -d, --debug       Enable more verbose printout, use multiple for increased debug level"  << std::endl;
  std::cerr << " -h, --help        List the commands and options available"  << std::endl;
  std::cerr << std::endl;
  std::cerr << std::endl;
}



int main(int argc, char*argv[]) {
  //
  //Parse command-line  
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  std::string configFile = "";
  std::string channelName = "";
  std::string deviceName = "Keithley2700";
  std::string name = "";
  std::string protocol = "";
  uint16_t gpib_addr = 0;
  uint16_t gpib_board = 0;
  uint16_t read_tmo = 0;
  std::string command = "";
  std::string chan = "0";

  // get default hardware config file from ~/.labRemote
  std::string homedir;
  if (getenv("HOME")==NULL)
    homedir = getpwuid(getuid())->pw_dir;
  else
    homedir = getenv("HOME");

  if( access( (homedir+"/.labRemote/hardware.json").c_str(), F_OK ) != -1 )
    configFile=homedir+"/.labRemote/hardware.json";

  // arguments
  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"channel",  required_argument, 0,  'c' },
      {"equip",    required_argument, 0,  'e' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
    
    c = getopt_long(argc, argv, "c:p:e:d:h", long_options, &option_index);
    if (c == -1)
      break;
    switch (c) {
      case 'c':
	channelName= optarg;
	break;
      case 'e':
	configFile = optarg;
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  // read json config.
  json hardwareConfig;
  std::ifstream i(configFile);
  i >> hardwareConfig;
 
  // find the one with defined deviceName
  for (const auto& it : hardwareConfig["devices"]) {
    for (const auto& cfg : it.items()) {
      if(cfg.key() == "hw-model" && cfg.value() == deviceName){
        name = it["name"];
      }
    }
  }
  // read communication config
  for (const auto& it : hardwareConfig["devices"]) {
    if (it["name"] == name){
       for (const auto& cfg : it.items()) {
          if (cfg.key() == "communication"){
            for (const auto& commu : cfg.value().items()) {
              if (commu.key() == "protocol") protocol = commu.value();
              else if (commu.key() == "gpib_addr") gpib_addr = commu.value();
              else if (commu.key() == "gpib_board") gpib_board = commu.value();
              else if (commu.key() == "read_tmo") read_tmo = commu.value();
            }
          }
        }
      }
    }  

  if (protocol != "GPIBNICom"){
      logger(logERROR) << "This code only supports GPIBNICom protocol." << protocol;
      return -1;
  }

  json gpibConfig = {
      {"gpib_addr", gpib_addr},
      {"gpib_saddr", 0},
      {"gpib_board", gpib_board},
      {"read_tmo", read_tmo}
  };

  // read command in config 
  for (const auto& it : hardwareConfig["channels"]) {
    if (it["name"] == channelName) {
       for (const auto& cfg : it.items()) {
         if (cfg.key() == "device" && cfg.value() != deviceName){
           logger(logERROR) << "device not supported:" << cfg.value();
           return -1;
         }
         if (cfg.key() == "command"){
           command = cfg.value();
         }
         if (cfg.key() == "channel"){
	   chan = std::to_string((int)cfg.value());
         }
       }
     }
  }

  Keithley2700 meter(gpibConfig);
  meter.init();
  KeithleyMode mode;

  if (command == "meas-voltageDC") {
    mode = KeithleyMode::VOLTAGE;
  }
  else if (command =="meas-currentDC") { 
    mode = KeithleyMode::CURRENT;
  }
  else if (command =="meas-resistance") {
    mode = KeithleyMode::RESISTANCE;
  }
  else if (command =="meas-frequency") {
    mode = KeithleyMode::FREQUENCY;
  }
  if(chan=="0"){
    std::cout << std::stof(meter.readChannel(mode)) << std::endl;
  } else {
    std::cout << std::stof(meter.readChannel(chan, mode)) << std::endl;
  }

  return 0;
}

