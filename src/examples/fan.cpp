#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <thread>
#include <signal.h>
#include <getopt.h>

#include <nlohmann/json.hpp>

#include <EquipConf.h>
#include <IPowerSupply.h>
#include <DataSinkConf.h>
#include <IDataSink.h>
#include <InfluxDBSink.h>
#include <Logger.h>

bool quit=false;
void cleanup(int signum)
{ quit=true; }

void usage(char *argv[])
{
  std::cout << "" << std::endl;
  std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cout << "List of options:" << std::endl; 
  std::cout << "-c, --config config-file" << std::endl;
  std::cout << "-m, DBmeasName" << std::endl;
  std::cout << "-n, fanName" << std::endl;
 }

int main(int argc, char** argv){

  double upperlim = 40.0;

  std::string configFile = "";
  std::string lvName = "";
  std::string hvName = "";
  std::string sinkName = "db";
  std::string ntcName = "";
  std::string channelName = "";
  std::string fanName = "";
  std::string DBmeasName = "";

  int c;
  while(true){
    int option_index = 0;
    static struct option long_options[] = {
      {"config",   required_argument, 0,  'c' },
      {"DBmeas",   required_argument, 0,  'm' },
      {"name",     required_argument, 0,  'n' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "c:m:n:h", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
      case 'c':
	configFile = optarg;
	break;
      case 'm':
	DBmeasName = optarg;
	break;
      case 'n':
	fanName = optarg;
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	usage(argv);
	return 1;
      }
  }

  if(configFile == ""){
    logger(logERROR) << "no config file name provided -> exit";
    usage(argv);
    return -2;
  }

  // read json config.
  nlohmann::json hardwareConfig;
  std::ifstream i(configFile);
  i >> hardwareConfig;
  for (const auto& coolit : hardwareConfig["cooling"]) {
    //check label 
    if (coolit["name"] == fanName){
      for (const auto& coolcfg : coolit.items()) {
	//std::cout << "Key " << coolcfg.key() << std::endl;
	if(coolcfg.key()=="sink") {
	  sinkName = coolcfg.value();
	}
	if(coolcfg.key()=="NTC") {
	  ntcName = coolcfg.value();
	}
	if(coolcfg.key()=="LVchan") {
	  lvName = coolcfg.value();
	}
	if(coolcfg.key()=="HVchan") {
	  hvName = coolcfg.value();
	}
	if(coolcfg.key()=="fanchan") {
	  channelName = coolcfg.value();
	}
	if(coolcfg.key()=="upperlim") {
	  upperlim = coolcfg.value();
	}
      }
    }
  }

  if(channelName == ""){
    logger(logERROR) << "no fan PSU channel name provided -> exit";
    return -2;
  }
  if(ntcName == ""){
    logger(logERROR) << "no NTC sensor name provided -> exit";
    return -2;
  }
  if(lvName == ""){
    logger(logERROR) << "no LV PS name provided -> exit";
    return -2;
  }
  if(hvName == ""){
    logger(logERROR) << "no HV PS name provided -> exit";
    return -2;
  }

  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  std::shared_ptr<InfluxDBSink> infsk = std::dynamic_pointer_cast<InfluxDBSink>(sink);
  if(infsk==nullptr){
    logger(logERROR) << "Couldn't find InfluxDB in config. ";
    return -3;
  }

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  //check if config file is provided, if not exit
  EquipConf hw;
  if (configFile.empty())
    {
      logger(logERROR) << "No input config file, either create default or use --equip.";
      return -4;
    }
  else
    {
      hw.setHardwareConfig(configFile);
    }

  double Tntc = 20.0;
  int count_read = 0;

  // Module PSU for interlock
  std::shared_ptr<PowerSupplyChannel> PSlv, PShv, PSfan;
  try{
    PSlv = hw.getPowerSupplyChannel(lvName);
    PShv = hw.getPowerSupplyChannel(hvName);
    PSfan = hw.getPowerSupplyChannel(channelName);
  } catch(...){
    logger(logERROR) << "Can't connect to LV or HV or Vfan supplies: " << lvName << ", " << hvName << ", " << channelName;
    return -5;
  }

  bool atTarg = false;
  bool powerOff=false;
  uint nBadNtc = 0;
  while(!quit){

    // check module NTC temperature
    try{
      std::string resp;
      //infsk->query(resp, "select * from environment where Sensor = '"+ntcName+"' and time > now() - 60s ");
      infsk->query(resp, "select * from "+DBmeasName+" where Sensor = '"+ntcName+"' and time > now() - 60s ");
      auto infj = nlohmann::json::parse(resp);
      int time_id = -1, temp_id = -1;
      bool gti=false, gte=false;
      long unsigned int old_time = 0, new_time = 0;
      uint i=0;
      for(const auto& cols : infj["results"][0]["series"][0]["columns"].items()){
	if(cols.value() == "time") time_id = i;
	if(cols.value() == "temperature") temp_id = i;
	i++;
      }
      if(time_id>=0){
	for(const auto& vals : infj["results"][0]["series"][0]["values"].items()){
	  nlohmann::json jv = vals.value();
	  new_time = jv[time_id];
	  if(new_time>old_time){
	    if(temp_id>=0){
	      try{
		Tntc = jv[temp_id]; 
	      } catch(...){
		Tntc = -273.15;
	      }
	    }
	    old_time = new_time;
	  }
	}
	// sanity checks
	std::time_t ts = old_time/1e9; // InfluxDB timestamp is in 0.1 ns
	if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts)>30){
	  logger(logERROR) << "InfluxDB data not updated for more than 30 s. ABORT!";
	  quit = true;
	  powerOff=true;
	}
	else if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts)>10){
	  logger(logWARNING) << "InfluxDB data not updated for more than 10 s!";
	}
	
	if(Tntc<-100.) { //wrong reading, abort if it occurs several times
	  nBadNtc++;
	  if(nBadNtc>3){
	    logger(logERROR) << "Unreasonable NTC T reading: " << Tntc << ", check cabling. ABORT!";
	    quit = true;
	    powerOff=true;
	  } else {
	    logger(logWARNING) << "Unreasonable NTC T reading: " << Tntc;
	  }
	} else
	  nBadNtc=0;
      }
    } catch(std::runtime_error& e){ // runtime error check
      PShv->turnOff();
      PSlv->turnOff();
      logger(logERROR) << "Catch runtime_error -> return";
    }

    // the actual interlock check
    if(Tntc>upperlim){
      logger(logERROR) << "SW interlock fired -> powering off LV, HV";
      quit = true; // quits loop and thus cooling
      powerOff=true;
    }

    // fan voltage interlock check
    if(PSfan->measureVoltage()<5.0){
      logger(logERROR) << "fan unit off or low voltage -> powering off LV, HV";
      quit = true; // quits loop and thus cooling
      powerOff=true;
    }

    // sleep 1s
    if(!quit) std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    if(count_read == 10){
      logger(logINFO) << "T_NTC " << Tntc;
      //logger(logINFO) << "T_NTC " << std::setprecision(1) << Tntc;
      count_read = 0;
    }
    else{
      count_read++;
    }

  }

  if(powerOff){
    PShv->turnOff();
    PSlv->turnOff();
  }

}
