#include <getopt.h>
#include <string.h>
#include <signal.h>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <thread>
#include <chrono>
#include <sys/stat.h> 
#include <map>
#include <nlohmann/json.hpp>

#include <Logger.h>
#include <ClimateSensorRegistry.h>
#include <ClimateSensorConf.h>
#include <ClimateSensorAnalog.h>
#include <ClimateSensor.h>
#include <NTCSensor.h>
#include <HS1101LF.h>
#include <DataSinkConf.h>
#include <IDataSink.h>
#include "Keithley2700.h"
#include "Agilent34410A.h"
#include <InfluxDBSink.h>
#include <ReadFromDB.h>

//------ SETTINGS
std::string configFile="env_monitor.json";
std::string streamName="Climate";
std::string sinkName="db";
std::string label="all";
std::string DBmeasName="";
//---------------


bool quit=false;
void cleanup(int signum)
{ quit=true; }


void usage(char *argv[])
{
  std::cout << "Usage: "<< argv[0] << 
    " [-c config file] [-s stream name] [-n sensor name(s)] [-m InfluxDB meas. name]"<< std::endl;
  std::cout << "   defaults: -c " << configFile << " -s " << streamName<< " -n " << label << std::endl;
  std::cout << "OR: " << argv[0] << " -l" << std::endl;
  std::cout << "   to list all available classes that can be created from json" << std::endl;
}
void listSensors()
{
  std::cout << "Avalable sensor classes:" << std::endl;
  std::vector<std::string> classList = EquipRegistry::listClimateSensor();
  for(std::vector<std::string>::iterator it = classList.begin(); it!=classList.end();it++)
    std::cout << *it << std::endl;
}
int main(int argc, char** argv)
{

  using json = nlohmann::json;

  //Parse command-line  
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"DBmeas",   required_argument, 0,  'm' },
      {"names",    required_argument, 0,  'n' },
      {"config",   required_argument, 0,  'c' },
      {"stream",   required_argument, 0,  's' },
      {"sink",     required_argument, 0,  'k' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {"list",     no_argument      , 0,  'l' },
      {0,          0,                 0,  0 }
    };
    
    c = getopt_long(argc, argv, "m:s:k:c:n:t:dhl", long_options, &option_index);
    if (c == -1)
      break;

    switch (c)
      {
      case 's':
	streamName = optarg;
	break;
      case 'k':
	sinkName = optarg;
	break;
      case 'm':
	DBmeasName = optarg;
	break;
      case 'c':
	configFile = optarg;
	break;
      case 'n':
	label = optarg;
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      case 'l':
	listSensors();
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> stream = ds.getDataStream(streamName);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  std::shared_ptr<InfluxDBSink> infsk = std::dynamic_pointer_cast<InfluxDBSink>(sink);
  if(infsk==nullptr){
    logger(logERROR) << "Couldn't find InfluxDB in config. ";
    return -3;
  }

  // hard-coded for now: module NTC via Keithley2700

  // read json config.
  json hardwareConfig;
  std::ifstream i(configFile);
  i >> hardwareConfig;
 
  // find the one with defined deviceName
  std::string name = "";
  std::string protocol = "";
  uint16_t gpib_addr = 0;
  uint16_t gpib_board = 0;
  uint16_t read_tmo = 0;


  /*
  for (const auto& it : hardwareConfig["devices"]) {
    if(it["hw-model"] == "Keithley2700") name = it["name"];
  }
  // read communication config
  for (const auto& it : hardwareConfig["devices"]) {
     if (it["name"] == name){
       for (const auto& cfg : it.items()) {
          if (cfg.key() == "communication"){
            for (const auto& commu : cfg.value().items()) {
              if (commu.key() == "protocol") protocol = commu.value();
              else if (commu.key() == "gpib_addr") gpib_addr = commu.value();
              else if (commu.key() == "gpib_board") gpib_board = commu.value();
              else if (commu.key() == "read_tmo") read_tmo = commu.value();
            }
          }
        }
      }
    }  

  if (protocol != "GPIBNICom"){
      logger(logERROR) << "This code only supports GPIBNICom protocol." << protocol;
      return -1;
  }

  json gpibConfig = {
      {"gpib_addr", gpib_addr},
      {"gpib_saddr", 0},
      {"gpib_board", gpib_board},
      {"read_tmo", read_tmo}
  };
  Keithley2700 meter(gpibConfig);
  meter.init();
  */

  for (const auto& it : hardwareConfig["devices"]) {
    if(it["hw-model"] == "Agilent34410A") name = it["name"];
  }
  // read communication config
  for (const auto& it : hardwareConfig["devices"]) {
     if (it["name"] == name){
       for (const auto& cfg : it.items()) {
          if (cfg.key() == "communication"){
            for (const auto& commu : cfg.value().items()) {
              if (commu.key() == "protocol") protocol = commu.value();
              else if (commu.key() == "gpib_addr") gpib_addr = commu.value();
              else if (commu.key() == "gpib_board") gpib_board = commu.value();
              else if (commu.key() == "read_tmo") read_tmo = commu.value();
            }
          }
        }
      }
    }  

  if (protocol != "GPIBNICom"){
      logger(logERROR) << "This code only supports GPIBNICom protocol." << protocol;
      return -1;
  }
  json gpibConfig2 = {
      {"gpib_addr", gpib_addr},
      {"gpib_saddr", 0},
      {"gpib_board", gpib_board},
      {"read_tmo", read_tmo}
  };
  Agilent34410A meter2(gpibConfig2);

  // read sensors on GPIB devices in config 
  /*
  std::map<std::string , std::shared_ptr<NTCSensor> > m_gsens;
  std::map<std::string , int > m_gchan;
  for (const auto& it : hardwareConfig["gpibsensors"]) {
     std::string name = it["name"];
     if(it["type"] == "NTCSensor"){
       int chan = -1;
       for (const auto& cfg : it.items()) {
         if (cfg.key() == "device" && cfg.value() != "Keithley2700"){
           logger(logERROR) << "device not supported:" << cfg.value();
           return -1;
         }
         if (cfg.key() == "chan") chan = cfg.value();
       }
       if(chan >=0) {
         m_gchan.insert(std::make_pair(name, chan));
         std::shared_ptr<NTCSensor> sens(new NTCSensor(name));
         sens->setConfiguration(it);
         m_gsens.insert(std::make_pair(name, sens));
       }
     }
  }
  */

  std::string amb2_name = "Ambient 2", Tamb_name="none", Tosc_name="none";
  HS1101LF hshum(0, 0, 0, 0);
  int hchan = 0;
  for (const auto& it : hardwareConfig["gpibsensors"]) {
    amb2_name = it["name"];
    if(it["type"] == "HS1101LF"){
      hshum.setConfiguration(it);
      for (const auto& cfg : it.items()) {
	if (cfg.key() == "device" && cfg.value() != "Agilent34410A"){
	  logger(logERROR) << "device not supported:" << cfg.value();
	  return -1;
	}
	if (cfg.key() == "tempSensA") {
	  logger(logDEBUG) << "Adding " << cfg.value() << " as HS1101 T-sens. A";
	  //hshum.setTempSensA(mySensors[cfg.value()]);
	  Tamb_name = cfg.value();
	}
	if (cfg.key() == "tempSensF"){
	  logger(logDEBUG) << "Adding " << cfg.value() << " as HS1101 T-sens. F";
	  //hshum.setTempSensF(mySensors[cfg.value()]);
	  Tosc_name = cfg.value();
	}
	if (cfg.key() == "chan") hchan = cfg.value();
      }
    }
  }

  // end hard-coded

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  int oldNerr = 0, nerr=0; // number of consecutive errors
  while(!quit){

  /*
  for(std::map<std::string , std::shared_ptr<NTCSensor> >::iterator it = m_gsens.begin();
      it!=m_gsens.end(); it++){
    float R_NTC=1.e38;
    try{
      if(m_gchan[it->first]==0)
	R_NTC = std::stof(meter.readChannel(KeithleyMode::RESISTANCE));
      else
	R_NTC = std::stof(meter.readChannel(std::to_string(m_gchan[it->first]), KeithleyMode::RESISTANCE));
    } catch(std::runtime_error &err){
      std::cerr << "Caught unhandled runtime exception: " << err.what() << " reading NTC" << std::endl;
    } catch(...){
      std::cerr << "Caught unhandled exception reading NTC" << std::endl;
    }
    stream->setTag("Sensor", it->first);
    stream->startMeasurement((DBmeasName=="")?"environment":DBmeasName, std::chrono::system_clock::now());
    double measval;
    // write all values that are covered into stream
    try{
      measval = it->second->RtoC(R_NTC);
      stream->setField("temperature", measval);
    } catch(...){}
    stream->recordPoint();
    stream->endMeasurement();
  }
  */

    if(hchan==0){
      double freq = std::stof(meter2.sense(AgilentMode::FREQUENCY));
      hshum.setFreq(freq);
      logger(logDEBUG) << "HS1100 frequency: " << freq;
    } else {
      // not supported in Agilent
      //    hshum.setFreq(std::stof(meter2.sense(std::to_string(hchan), AgilentMode::FREQUENCY)));
      logger(logERROR) << "Reading of several channels not supported on Agilent meter";
      break;
    }
    double Tamb = 20.0;
    if(Tamb_name!="none")
      ReadFromDB(infsk, DBmeasName, Tamb_name, "temperature", 20, 30, Tamb);
    hshum.setTemp(Tamb);
    double Tosc = 20.0;
    if(Tosc_name!="none")
      ReadFromDB(infsk, DBmeasName, Tosc_name, "temperature", 20, 30, Tosc);
    hshum.setTempOsc(Tosc);

    try{
      hshum.read();
    } catch(std::runtime_error &err){
      std::cerr << "Caught unhandled runtime exception: " << err.what() << " in " << amb2_name << std::endl;
    } catch(...){
      std::cerr << "Caught unhandled exception in " << amb2_name << std::endl;
    }
    stream->setTag("Sensor", amb2_name);
    stream->startMeasurement((DBmeasName=="")?"environment":DBmeasName, std::chrono::system_clock::now());
    double measval;
    // write all values that are covered into stream
    try{
      measval = hshum.temperature();
      stream->setField("temperature", measval);
    } catch(...){}
    try{
      measval = hshum.humidity();
      stream->setField("humidity", measval);
      measval = hshum.dewPoint();
      stream->setField("dewpoint", measval);
    } catch(...){}
    stream->recordPoint();
    stream->endMeasurement();
  }

  return 0;
}
