#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <signal.h>
#include <getopt.h>
#include <nlohmann/json.hpp>

#include <EquipConf.h>
#include <IPowerSupply.h>
#include <DataSinkConf.h>
#include <IDataSink.h>
#include <InfluxDBSink.h>
#include <Logger.h>
#include <PID_v1.h>
#include <simpati.h>

// initialisation of PID parameters
double kp = 1.0;
double ki = 0.15;
double kd = 0.2;


bool quit=false;
void cleanup(int signum)
{ quit=true; }


void usage(char *argv[])
{
  std::cout << "Usage: "<< argv[0] << "target-T target-time config-file NTC-name DP_name [sink-name]"<< std::endl;

}

double setTchl(double Ttarg)
{
  double Tchl = Ttarg -18 ; 
  // Now: target-T equals to the chamber T to be set. Could be changed.
  if(Tchl<-40.0) Tchl = - 40.0;

  return Tchl;
}

int main(int argc, char** argv){

  if(argc<4){
    usage(argv);
    return -2;
}
double Ttarg = atof(argv[1]);
double timetarg = atof(argv[2]);
std::string configFile = argv[3];
std::string ntcName = argv[4];
std::string dpName = argv[5];
std::string sinkName = "db";
if(argc>=7) sinkName = argv[6];
///

double Tchl = setTchl(Ttarg);
// Create sink
DataSinkConf ds;
ds.setHardwareConfig(configFile);
std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
std::shared_ptr<InfluxDBSink> infsk = std::dynamic_pointer_cast<InfluxDBSink>(sink);
if(infsk==nullptr){
logger(logERROR) << "Couldn't find InfluxDB in config. ";
return -3;
}

///  
// communication with the small climate chamber
simpati *mychamber = NULL;
try{
mychamber = new simpati();
}catch(...){
logger(logINFO) << "Unknown exception while calling simpati";
return -4;
}
std::map<int, std::string> names = mychamber->getNames();

logger(logINFO) << "Climate chamber communication established: let's go...";
mychamber->setT(Tchl);

// Register interrupt for cleanup
signal(SIGINT, cleanup);

//check if config file is provided, if not create JSON config
EquipConf hw;
if (configFile.empty())
  {
logger(logERROR) << "No input config file, either create default or use --equip.";
return -7;
}
 else
   {
hw.setHardwareConfig(configFile);
}

double Tset=0., Tmax=40.;
double Tntc = 20.;


// set up PID class
PID pid(&Tntc, &Tset, &Ttarg, kp, ki, kd, P_ON_E, 1);

pid.SetOutputLimits((-1.)*Tmax, Tmax);
pid.SetSampleTime(60000);

pid.SetMode(1);
///

logger(logINFO) << "T_Module_target: " << Ttarg << ", T_chamber_target = " << Tchl;
auto start = std::chrono::system_clock::now();
auto end = std::chrono::system_clock::now();
auto start_plateau = std::chrono::system_clock::now();
auto end_plateau = std::chrono::system_clock::now();


std::string channel_1 = "VSP"; // Low Voltage PS 
std::string channel_2 = "Vfan"; // Voltage for the little fan 


std::shared_ptr<PowerSupplyChannel> PSa, PSb;
PSa = hw.getPowerSupplyChannel(channel_1);
double volt_1 = PSa->measureVoltage();
double curr_1 = PSa->measureCurrent();


if(channel_2!="none"){
PSb = hw.getPowerSupplyChannel(channel_2);
double volt_2 = PSb->measureVoltage();
double curr_2 = PSb->measureCurrent();
logger(logINFO) << "running dual-channel mode with " <<channel_1 << " and " << channel_2;
logger(logINFO) << "INFO: Voltage channel_1  " <<volt_1;
logger(logINFO) << "INFO: Current channel_1  " <<curr_1;
logger(logINFO) << "INFO: Voltage channel_2  " <<volt_2;
logger(logINFO) << "INFO: Current channel_2  " <<curr_2;

} else {
logger(logINFO) << "INFO: running single-channel mode with " <<channel_1;
logger(logINFO) << "INFO: Voltage single-channel  " <<volt_1;
logger(logINFO) << "INFO: Current single-channel  " <<curr_1;

}
  std::vector<int> digch;
  digch.push_back(1);
 
  bool atTarg = false;
  while(!quit){

    FILE *in = fopen("/tmp/climatechamber","r");
    if(in!=0){
      double newT;
      if(fscanf(in, "%lf\n", &newT)==1) Ttarg = newT;
      fclose(in);
      remove("/tmp/climatechamber");
      Tchl = setTchl(Ttarg);
      logger(logINFO) << "New T_Module_target: " << Ttarg << ", T_chamber_target = " << Tchl;
      mychamber->setT(Tchl);     
    }

    mychamber->setEnabled(digch, true);
    
    ///
                    
      std::string resp;
      infsk->query(resp, "select * from environment where Sensor = '"+ntcName+"' and time > now() - 45s ");
      bool runPID = true;
      
      auto infj = nlohmann::json::parse(resp);
      uint time_id = -1, temp_id = -1, dp_id=-1;
      double DP = -273.15;

      long unsigned int last_time = 0, old_time = 0, new_time = 0;
      uint i=0;
      for(const auto& cols : infj["results"][0]["series"][0]["columns"].items()){
	if(cols.value() == "time") time_id = i;
	if(cols.value() == "temperature") temp_id = i;
	if(cols.value() == "dewpoint") dp_id = i;
	
	i++;
      }
      if(time_id>=0){
	
	for(const auto& vals : infj["results"][0]["series"][0]["values"].items()){
	  nlohmann::json jv = vals.value();
	  new_time = jv[time_id];
	  if(new_time>old_time){
	    if(temp_id>=0){
	      try{
		Tntc = jv[temp_id]; 
	      } catch(...){
		Tntc = -273.15;
	      }
	    }
	    if(dp_id>=0){
	      try{
		DP = jv[dp_id]; 
	      } catch(...){
		DP = -273.15;
	      }
	    }
	  }
	  old_time = new_time;
	}
	runPID = true;
	pid.Compute();

	std::time_t ts = old_time/1e9; // InfluxDB timestamp is in 0.1 ns
	std::cout << "=== Module Tntc = " << Tntc << " at " << std::asctime(std::localtime(&ts));// << std::endl;
	
	if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts)>30){
	  logger(logERROR) << "InfluxDB data not updated for more than 30 s. ABORT!";
	  runPID = false;

	  quit = true;

	}
	else if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts)>10){
	  logger(logWARNING) << "InfluxDB data not updated for more than 10 s!";
	  runPID = false;

	}
	last_time = old_time;
      }
      //// checking with reading from DP_name environmental sensor
      
      std::string resp1;
      infsk->query(resp1, "select * from environment where Sensor = '"+dpName+"' and time > now() - 45s ");
      
      auto infj1 = nlohmann::json::parse(resp1);
      uint time_id_1 = -1, temp_id_1 = -1, humi_id_1 =-1, dp_id_1=-1;
      double DP_1 = -273.15, HUMI_1 = -100., T_1 = -273.15;
      long unsigned int last_time_1 = 0, old_time_1 = 0, new_time_1 = 0;
      uint i1=0;
      for(const auto& cols : infj1["results"][0]["series"][0]["columns"].items()){
	if(cols.value() == "time") time_id_1 = i1;
	if(cols.value() == "temperature") temp_id_1 = i1;
	if(cols.value() == "humidity") humi_id_1 = i1;
	if(cols.value() == "dewpoint") dp_id_1 = i1;
	
	i1++;
      }
      
      
      if(time_id_1>=0){
	
	for(const auto& vals : infj1["results"][0]["series"][0]["values"].items()){
	  nlohmann::json jv1 = vals.value();
	  new_time_1 = jv1[time_id_1];
	  if(new_time_1>old_time_1){
	    if(temp_id_1>=0){
	      try{
		T_1 = jv1[temp_id_1]; 
	      } catch(...){
		T_1 = -273.15;
	      }
	    }
	    if(dp_id_1>=0){
	      try{
		DP_1 = jv1[dp_id_1]; 
	      } catch(...){
		DP_1 = -273.15;
	      }
	    }
	    if(humi_id_1>=0){
	      try{
		HUMI_1 = jv1[humi_id_1]; 
	      } catch(...){
		HUMI_1 = -100.;
	      }
	    }
	  }
	  old_time_1 = new_time_1;
	}
	std::time_t ts_1 = old_time_1/1e9; // InfluxDB timestamp is in 0.1 ns
	std::cout << "== Ambient T = " << T_1 << ", Relative Humidity = " << HUMI_1 << ", Dew Point = " << DP_1 << " at " << std::asctime(std::localtime(&ts_1));// << std::endl;
	
	if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts_1)>30){
	  logger(logERROR) << "InfluxDB data not updated for more than 30 s. ABORT!";
	  quit = true;
	}
	else if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts_1)>10){
	  logger(logWARNING) << "InfluxDB data not updated for more than 10 s!";
	}
	last_time_1 = old_time_1;
      }
      
      ////
      // sanity checks
      
      if(Tntc<-40.) { //wrong reading, abort
	logger(logERROR) << "Unreasonable NTC T reading (too cold): " << Tntc << ", climate Chamber will be switched off .. ABORT!";
	mychamber->setEnabled(digch, false);	
	quit = true;
      }
      if(Tntc>40.) { //wrong reading, abort
	logger(logERROR) << "Unreasonable NTC T reading (too hot): " << Tntc << ", check cabling. cooling will remain but the PS to be switched off .. ABORT!";
	PSa->getPowerSupply()->turnOffAll();
	quit = true;
      }
      // humidity check

     if(HUMI_1 > 50.) { //wrong reading, abort
	logger(logERROR) << "Too high relative humidity: " << HUMI_1 << ", check the compressed air flow. climate Chamber will be switched off .. ABORT!";
	mychamber->setEnabled(digch, false);	
	quit = true;
      }

      // dewpoint check
      
      if(fabs(DP_1-Tntc)<10.) { //dewpoint close to Tntc
	logger(logERROR) << "NTC and DP T are very close: Tntc = " << Tntc << ", DP = " <<DP_1 << ", Climate Chamber will be switched off, check the compressed air flow!";
	mychamber->setEnabled(digch, false);		
	quit = true;
      }
      if(fabs(Tntc - Ttarg ) < 0.5) { // target T reached
      // Some computation here
	end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
	
	logger(logINFO) << " Target Temperature reached = " << Tntc << ", at " << std::ctime(&end_time)
			<< "elapsed time:" << elapsed_seconds.count() << " s\n";
	atTarg = true;
    }
      if(atTarg)start_plateau = end;
      std::time_t start_time_plateau = std::chrono::system_clock::to_time_t(start_plateau);

      while(atTarg){
	std::chrono::duration<double> elapsed_seconds_plateau =  std::chrono::system_clock::now() - start_plateau;
	logger(logINFO) << " Plateau Target Temperature = " << Tntc << ", at " << std::ctime(&start_time_plateau)
			<< "elapsed time:" << elapsed_seconds_plateau.count() << " s\n";// << std::endl;

        pid.Compute();
	
      	logger(logINFO) << "PID changed T_set temperature to  " << std::fixed << std::setprecision(2) << Tset << 
      	  " and T_NTC = " << std::setprecision(1) << Tntc <<
      	  " and T_Ambient = " << std::setprecision(1) << T_1;
	
	//      for(int i=0;i<5;i++){
	std::this_thread::sleep_for (std::chrono::milliseconds(1000));
	std::cout << "Name     \tSet\tMeas."<<std::endl;
	for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
	  std::pair<double, double> vals = mychamber->getVals()[it->first];
	  std::cout << it->second<<"\t"<<vals.first<<"\t"<<vals.second<<std::endl;
	}
	std::cout << std::endl;
	//	}
      }
      
      
      if(!quit) std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  }
  mychamber->setEnabled(digch, false);  
  return 0;
}


