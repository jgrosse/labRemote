#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <signal.h>
#include <getopt.h>
#include <nlohmann/json.hpp>

#include <EquipConf.h>
#include <DataSinkConf.h>
#include <IDataSink.h>
#include <InfluxDBSink.h>
#include <Logger.h>
#include <simpati.h>


bool quit=false;
void cleanup(int signum)
{ quit=true; }


void usage(char *argv[])
{
  std::cout << "Usage: "<< argv[0] << " Burnin-T-Profile config-file NTC-name DP_name [sink-name] "<< std::endl;

}


int main(int argc, char** argv){

  if(argc<4){
    usage(argv);
    return -2;
  }
  std::string BurninTProfile = argv[1];
  std::string configFile = argv[2];
  std::string ntcName = argv[3];
  std::string dpName = argv[4];
  std::string sinkName = "db";
  if(argc>=6) sinkName = argv[5];
  ///
 
  // Create sink
  DataSinkConf ds;
  ds.setHardwareConfig(configFile);
  std::shared_ptr<IDataSink> sink = ds.getDataSink(sinkName);
  std::shared_ptr<InfluxDBSink> infsk = std::dynamic_pointer_cast<InfluxDBSink>(sink);
  if(infsk==nullptr){
    logger(logERROR) << "Couldn't find InfluxDB in config. ";
    return -3;
  }

  ///  
  // communication with the small climate chamber
    simpati *mychamber = NULL;
    try{
      mychamber = new simpati();
    }catch(...){
      logger(logINFO) << "Unknown exception while calling simpati";
    return -4;
    }
  std::map<int, std::string> names = mychamber->getNames();

  logger(logINFO) << "Climate chamber communication established: Chamber Status: " << mychamber->getChamberStatus();

  // Register interrupt for cleanup
  signal(SIGINT, cleanup);

  //check if config file is provided, if not create JSON config
  EquipConf hw;
  if (configFile.empty())
    {
      logger(logERROR) << "No input config file, either create default or use --equip.";
      return -7;
    }
  else
    {
      hw.setHardwareConfig(configFile);
    }




  	logger(logINFO) << " The Defined Temperature Profile for the Burn-in test is : " << BurninTProfile;
      auto start = std::chrono::system_clock::now();

  std::vector<int> digch;
  digch.push_back(1);

  while(!quit){

    FILE *in = fopen("/tmp/climatechamber","r");
    if(in!=0){
      std::string newT;
      if(fscanf(in, "%lf\n", &newT)==1) BurninTProfile = newT;
      fclose(in);
      remove("/tmp/climatechamber");
    }
  	logger(logINFO) << " The Defined Temperature Profile for the Burn-in test is : " << BurninTProfile;

    	mychamber->startProgram(BurninTProfile); // example burn in profile

    
    ///
      
              
      std::string resp;
      infsk->query(resp, "select * from environment where Sensor = '"+ntcName+"' and time > now() - 45s ");
      
      auto infj = nlohmann::json::parse(resp);
      uint time_id = -1, temp_id = -1, dp_id=-1;
      double Tntc = -273.15, DP = -273.15;
      long unsigned int last_time = 0, old_time = 0, new_time = 0;
      uint i=0;
      for(const auto& cols : infj["results"][0]["series"][0]["columns"].items()){
	if(cols.value() == "time") time_id = i;
	if(cols.value() == "temperature") temp_id = i;
	if(cols.value() == "dewpoint") dp_id = i;
	
	i++;
      }
      if(time_id>=0){
	
	for(const auto& vals : infj["results"][0]["series"][0]["values"].items()){
	  nlohmann::json jv = vals.value();
	  new_time = jv[time_id];
	  if(new_time>old_time){
	    if(temp_id>=0){
	      try{
		Tntc = jv[temp_id]; 
	      } catch(...){
		Tntc = -273.15;
	      }
	    }
	    if(dp_id>=0){
	      try{
		DP = jv[dp_id]; 
	      } catch(...){
		DP = -273.15;
	      }
	    }
	  }
	  old_time = new_time;
	}
	std::time_t ts = old_time/1e9; // InfluxDB timestamp is in 0.1 ns
	std::cout << "=== Module Tntc = " << Tntc << " at " << std::asctime(std::localtime(&ts));// << std::endl;
	
	if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts)>30){
	  logger(logERROR) << "InfluxDB data not updated for more than 30 s. ABORT!";
    	  mychamber->setEnabled(digch, false);

	  quit = true;
	}
	else if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts)>10){
	  logger(logWARNING) << "InfluxDB data not updated for more than 10 s!";
	}
	last_time = old_time;
      }
      //// checking with reading from DP_name environmental sensor
      
      std::string resp1;
      infsk->query(resp1, "select * from environment where Sensor = '"+dpName+"' and time > now() - 45s ");
      
      auto infj1 = nlohmann::json::parse(resp1);
      uint time_id_1 = -1, temp_id_1 = -1, dp_id_1=-1;
      double DP_1 = -273.15, T_1 = 20.;
      long unsigned int last_time_1 = 0, old_time_1 = 0, new_time_1 = 0;
      uint i1=0;
      for(const auto& cols : infj1["results"][0]["series"][0]["columns"].items()){
	if(cols.value() == "time") time_id_1 = i1;
	if(cols.value() == "temperature") temp_id_1 = i1;
	if(cols.value() == "dewpoint") dp_id_1 = i1;
	
	i1++;
      }
 
      
      if(time_id_1>=0){
	
	for(const auto& vals : infj1["results"][0]["series"][0]["values"].items()){
	  nlohmann::json jv1 = vals.value();
	  new_time_1 = jv1[time_id_1];
	  if(new_time_1>old_time_1){
	    if(temp_id_1>=0){
	      try{
		T_1 = jv1[temp_id_1]; 
	      } catch(...){
		T_1 = -273.15;
	      }
	    }
	    if(dp_id_1>=0){
	      try{
		DP_1 = jv1[dp_id_1]; 
	      } catch(...){
		DP_1 = -273.15;
	      }
	    }
	  }
	  old_time_1 = new_time_1;
	}
	std::time_t ts_1 = old_time_1/1e9; // InfluxDB timestamp is in 0.1 ns
	std::cout << "=== Ambient T = " << T_1 << ", Dew Point = " << DP_1 << " at " << std::asctime(std::localtime(&ts_1));// << std::endl;
	
	if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts_1)>30){
	  logger(logERROR) << "InfluxDB data not updated for more than 30 s. ABORT!";
          mychamber->setEnabled(digch, false);

	  quit = true;
	}
	else if((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())-ts_1)>10){
	  logger(logWARNING) << "InfluxDB data not updated for more than 10 s!";
	}
	last_time_1 = old_time_1;
      }
      
      ////
      // sanity checks
      
      if(Tntc<-40.) { //wrong reading, abort
	logger(logERROR) << "Unreasonable NTC T reading: " << Tntc << ", check cabling. ABORT!";
        mychamber->setEnabled(digch, false);

	quit = true;
      }
      // dewpoint check
      
      if(fabs(DP_1-Tntc)<10.) { //dewpoint close to Tntc
	logger(logERROR) << "NTC and DP T are very close: Tntc = " << Tntc << ", DP = " <<DP_1 << ", check the compressed air pressure!";
        mychamber->setEnabled(digch, false);

	quit = true;
      }
	
	
        for(int i=0;i<5;i++){
	  std::this_thread::sleep_for (std::chrono::milliseconds(1000));
	  std::cout << "Name     \tSet\tMeas."<<std::endl;
	  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
	    std::pair<double, double> vals = mychamber->getVals()[it->first];
	    std::cout << it->second<<"\t"<<vals.first<<"\t"<<vals.second<<std::endl;
	  }
	  std::cout << std::endl;
	}
      
      // also store chiller measured temperature
      //sink->setTag("Sensor", "ChillerFluid");
      //sink->startMeasurement("environment", std::chrono::system_clock::now());
      //sink->setField("temperature", chiller->measureTemperature());
      //sink->recordPoint();
      //sink->endMeasurement();
      
      // sleep 1s
      if(!quit) std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  }
      // Some computation here
	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end-start;
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
	
	logger(logINFO) << " End of Program at " << std::ctime(&end_time)
			<< "elapsed time:" << elapsed_seconds.count() << "s\n";// << std::endl;

  
  //sc->sendreceive("RELOFF");
  //chiller->turnOff();
  
  return 0;
}


