#include <Adafruit_MCP23017.h>

const int POLARITY = 8;
const unsigned int MAXCMDLENGTH=256;
// Command parsing
size_t cmdptr=0;
char command[MAXCMDLENGTH];

Adafruit_MCP23017 mcp;

// The big main loop
//

//
// Setup serial
void setup()
{
  Serial.begin(9600);
  mcp.begin();
  mcp.pinMode(POLARITY, OUTPUT);
}

//
// The main loop looks for commands
void loop()
{
  if(Serial.available()==0)
    return;

  // Read new data
  size_t length=Serial.readBytes(&command[cmdptr], min(Serial.available(), MAXCMDLENGTH-cmdptr));
  if(length==0) return; // No new data...
  cmdptr+=length;

  // Check if command finished (new line)
  if (cmdptr < 2) return;
  if (command[cmdptr-2]!='\r' && command[cmdptr-1]!='\n') {
    if (cmdptr >= MAXCMDLENGTH-1) {
      //overflow command. Clean-up buffer to avoid stalled program
      cmdptr=0;
      Serial.println("ERR Command too long");
    }
    return;
  }

  // There is a command! Process it...
  char *c=command; while(*c) { *c=toupper(*c); c++; }
  command[cmdptr-2]='\0';

  // Execute the right command                                                                            
  if(strncmp("HELP", command, 4)==0)
    { // Print help menu
       Serial.println("Hello World from ArdRelay! Use commands RELON and RELOFF.");
    }
  else if(strncmp("RELON", command, 5)==0)
    { // Print help menu
       Serial.println("Turning relay on");
       mcp.digitalWrite(POLARITY, HIGH);
    }
  else if(strncmp("RELOFF", command, 6)==0)
    { // Print help menu
       Serial.println("Turning relay off");
       mcp.digitalWrite(POLARITY, LOW);
    }
  else
    {
       Serial.println("ERR unknown command");
    }
  
  cmdptr=0; // Reset command

}
