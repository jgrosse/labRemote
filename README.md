# Small library to control and read out lab equipment.

<!---------------------------------------------------------------------------->
<!------------------------- TABLE OF CONTENTS -------------------------------->
<!---------------------------------------------------------------------------->
# Table of Contents
 * [Getting the Code](#getting-the-code)
   * [Updating Submodules After Changing Branches](#updating-submodules-after-changing-branches)
 * [labRemote Requirements](#labremote-requirements)
   * [Additional Requirements](#additional-requirements)
 * [Compiling labRemote](#compiling-labremote)
 * [labRemote Examples](#labremote-examples)
 * [Contributing to labRemote](#contributing-to-labremote)
 * [Creating New labRemote Based Projects](#creating-new-projects)
 * [Python Bindings](#python-bindings)
   * [Requirements for Python Bindings](#requirements-for-python-bindings)
   * [Installation with pip](#installation-with-pip)
   * [How to use the Python bindings](#how-to-use-the-python-bindings)

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
# Getting the Code

You can obtain the code by checking out the `labRemote` repository:
```shell
git clone --recursive <url> # example url: https://gitlab.cern.ch/berkeleylab/labRemote.git
```
The `--recursive` flag checks out any of the `labRemote` external dependencies (e.g.
[nlohmann json](https://github.com/nlohmann/json)). If you wish to work off of the
non-default branch of `labRemote`, please see the next section on [Updating submodules when
changing branches](#updating-submodules-after-changing-branches).

## Updating submodules after changing branches
Typical use cases involve cloning the `labRemote` repository following the command
described in [Getting the Code](#getting-the-code) and then switching to another
branch immediately after. Switching `labRemote` branches does not automatically update the
submodules contained within `labRemote` to their respective commit histories of
the `labRemote` branch that you switch to. Because of this, you must manually update
the `labRemote` submodules when changing branches. The complete procedure then becomes:

```shell
git clone --recursive <url> # as before
cd labRemote
git checkout <branch-name>
git submodule update --init --recursive
```
At this point, the submodules will be at their commits corresponding to the branch
`<branch-name>` as opposed to their state of the default branch checked out in the
initial `git clone` call.

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
# labRemote Requirements

The minimal requirements for compiling the `labRemote` code are those provided by
a suitable `C++` compiler. `labRemote` is written with `C++11` in mind -- specificifcally, that provided
by the default `CentOS7` installation: `gcc 4.8.5`:

| Requirement | Value |
| :--- | :--- |
| gcc | 4.8.5 (C++11) |

## Additional Requirements

Additional external libraries may be necessary if you need to leverage specific
`labRemote` libraries. These are specified in the table below.
For those libraries not provided as `labRemote` submodules, it is expected
that the user is responsible for installing them on their system if required.

| External Requirement | Comment / Links | Relevant `labRemote` libraries | Provided as `labRemote` submodule under [src/exts](src/exts)? |
| :--- | :--- | :--- | :--- |
| [nlohmann_json](https://github.com/nlohmann/json) | Header-only JSON for Modern C++ | [libPS](src/libPS) and [libEquipConf](src/libEquipConf) | **YES** |
| [influxdb-cpp](https://github.com/orca-zhang/influxdb-cpp) | Header-only C++ client for InfluxDB | [libDataSink](src/libDataSink) | **YES** |
| [pybind11](https://github.com/pybind/pybind11) | `python` bindings for `labRemote` (see [Python Bindings](#python-bindings) section) | | **YES** |
| [libftdi](https://www.intra2net.com/en/developer/libftdi/download.php) | Generic FTDI library | FTDICom | **NO** |
| [libmpsse](https://github.com/l29ah/libmpsse) | Library that implements common communication protocols (I2C, SPI) using the MPSSE framework on FTDI chips | FTDICom | **NO** |
| [linux-gpib](https://linux-gpib.sourceforge.io/) | Support for GPIB hardware | [libPS](src/libPS) | **NO** |
| [Qt 5.9.2](https://www.qt.io/download-qt-for-application-development) | GUI fun in C++ | [Probe Station](src/libWaferProb) | **NO** |
| [OpenCV 3.3.1](https://docs.opencv.org/3.3.1/d7/d9f/tutorial_linux_install.html) | | [Probe Station](src/libWaferProb) | **NO** |
| [gclib](http://galilmc.com/sw/pub/all/doc/gclib/html/osx.html) | | [Probe Station](src/libWaferProb) | **NO** |
| [graphviz](https://graphviz.org/) | Requirement for `doxygen`, provides `dot` utility | | **NO** |

One can also refer to the [.gitlab-ci.yml](.gitlab-ci.yml) file for a complete listing
of installation and dependencies.

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
# Compiling labRemote
Once you have checked out the `labRemote` repository, follow the usual `cmake` installation
procedure:
```shell
cd path/to/labRemote/
mkdir build
cd build
cmake3 .. # Can be cmake outside of CentOS
make
```

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
# labRemote Examples
Example programs using `labRemote` libraries are provided for in the
[src/examples](src/examples) directory. Upon successuful compilation,
they should be available as executables under the installation directory
(`build/examples/`).

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
# Contributing to labRemote
See the project's [CONTRIBUTING](CONTRIBUTING.md) page for complete info.


<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
# Creating New Projects
You can add `labRemote` as a submodule and create a new project.
All projects should be [labRemote-apps](https://gitlab.cern.ch/berkeleylab/labremote-apps).
An example `labRemote-app` is provided for by [ExampleLabRemoteProject](https://gitlab.cern.ch/berkeleylab/labremote-apps/examplelabremoteproject)


<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
# Python Bindings

Python bindings to the `labRemote` target can be enabled either by setting a flag
during the [cmake compilation step](#compiling-labremote) or via `pip`.

## Requirements for Python Bindings

A `python` version `>=3.6` is required in order to build the `python` bindings for `labRemote`. Ensure that
you have the needed `python` source files and development packages installed on your system.

On `CentOS` this can be achieved with:
```shell
yum install python3-devel
```
More information on how to install `python` can be found [here](https://github.com/dantrim/danny_installs_python).

## Installation with `pip`

You can build the `labRemote` `python` bindings using `pip`. There are two ways you can do this:
  - [Method Number One](#method-number-one): requires that you first [clone the `labRemote` repository as usual](#getting-the-code)
  - [Method Number Two](#method-number-two): does not require that you manually clone `labRemote`
  
The two methods are outlined below.

### Method Number One
This method assumes that you have [cloned the `labRemote` repository](#getting-the-code) and is the method
to use if you need to have the `labRemote` `C++` source code and/or utilities available in addition to using
the `python` bindings. For example, if you are a developer you should follow this method.

The method is outlined here:
```shell
cd /path/to/labRemote 
python3 -m venv myenv
source myenv/bin/activate # start the virtual environment
{myenv} python -m pip install -U pip
{myenv} python -m pip install .
{myenv} python -c "import labRemote"
```
Note that you are not absolutely required to follow the [usual CMake build procedure](#compiling-labremote) in order
for the steps above to succeed: the necessary build for the `labRemote` module occurs during the `pip install .` step.
Of course, if you wish to use the `C++` utilities you will still need to [compile labRemote as usual](#compiling-labremote)
even after following the above steps.


### Method Number Two

This method does not require that you have cloned the `labRemote` repository beforehand, and is as follows:
```shell
python3 -m venv myenv
source myenv/bin/activate # start the virtual environment
{myenv} python -m pip install -U pip
{myenv} python -m pip install git+https://gitlab.cern.ch/berkeleylab/labRemote.git@devel
{myenv} python -c "import labRemote"
```

## How to use the `python` bindings

Examples of how to use the `labRemote` `python` bindings are provided in the [examples](src/examples) directory,
where you will find `python` versions of existing `C++` `labRemote` examples.
