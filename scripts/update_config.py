#!/bin/env python

#
# Script to update labRemote JSON configurations (c.f. !168).
#
# author: Daniel Joseph Antrim
# e-mail: dantrim AT lbl DOT gov
# date: February 2021
#

import sys
from argparse import ArgumentParser

import json
import jq # needed for updating the fields
from typing import Dict

from pathlib import Path


FIELDS_TO_UPDATE = ["devices", "channels", "datastreams", "datasinks", "Com", "ADCDevices", "I2CDevices", "sensors", "gpibsensors"]

def update_path(path : str, json_config : Dict) -> Dict :
    """
    Method to update the JSON object and/or value
    located at the position within the input (potentially
    arbitrarily deeply nested) JSON configuration.

    If no node appears in the input JSON configuration
    with the provided path, then an exception is thrown.

    The update takes the JSON objects located under
    the pointed-to node path by replacing them
    with a list of the same objects now with a "name" field
    that was once the key to the initial object.
    For example, providing the path "labA.devices" 
    along with the following JSON config,

    {
        "labA" : {
            "devices" : {
                "PS1" : {
                    "foo" : 123,
                    "bar" : "/dev/ttyAMC0"
                },
                "PS2" : {
                    "foo" : 456,
                    "bar" : "/dev/ttyAMC1"
                }
            }
        },
        "labB" : {
            "devices" : {
                "PS3" : {
                    "foo" : 200,
                    "bar" : "/dev/ttyAMC0"
                },
                "PS4" : {
                    "foo" : 800,
                    "bar" : "/dev/ttyAMC1"
                }
            }
        }
    }

    would produce the following updated JSON config as an output:

    {
        "labA" : {
            "devices" : [
                {
                    "name" : "PS1",
                    "foo" : 123,
                    "bar" : "/dev/ttyAMC0"
                },
                {
                    "name" : "PS2",
                    "foo" : 456,
                    "bar" : "/dev/ttyAMC1"
                }
            ]
        },
        "labB" : {
            "devices" : {
                "PS3" : {
                    "foo" : 200,
                    "bar" : "/dev/ttyAMC0"
                },
                "PS4" : {
                    "foo" : 800,
                    "bar" : "/dev/ttyAMC1"
                }
            }
        }
    }

    Notice that the "devices" node under the "labB" parent node
    is unaffected since the provided path was "labA.devices".

    Parameters
    ----------
    path: str
        Indicates the path to a JSON node that should be updated.
        For example, a path "labA.foo.bar.devices" will update
        the JSON objects appearing under ["labA"]["foo"]["bar"]["devices"]
        in Python dict notiation.

    json_config: Dict
        The JSON configuration to be updated.
            
    """

    keys = [x.strip() for x in path.strip().split('.')]
    final_node = json_config
    for key in keys :
        if key not in final_node :
            raise ValueError(f"ERROR: Key \"{key}\" does not appear in specified path \"{path}\" in provided JSON config")
        final_node = final_node[key]

    # the keys appearing in the JSON objects under the final node
    # specified by 'path' are now values to "name" fields of
    # JSON objects that will be grouped into a list
    updated_node = []
    for key, val in final_node.items() :
        new_object = {}
        new_object["name"] = key
        for sub_key, sub_val in val.items() :
            new_object[sub_key] = sub_val
        updated_node.append(new_object)

    # use jq to update the arbitrarily nested node
    jq_path = '.' + path.strip()
    compile_string = f'{jq_path} = {updated_node}'

    # jq needs double-quotes in order to interpet the string fields in the JSON
    compile_string = compile_string.replace("'", '"')
    updated_string = jq.compile(compile_string).input(json_config).text()
    updated_config = json.loads(updated_string)
    return updated_config


def update_config(input_filename : str) -> None :
    """
    Insepect the input JSON file and update
    any of the labRemote JSON objects that
    need to be.

    The updates are not made in place, rather a
    new file is created with a suffix indicating
    that it is updated.

    Parameters
    ----------
    input_filename: str
        The input file to open and parse

    Returns
    ----------
    None
    """

    global FIELDS_TO_UPDATE

    with open(input_filename, "r") as infile :

        try :
            input_config = json.load(infile)
        except json.JSONDecodeError as e:
            print(f"ERROR: failed to parse JSON from input: {e}")
            sys.exit(1)

    def find_path_to_field(element_to_look_for, node, path = '', paths = []) :
        """
        Find the paths to any child nodes whose name is "element_to_look_for".
        """ 
        if element_to_look_for in node :
            path = path + element_to_look_for #+ ' = ' + node[element_to_look_for] #.encode("utf-8")
            paths.append(path)
        for key, val in node.items() :
            if isinstance(val, dict) :
                find_path_to_field(element_to_look_for, val, path + key + ".", paths)

    # update those fields indicated in the global list
    # (can handle cases where a JSON config has multiple
    # paths leading to nodes whose names appear in the
    # global list)
    for field_to_update in FIELDS_TO_UPDATE :
        found_paths = []
        find_path_to_field(field_to_update, input_config, '', found_paths)
        for found_path in found_paths :
            input_config = update_path(found_path, input_config)

    # create the output filename based on adding a suffix
    # to the input file's name
    input_path = Path(input_filename)
    output_filename = input_path.stem + "_updated.json"

    # here we don't worry about overwriting, since applying
    # this script multiple times would in any case lead to
    # issues (e.g. running it over an already-updated config
    # is not recommended and may case issues)
    print(f"Storing updated configuration: {Path(output_filename).absolute()}")
    with open(output_filename, "w", encoding = "utf-8") as ofile :
        json.dump(input_config, ofile, ensure_ascii = False, indent = 4)

def main() :

    parser = ArgumentParser(description = "Update a labRemote JSON configuration file!")
    parser.add_argument("input", type = str,
            help = "The JSON file to inspect and update"
    )
    args = parser.parse_args()

    path = Path(args.input)
    if not path.is_file() or not path.exists() :
        print(f"ERROR: bad input \"{args.input}\"")
        sys.exit(1)

    # update the config and save the updated one as a new file
    update_config(args.input)

if __name__ == "__main__" :
    main()
